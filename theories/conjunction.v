(* conjunction.v
*)

From iris.heap_lang Require Import proofmode notation lang.
From TLC            Require Import LibLogic.
From spygame        Require Import type_repr proph_lib.

Section ConjunctionRule.

  Context `{!heapGS Σ}.

  Definition withProph : val := λ: "e",
    let: "p" := NewProph in
    let: "y" := "e" #() in
    resolve_proph_oneshot "p" "y";;
    "y".

  Lemma withProph_spec1
    (P :       iProp Σ)
    (Q : val → iProp Σ)
    (R : val → Prop)
    (e : val)
   `{R_dec: ∀ v, Decision (R v)} :

    {{{ P }}}            e #()  {{{ y, RET y; Q y           }}} ∗
    {{{ P }}}            e #()  {{{ y, RET y;       ⌜ R y ⌝ }}} ⊢
    {{{ P }}}  withProph e      {{{ y, RET y; Q y ∗ ⌜ R y ⌝ }}}.

  Proof.
    iIntros "[#e_spec #e_spec']". iModIntro. iIntros (Φ) "HP Post". wp_lam.
    wp_apply (wp_new_proph_oneshot val);[done|].
    iIntros (z p) "Hp"; wp_pures.
    (* Case analysis on: (R z) ∨ (¬ R z). *)
    case (R_dec z).
    (* Case: (R z). *)
    + intro R_true. wp_apply ("e_spec" with "HP").
      iIntros (y) "HQ". wp_pures.
      wp_apply (wp_resolve_proph_oneshot p z y with "Hp").
      iIntros "->". wp_pures. iApply "Post". by iFrame.
    (* Case: (¬ R z). *)
    + intro R_false. wp_apply ("e_spec'" with "HP").
      iIntros (y R_true). wp_pures.
      wp_apply (wp_resolve_proph_oneshot p z y with "Hp").
      iIntros "->". contradiction.
  Qed.

  Lemma withProph_spec2 {X : Type}
    (P : iProp Σ)
    (Q : X → val → Prop)
    (e : val) :

    (∀ x, {{{ P }}}            e #()  {{{ y, RET y;      ⌜ Q x y ⌝ }}}) ∗
          {{{ P }}}            e #()  {{{ y, RET y;        True    }}}  ⊢
          {{{ P }}}  withProph e      {{{ y, RET y; ⌜ ∀ x, Q x y ⌝ }}}.

  Proof.
    iIntros "[#e_spec #e_spec']". iModIntro. iIntros (Φ) "HP Post". wp_lam.
    wp_apply (wp_new_proph_oneshot val);[done|].
    iIntros (z p) "Hp"; wp_pures.
    (* Case analysis on: (∀ x, Q x z) ∨ (∃ x, ¬ Q x z). *)
    case (classicT (∀ x, Q x z)).
    (* Case: (∀ x, Q x z). *)
    + intro Q_true. wp_apply ("e_spec'" with "HP").
      iIntros (y) "_". wp_pures.
      wp_apply (wp_resolve_proph_oneshot p z y with "Hp").
      iIntros "->". wp_pures. by iApply "Post".
    (* Case: (∃ x, ¬ Q x z). *)
    + rewrite not_forall_eq. case. intros x Q_false. wp_apply ("e_spec" $! x with "HP").
      iIntros (y Q_true).  wp_pures.
      wp_apply (wp_resolve_proph_oneshot p z y with "Hp").
      iIntros "->". contradiction.
  Qed.

  Lemma withProph_spec3 `{I : Inhabited X}
    (P : iProp Σ)
    (Q : X → val → Prop)
    (e : val) :

    (∀ x, {{{ P }}}            e #()  {{{ y, RET y;      ⌜ Q x y ⌝ }}}) ⊢
          {{{ P }}}  withProph e      {{{ y, RET y; ⌜ ∀ x, Q x y ⌝ }}}.

  Proof.
    iIntros "#spec"; iModIntro; iIntros (Φ) "HP Post".
    wp_apply (withProph_spec2 P Q with "[] [HP]");[iSplit;[done|]|done|done].
    iModIntro. clear Φ. iIntros (Φ) "HP Post".
    wp_apply ("spec" $! inhabitant with "HP").
    iIntros (y) "_"; by iApply "Post".
  Qed.

  Lemma withProph_spec4
    (P :       iProp Σ)
    (Q : val → iProp Σ)
    (e : val) :

    {{{ P }}}            e #()  {{{ y, RET y; Q y }}} ⊢
    {{{ P }}}  withProph e      {{{ y, RET y; Q y }}}.

  Proof.
    iIntros "#e_spec". iModIntro. iIntros (Φ) "HP Post". wp_lam.
    wp_apply (wp_new_proph_option val);[done|]. iIntros (v p) "Hp"; wp_pures.
    wp_apply ("e_spec" with "HP"). iIntros (y) "HQ". wp_pures.
    wp_apply (wp_resolve_proph_Some p _ y with "Hp").
    iIntros "%". wp_pures. by iApply "Post".
  Qed.

End ConjunctionRule.
