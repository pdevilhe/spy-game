From iris.program_logic Require Import weakestpre.
From iris.base_logic    Require Import invariants.
From iris.proofmode     Require Import proofmode.
From iris.heap_lang     Require Import proofmode notation lang.
From iris.heap_lang.lib Require Import spin_lock.

From spygame Require Import lists type_repr proph_lib modulus maps
                             optimal_least_fixed_point orders_ext lock_lib.

Section Fix.

  Context `{Map Σ} `{lock} `{!lockG Σ}.

  Context `{DecTv : Decodable Tv, DecTp : Decodable Tp}.

  Context `{PO : PartialOrder Tp R}
          (bot : Tp) (bot_spec : ∀ a, R bot a)
          (ε : Tv → (Tv → Tp) → Tp)
          (Mono : monotone (pointwise_rel R) (pointwise_rel R) (λ f v, ε v f)).

  Instance interpTp_inhab  : LibLogic.Inhab Tp := (LibLogic.Inhab_of_val bot).

  Context (prop_bot : val) (prop_bot_spec : prop_bot = encode' bot).

  Context (var_eq : val) (var_eq_spec : (∀ (v1 v2 : Tv),
             {{{ True }}}
               var_eq (encode' v1) (encode' v2)
             {{{ RET #(bool_decide (v1 = v2)); True }}})).

  Context (prop_eq : val) (prop_eq_spec : (∀ (p1 p2 : Tp),
             {{{ True }}}
               prop_eq (encode' p1) (encode' p2)
             {{{ RET #(bool_decide (p1 = p2)); True }}})).

  Definition με  := optimal_lfp (=) R (λ f v, ε v f).
  Definition μ   := LibFix.support με.
  Definition Dom := LibFix.dom     με.

  Definition partial_fixed_point_ φ (D : Tv → Prop) :=
    ∀ φ', (∀ v, D v → φ v = φ' v) → ∀ v, D v → φ' v = ε v φ'.

  Definition generally_consistent_ φ D :=
    partial_fixed_point_ φ D ∧
    (∀ φ' D', partial_fixed_point_ φ' D' → ∀ v, D v → D' v → R (φ v) (φ' v)).

  Lemma partial_fixed_point_equiv φ D :
    partial_fixed_point_ φ D ↔
    LibFix.partial_fixed_point (=) (λ f v, ε v f) (LibFix.Build_partial φ D).
  Proof.
    split; intros partial_fp φ'.
    + simpl; intros pfun_eq v; by apply partial_fp.
    + intros extends v in_Dom; apply partial_fp; [ intros u in_Dom'; apply extends | ]; done.
  Qed.

  Lemma generally_consistent_equiv φ D :
    generally_consistent_ φ D ↔
    generally_consistent_partial_lfp (=) R (λ f v, ε v f) (LibFix.Build_partial φ D).
  Proof.
    unfold generally_consistent_partial_lfp; split.
    + case; intros partial_fp consistent.
      split; [ | case; intros φ' D' ]; rewrite -partial_fixed_point_equiv; [ done | ].
      intros partial_fp' v; simpl; intros [in_D in_D']; by apply (consistent _ D').
    + case; intros partial_fp consistent.
      split; [ | intros φ' D' ]; rewrite partial_fixed_point_equiv; [ done | ].
      intros partial_fp' v in_D in_D'; apply (consistent (LibFix.Build_partial φ' D')); done.
  Qed.

  Lemma μ_is_generally_consistent_ : generally_consistent_ μ Dom.
  Proof.
    case (@optimal_lfp_properties _ _ _ (=) R (λ f v, ε v f)).
    { intros x y le ge; by apply PO. } { apply LibRelation.equiv_eq. }
    by rewrite generally_consistent_equiv.
  Qed.

  Lemma μ_is_maximal_generally_consistent_ :
    ∀ φ D, generally_consistent_ φ D → ∀ v, D v → Dom v ∧ φ v = μ v.
  Proof.
    case (@optimal_lfp_properties _ _ _ (=) R (λ f v, ε v f)).
    { intros x y le ge. by apply PO. } { apply LibRelation.equiv_eq. }
    intros _ Uppb φ D; rewrite generally_consistent_equiv; intros consistent v.
    case (Uppb _ consistent); intros pred_incl pfun_equiv in_D.
    split; [ apply pred_incl | apply pfun_equiv ]; done.
  Qed.

  Lemma trivially_generally_consistent_ :
    ∀ φ D, (∀ v, ¬ D v) → generally_consistent_ φ D.
  Proof.
    intros φ D empty_dom; split;
    [ intros φ' _ v Dv | intros φ' _ _ v Dv _ ]; case (empty_dom v Dv).
  Qed.

  Lemma extends_default_map_lookup (m1 m2 : gmap Tv  Tp) G :
    G ⊆ elements (dom m1) →
      extends (λ u, default bot (m1 !! u)) (λ u, default bot ((m1 ∪ m2) !! u)) G.
  Proof.
    intro elements_incl. rewrite extends_lookup; intros i v lookup_G.
    have : v ∈ dom m1; [
    rewrite -elem_of_elements; by apply elements_incl, (elem_of_list_lookup_2 _ i) |].
    rewrite elem_of_dom; destruct 1 as [c lookup_m1].
    by rewrite (lookup_union_Some_l _ _ _ _ lookup_m1) lookup_m1.
  Qed.

  Definition lfp : val := λ: "eqs",

    let: "permanent"    := make_map #() in
    let: "transient"    := make_map #() in
    let: "dependencies" := make_map #() in
    let: "todo"         := ref nil      in

    let: "master" := newlock #() in

    let: "schedule" := λ: "v",
      map_insert "dependencies" "v" NONEV;;
      "todo" <- cons "v" (! "todo")
    in

    let: "discover" := λ: "v",
      map_insert "transient" "v" prop_bot;;
      "schedule" "v"
    in

    let: "reevaluate" := λ: "v",

      let: "p"  := NewProph    in
      let: "lk" := newlock #() in

      let: "request" := λ: "w",
        withLock "lk" (λ: <>,
          match: map_lookup "permanent" "w" with SOME "c" => "c" | NONE =>
          match: map_lookup "transient" "w" with SOME "c" => "c" | NONE =>
            "discover" "w";; resolve_proph_cons "p" "w";; prop_bot end
          end)
      in

      let: "pair" :=
        modulus ("eqs" "v") "request" in
      let: "c" := Fst "pair" in
      let: "G" := Snd "pair" in

      acquire "lk";;
      resolve_proph_nil "p";;
      map_insert "dependencies" "v" "G";;

      if: (prop_eq (map_lookup_exn "transient" "v") "c") then #() else
        let: "pred" :=
          map_dom (map_filter (λ: "pair", list_mem var_eq "v" (Snd "pair"))
                              "dependencies")
        in
        list_iter (λ: <> "w", "schedule" "w") "pred";;
        map_insert "transient" "v" "c"
    in

    let: "loop" :=
      (rec: "loop" <> :=
        match: (! "todo") with NONE => #() | SOME "pair" =>
          "todo" <- Snd "pair";; "reevaluate" (Fst "pair");; "loop" #()
        end)
    in

    let: "freeze" := λ: <>,
      map_transfer "transient" "permanent";; map_flush "dependencies"
    in

    let: "get" := λ: "v",
      withLock "master" (λ: <>,
        match: map_lookup "permanent" "v" with SOME "c" => "c" | NONE =>
          "discover" "v";; "loop" #();; "freeze" #();; map_lookup_exn "permanent" "v"
        end)
    in
    "get".


  Definition master_inv permanent transient dependencies todo : iProp Σ :=
    (∃ P,
      is_map    permanent (P : gmap Tv Tp) ∗
      is_map    transient (∅ : gmap Tv Tp) ∗
      is_map dependencies (∅ : gmap Tv (list Tv)) ∗
      todo ↦ nil ∗
      ⌜ generally_consistent_ (λ u, default bot (P !! u)) (λ v, is_Some (P !! v)) ⌝)%I.

  Definition loop_inv P v transient dependencies todo : iProp Σ :=
    (∃ T D us,
      is_map    transient (T : gmap Tv Tp) ∗
      is_map dependencies (D : gmap Tv (list Tv)) ∗
      todo ↦ encode' (us : list Tv) ∗
      ⌜ dom D = dom T ⌝ ∗
      ⌜ us ⊆ elements (dom T) ⌝ ∗ ⌜ is_Some ((P ∪ T) !! v) ⌝ ∗
      ⌜ map_Forall (λ v c,
          (v ∈ us ∧ D !! v = Some []) ∨
          (∃ G, D !! v = Some G ∧ G ⊆ elements (dom (P ∪ T)) ∧
            (∀ F', (extends (λ u, default bot ((P ∪ T) !! u)) F' G) → c = ε v F'))) T ⌝ ∗
      ⌜ ∀ φ D', partial_fixed_point_ φ D' → map_Forall (λ v c, D' v → R c (φ v)) T ⌝)%I.

  Definition lk_inv P T D us vs permanent transient dependencies todo p' : iProp Σ :=
    (∃ (ws ws' : list Tv),
      let T' := (gset_to_gmap bot (list_to_set ws) : gmap Tv Tp) in
      let D' := (gset_to_gmap []  (list_to_set ws) : gmap Tv (list Tv))in
      is_map permanent (P : gmap Tv Tp) ∗
      is_map transient (T ∪ T') ∗
      is_map dependencies (D ∪ D') ∗
      todo ↦ encode' (ws ++ us) ∗
      proph_list p' ws' ∗
      ⌜ vs = (list.reverse ws) ++ ws' ⌝)%I.

  Lemma close_permanent_inv_after_freeze (P T : gmap Tv Tp) (D : gmap Tv (list Tv)) :

    generally_consistent_ (λ u, default bot (P !! u)) (λ v, is_Some (P !! v)) →
    (map_Forall (λ v c,
      (∃ G, D !! v = Some G ∧ G ⊆ elements (dom (P ∪ T)) ∧
        (∀ F', (extends (λ u, default bot ((P ∪ T) !! u)) F' G) → c = ε v F'))) T) →
    (∀ φ D', partial_fixed_point_ φ D' → map_Forall (λ v c, D' v → R c (φ v)) T) →
    generally_consistent_ (λ u, default bot ((P ∪ T) !! u)) (λ v, is_Some ((P ∪ T) !! v)).
  Proof.
    intros [partial_fp consistent] map_Forall_T semi_consistent; split;
    [ intros φ' eq_over_dom v | intros φ' D' partial_fp' v ];
    destruct 1 as [c lookup]; revert lookup; rewrite lookup_union_Some_raw;
    case; [ | case | | case ]; intro lookup_P; [ | intros lookup_T | | intro lookup_T ].
    + apply partial_fp; [ | by exists c ]; clear lookup_P; intro u.
      destruct 1 as [c' lookup_P]; rewrite lookup_P //=.
      transitivity (default bot ((P ∪ T) !! u)); [ | apply eq_over_dom; exists c' ];
      rewrite lookup_union lookup_P; case (T !! u); done.
    + destruct (map_Forall_T v c lookup_T) as [G [lookup_D [incl extends]]].
      transitivity c.
      ++ symmetry; transitivity (default bot ((P ∪ T) !! v)); [ | apply eq_over_dom; exists c ];
         by rewrite lookup_union lookup_P lookup_T.
      ++ apply extends; rewrite extends_lookup; intros i u lookup_G; apply eq_over_dom.
         have: u ∈ dom (P ∪ T).
         { rewrite -elem_of_elements; apply incl, (elem_of_list_lookup_2 _ i); done. }
         by rewrite elem_of_dom.
    + rewrite (_ : (P ∪ T) !! v = P !! v); [ | rewrite lookup_union lookup_P; by case (T !! v) ].
      intro in_D; apply (consistent _ D'); [ | exists c | ]; done.
    + rewrite (_ : (P ∪ T) !! v = T !! v); [ | by rewrite lookup_union lookup_P lookup_T ].
      intros in_D; apply (semi_consistent _ D'); [ | rewrite lookup_T | ]; done.
  Qed.

  Lemma close_transient_inv_after_else_branch φ φ' D' :
    partial_fixed_point_ φ' D' →
    (∀ v, D' v → R (φ v)   (φ' v)) →
     ∀ v, D' v → R (ε v φ) (φ' v).
  Proof.
    intros partial_fp semi_consistent v in_D'.
    set (f := λ v, match LibLogic.classicT (D' v) with left _ => φ' v | right _ => φ v end).
    assert ((pointwise_rel R) φ f) as le.
    { intros u; unfold f.
      case (LibLogic.classicT (D' u)); [ apply semi_consistent | intros _; apply PO ]. }
    transitivity (f v).
    + rewrite (_ : f v = ε v f); [ by apply Mono | ].
      apply partial_fp; [ intros u; unfold f; case (LibLogic.classicT (D' u)) | ]; done.
    + unfold f; case (LibLogic.classicT (D' v)); [ intros _; apply PO | ]; done.
  Qed.

  Lemma lfp_spec (eqs : val) :
    {{{ implements (Func (Data Tv) (Func (Func (Data Tv) (Data Tp)) (Data Tp))) eqs ε }}}
      lfp eqs
    {{{ (f : val), RET f;
        implements (Part (Data Tv) (Data Tp)) f με }}}.
  Proof.
    iIntros (Φ) "#eqs_spec Post"; wp_lam; wp_pures.
    rewrite -encode_nil prop_bot_spec; unfold withLock.
    wp_apply (make_map_spec Tv Tp);        [ done | iIntros (permanent)    "Hpermanent"; wp_pures    ].
    wp_apply (make_map_spec Tv Tp);        [ done | iIntros (transient)    "Htransient"; wp_pures    ].
    wp_apply (make_map_spec Tv (list Tv)); [ done | iIntros (dependencies) "Hdependencies" ].
    wp_alloc todo as "Htodo".
    wp_pures; wp_bind (newlock #())%E.
    wp_apply (newlock_spec (master_inv permanent transient dependencies todo)
      with "[Hpermanent Htransient Hdependencies Htodo]").
    { iExists ∅; iFrame; iPureIntro. apply trivially_generally_consistent_, lookup_empty_is_Some. }
    iIntros (master γ1) "#Hmaster"; wp_pures. iModIntro. iApply "Post".
    iIntros (x v); iModIntro; clear Φ; iIntros (Φ) "-> Post". wp_pures.
    wp_apply (acquire_spec with "Hmaster"); iIntros "[Hlocked1 Hinv]".
    iDestruct "Hinv" as (P) "(Hpermanent & Htransient & Hdependencies & Htodo & %generally_consistent)".
    wp_pures; wp_apply (map_lookup_spec (A:=Tv) with "Hpermanent"); iIntros "Hpermanent".
    case_eq (P !! v); [ intro c | ]; intro lookup_P; rewrite lookup_P; wp_pures.
    + wp_apply (release_spec γ1 master
               (master_inv permanent transient dependencies todo)
        with "[Hlocked1 Hpermanent Htransient Hdependencies Htodo]"); [ by iFrame "∗#" | ].
      iIntros "_"; wp_pures.
      case (μ_is_maximal_generally_consistent_ _ _ generally_consistent v); [ by exists c | ].
      rewrite lookup_P //=; intros in_Dom eq_μv; rewrite eq_μv; by iApply "Post".
    + wp_apply (map_insert_spec (A:=Tv) with "Htransient"); iIntros "Htransient"; wp_pures.
      wp_apply ((map_insert_spec _ (∅ : gmap Tv (list Tv))) v  [] with "Hdependencies").
      iIntros "Hdependencies"; wp_seq; wp_load.
      wp_apply (cons_spec v []); [ done | iIntros "_" ]; wp_store.
      iAssert (loop_inv P v transient dependencies todo)
        with "[Htransient Hdependencies Htodo]" as "Hloop_inv".
      { iExists (<[v := bot]> ∅), (<[v := []]> ∅), [v]; iFrame.
        rewrite (_:(<[v := bot]> ∅) = {[v := bot]}); [| done].
        rewrite (_:(<[v := []]> ∅) = {[v := []]}); [| done].
        iPureIntro; split; [| split; [| split; [| split] ]]; try rewrite dom_singleton_L.
        + by rewrite dom_singleton_L.
        + by rewrite elements_singleton.
        + rewrite lookup_union lookup_P lookup_singleton is_Some_alt //=.
        + apply map_Forall_insert_2; [| done]; left; split;
          [ apply elem_of_list_singleton | rewrite lookup_singleton ]; done.
        + intros φ D' _; apply map_Forall_insert_2; [ intros _; apply bot_spec | ]; done. }
      iLöb as "IH_loop" forall (Φ); wp_rec.
      iDestruct "Hloop_inv" as (T D us)"(Htransient & Hdependencies & Htodo & %same_dom & %dom_incl & %is_Some_lookup_v & %map_Forall_T & %semi_consistent)".
      case_eq us; [ intro eq_nil | intros u us' eq_cons ]; wp_load; wp_match.
      - wp_seq; wp_lam.
        wp_apply (map_transfer_spec (A:=Tv) with "[Hpermanent Htransient]"); [by iFrame|].
        iIntros "[Htransient Hpermanent]"; wp_seq.
        wp_apply (map_flush_spec _ D with "Hdependencies"); iIntros "Hdependencies"; wp_seq.
        case is_Some_lookup_v; intros c lookup_PuT.
        wp_apply (map_lookup_exn_spec _ _ _ _ lookup_PuT with "Hpermanent"); iIntros "Hpermanent".
        assert (generally_consistent_ (λ u, default bot ((P ∪ T) !! u))
                                      (λ v, is_Some ((P ∪ T) !! v))) as generally_consistent_PuT.
        { apply (close_permanent_inv_after_freeze _ _ D); try done.
          clear c lookup_PuT; intros u c lookup_T; case (map_Forall_T u c lookup_T); [ | done ].
          rewrite eq_nil elem_of_nil; by case. }
        wp_pures; wp_apply (release_spec γ1 master
               (master_inv permanent transient dependencies todo)
        with "[Hlocked1 Hpermanent Htransient Hdependencies Htodo]");  [ iSplit; [ done | ] | ].
        ++ iFrame; iPureIntro; apply generally_consistent_PuT.
        ++ iIntros "_"; wp_seq.
           case (μ_is_maximal_generally_consistent_ _ _ generally_consistent_PuT v);
           [ by exists c | intros in_Dom eq_μv ]; iApply "Post"; iPureIntro; split; [ | done ].
           rewrite lookup_PuT in eq_μv; simpl in eq_μv; by rewrite eq_μv.
      - wp_pures; wp_store; wp_pures.
        wp_apply (wp_new_proph_list Tv); [ done | iIntros (vs p') "Hp'" ]; wp_let.
        wp_apply (newlock_spec
                 (lk_inv P T D us' vs permanent transient dependencies todo p')
          with "[Hpermanent Htransient Hdependencies Htodo Hp']").
        { iExists [], vs; do 2 rewrite /= gset_to_gmap_empty right_id; by iFrame. }
        iIntros (lk γ2) "#Hlk"; wp_pures.
        wp_apply "eqs_spec"; [ done | iIntros (ff) "#ff_spec" ].
        wp_apply (modulus_partial_spec ff _  (ε u)
                   (λ u, default bot ((P ∪ T) !! u))
                   (λ u, u ∈ elements (dom (P ∪ T)) ++ vs)).
        ++ iSplit; [| done]; iIntros (x u'); iModIntro; clear Φ; iIntros (Φ) "-> Post"; wp_pures.
           wp_apply (acquire_spec with "Hlk"); iIntros "[Hlocked Hinv]".
           iDestruct "Hinv" as (ws ws')
             "(Hpermanent & Htransient & Hdependencies & Htodo & Hp' & ->)".
           wp_pures; wp_apply (map_lookup_spec (A:=Tv) with "Hpermanent"); iIntros "Hpermanent".
           case_eq (P !! u'); [ intro c | ]; intro lookup_P'; rewrite lookup_P'; wp_pures.
           -- wp_apply (release_spec γ2 lk (lk_inv
                P T D us' ((list.reverse ws) ++ ws') permanent transient dependencies todo p')
                with "[Hlocked Hpermanent Htransient Hdependencies Htodo Hp']"); [
              iSplit; [ done |]; by iFrame | ].
              iIntros "_"; wp_pures; iModIntro; iApply "Post".
              assert (u' ∈ elements (dom (P ∪ T)) ++ reverse ws ++ ws').
              +++ rewrite elem_of_app dom_union_L elem_of_elements elem_of_union; left; left.
                  rewrite elem_of_dom; by exists c.
              +++ simpl; iPureIntro; split; [ rewrite (lookup_union_Some_l _ _ _ c) | ]; done.
           -- wp_apply (map_lookup_spec (A:=Tv) with "Htransient"); iIntros "Htransient".
              case_eq ((T ∪ gset_to_gmap bot (list_to_set ws)) !! u'); [ intro c | ];
              intro lookup_T_ws; rewrite lookup_T_ws;
              [ | revert lookup_T_ws; rewrite lookup_union_None; case; intros lookup_T lookup_ws ];
              wp_pures.
              +++ wp_apply (release_spec γ2 lk (lk_inv
                    P T D us' ((list.reverse ws) ++ ws') permanent transient dependencies todo p')
                    with "[Hlocked Hpermanent Htransient Hdependencies Htodo Hp']"); [
                  iSplit; [ done |]; by iFrame | ].
                  iIntros "_"; wp_pures; iApply "Post"; revert lookup_T_ws; simpl.
                  rewrite lookup_union_Some_raw; case; [|case]; intro lookup_T; [|intro lookup_ws].
                  --- assert (u' ∈ (elements (dom (P∪T)) ++ reverse ws ++ ws')).
                      ++++ rewrite elem_of_app dom_union_L elem_of_elements elem_of_union.
                           left; right; rewrite elem_of_dom; by exists c.
                      ++++ iPureIntro; split; [ rewrite lookup_union lookup_P' lookup_T| ]; done.
                  --- assert (u' ∈ (elements (dom (P∪T)) ++ reverse ws ++ ws')).
                      ++++ do 2 rewrite elem_of_app; right; left.
                           rewrite elem_of_reverse; revert lookup_ws.
                           rewrite lookup_gset_to_gmap_Some; case; set_solver.
                      ++++ iPureIntro; split;
                           [ revert lookup_ws; rewrite lookup_gset_to_gmap_Some; case | done ].
                           intros _ eq_bot; by rewrite eq_bot lookup_union lookup_P' lookup_T.
              +++ wp_apply (map_insert_spec (A:=Tv) with "Htransient"); iIntros "Htransient"; wp_pures.
                  wp_apply (map_insert_spec (A:=Tv) _ _ _ ([] : list Tv) with "Hdependencies").
                  iIntros "Hdependencies"; wp_seq; wp_load.
                  wp_apply (cons_spec u' (ws ++ us')); [ done | iIntros "_" ]; wp_store.
                  wp_apply (wp_resolve_proph_cons p' _ u' with "Hp'"); iIntros (ws'') "[-> Hp']".
                  wp_pures; wp_apply (release_spec γ2 lk (lk_inv P T D us'
                    (reverse ws ++ u' :: ws'') permanent transient dependencies todo p')
                    with "[Hlocked Hpermanent Htransient Hdependencies Htodo Hp']").
                  { iSplit; [ done | ]; iFrame; iExists (u' :: ws).
                    have: u' ∉ dom D; [ by rewrite same_dom not_elem_of_dom |].
                    rewrite not_elem_of_dom reverse_cons; intro lookup_D.
                    do 2 (rewrite insert_union_r; [| done]; rewrite -gset_to_gmap_union_singleton).
                    iFrame; iPureIntro; by rewrite (cons_middle u' _ ws'') app_assoc. }
                  iIntros "_"; wp_pures; iApply "Post".
                  assert (u' ∈ (elements (dom (P ∪ T)) ++
                                 reverse ws ++ u' :: ws''));
                  [ do 2 rewrite elem_of_app; right; right; apply elem_of_list_here |].
                  simpl; iPureIntro; split; [ rewrite lookup_union lookup_P' lookup_T | ]; done.
        ++ iIntros (G) "[%elements_incl %eq_intro]"; wp_pures.
           wp_apply (acquire_spec with "Hlk"); iIntros "[_ Hinv]".
           iDestruct "Hinv" as (ws ws')
             "(Hpermanent & Htransient & Hdependencies & Htodo & Hp' & ->)".
           wp_seq; wp_apply (wp_resolve_proph_nil p' ws' with "Hp'"); iIntros "->"; wp_seq.
           wp_apply (map_insert_spec _ _ u G with "Hdependencies").
           iIntros "Hdependencies"; wp_seq.
           have : u ∈ dom T; [
           rewrite -elem_of_elements; apply dom_incl;
           rewrite eq_cons; apply elem_of_list_here |].
           rewrite elem_of_dom; destruct 1 as [c lookup_T].
           wp_apply (map_lookup_exn_spec (A:=Tv) _ _ _ c with "Htransient"); [
           by apply lookup_union_Some_l | iIntros "Htransient"; wp_pures ].
           wp_apply prop_eq_spec; [ done | iIntros "_" ].
           case (decodable_eq_dec c (ε u (λ u, default bot ((P ∪ T) !! u))));
           [ intro c_eq | intro c_ineq ].
           -- rewrite bool_decide_true; [| by rewrite c_eq ]; wp_if; wp_seq.
              iAssert (loop_inv P v transient dependencies todo)
                   with "[Htransient Hdependencies Htodo]" as "Hloop_inv".
              { iExists (T ∪ gset_to_gmap bot (list_to_set ws)),
                  (<[u := G]> (D ∪ gset_to_gmap []  (list_to_set ws))), (ws ++ us'); iFrame.
                iPureIntro; split; [| split; [| split; [| split ]]].
                + rewrite dom_insert_L; do 2 rewrite dom_union_L dom_gset_to_gmap.
                  rewrite same_dom -subseteq_union_L=>?.
                  rewrite elem_of_singleton elem_of_union elem_of_dom=>->. eauto.
                + intro u'; rewrite elem_of_app; case; [ intro in_ws | intro in_us' ];
                  subst us; generalize (dom_incl u');
                  rewrite dom_union_L dom_gset_to_gmap !elem_of_elements
                          elem_of_union elem_of_list_to_set elem_of_cons; by auto.
                + case is_Some_lookup_v; intros c' lookup_PuT.
                  by rewrite assoc lookup_union lookup_PuT is_Some_alt union_Some_l.
                + intros u' c'; case (decodable_eq_dec u' u);
                  [intros u'_eq lookup_T'|intros u'_neq].
                  - simplify_eq; right; exists G; rewrite lookup_insert; split; [ done | split ].
                    ++ rewrite assoc dom_union_L dom_gset_to_gmap.
                       revert elements_incl; rewrite app_nil_r Forall_forall; intro elements_incl.
                       intros g in_G; specialize (elements_incl g in_G); revert elements_incl.
                       rewrite elem_of_app; do 2 rewrite elem_of_elements; case; [ set_solver |].
                       rewrite elem_of_reverse elem_of_union; intro in_ws; right; set_solver.
                    ++ rewrite (lookup_union_Some_l _ _ _ _ lookup_T) in lookup_T'.
                       injection lookup_T'; simplify_eq; intros _ F' extends_G; apply eq_intro.
                       apply (extends_transitive
                       (λ u, default bot ((P ∪ T) !! u))
                       (λ u, default bot (((P ∪ T) ∪ (gset_to_gmap bot (list_to_set ws))) !! u))).
                       -- rewrite extends_lookup; intros _ u' _.
                          case_eq ((P ∪ T) !! u'); [ intro c' | ]; intro lookup_PuT.
                          +++ by rewrite (lookup_union_Some_l _ _ _ _ lookup_PuT).
                          +++ rewrite lookup_union lookup_PuT lookup_gset_to_gmap.
                              case (gset_elem_of_dec u' (list_to_set ws)).
                              --- intro     in_ws; by rewrite option_guard_True.
                              --- intro not_in_ws; by rewrite option_guard_False.
                       -- by rewrite -assoc.
                  - rewrite (lookup_insert_ne _ _ _ _ (not_eq_sym u'_neq)).
                    rewrite (lookup_union_Some_raw T); case;
                    [ intro lookup_T'; case (map_Forall_T _ _ lookup_T') |
                      case; intros lookup_T' lookup_ws ].
                    ++ case; rewrite eq_cons elem_of_cons; intros in_us lookup_D; left.
                       rewrite elem_of_app (lookup_union_Some_l _ _ _ _ lookup_D).
                       split; [ right; case in_us | ]; done.
                    ++ destruct 1 as [Gu' [lookup_D [elements_incl' eq_intro']]]; right.
                       exists Gu'; rewrite (lookup_union_Some_l _ _ _ _ lookup_D).
                       split; [ done | split; [ | intros F' extends_Gu' ] ].
                       -- rewrite assoc; intros g in_Gu'.
                          specialize (elements_incl' g in_Gu'); revert elements_incl'.
                          do 2 rewrite elem_of_elements; intro in_PuT.
                          rewrite dom_union_L elem_of_union; left; done.
                       -- apply eq_intro'; rewrite assoc in extends_Gu'.
                          apply (extends_transitive
                            (λ u, default bot ((P ∪ T) !! u))
                            (λ u, default bot
                              (((P ∪ T) ∪ (gset_to_gmap bot (list_to_set ws))) !! u))); [| done].
                          by apply extends_default_map_lookup.
                    ++ revert lookup_ws; rewrite lookup_gset_to_gmap_Some; case; intros in_ws _.
                       left; rewrite elem_of_app; split; [ left; set_solver | ].
                       have: u' ∉ dom D;
                       [ rewrite same_dom | ]; rewrite not_elem_of_dom; [ done | ].
                       intro lookup_D; rewrite lookup_union lookup_D.
                       rewrite lookup_gset_to_gmap option_guard_True; done.
                + intros φ D' partial_fp u' c'; rewrite lookup_union_Some_raw; case.
                  - by apply semi_consistent.
                  - rewrite lookup_gset_to_gmap_Some; case; intros _; case; intros _ eq_bot _.
                    rewrite -eq_bot; apply bot_spec.
              }
              by iApply ("IH_loop" with "[Post] [Hlocked1] [Hpermanent] [Hloop_inv]").
           -- rewrite bool_decide_false; [ wp_pures | intro c_eq; contradiction ].
              wp_apply (map_filter_spec (λ (p : Tv * list Tv), u ∈ p.2) with "[Hdependencies]").
              { iFrame; iIntros (x (u', G')); iModIntro; clear Φ; iIntros (Φ) "-> Post".
                wp_pures. wp_apply (list_mem_spec var_eq u G');
                            [apply var_eq_spec|done|iIntros "_"].
                by iApply "Post". }
              iIntros (pred_map) "[Hdependencies Hpred_map]".
              wp_apply (map_dom_spec _ (_ : gmap Tv (list Tv)) with "Hpred_map").
              iIntros (pred) "[Hpred_map %pred_eq_dom]". wp_pures.

              wp_apply (list_iter_spec _ pred (λ n, let pred' := take n pred in
                       todo ↦ encode' ((reverse pred') ++ (ws ++ us')) ∗
                       is_map dependencies ((gset_to_gmap [] (list_to_set pred'))
                         ∪ (<[u:=G]> (D ∪ gset_to_gmap [] (list_to_set ws)))))%I
                with "[Htodo Hdependencies]").
              { simpl; rewrite gset_to_gmap_empty left_id; iFrame.
                iIntros (n w); iModIntro.
                clear Φ; iClear "IH_loop eqs_spec ff_spec Hmaster Hlk".
                iIntros (Φ) "[[Htodo Hdependencies] %] Post"; wp_pures.
                wp_apply (map_insert_spec _ _ w ([] : list Tv) with "Hdependencies").
                iIntros "Hdependencies"; wp_seq; wp_load.
                wp_apply (cons_spec w); [ done | iIntros "_" ]; wp_store; iApply "Post".
                rewrite (take_S_r _ _ w) // reverse_app /=. iFrame.
                rewrite insert_union_l -gset_to_gmap_union_singleton -list_to_set_cons.
                rewrite (_ : list_to_set (w :: take n pred) =
                             list_to_set (take n pred ++ [w])); [ done | ].
                apply list_to_set_perm_L, Permutation_cons_append. }
              rewrite firstn_all; iIntros "[Htodo Hdependencies]"; wp_seq.
              wp_apply (map_insert_spec (A:=Tv) with "Htransient"); iIntros "Htransient"; wp_seq.
              iAssert (loop_inv P v transient dependencies todo)
                   with "[Htransient Hdependencies Htodo]" as "Hloop_inv".
              { iExists (<[u := ε u (λ u0 : Tv, default bot ((P ∪ T) !! u0))]>
                          (T ∪ gset_to_gmap bot (list_to_set ws))),
                        (gset_to_gmap [] (list_to_set pred)
                           ∪ <[u := G]> (D ∪ gset_to_gmap [] (list_to_set ws))),
                        (reverse pred ++ ws ++ us'); iFrame.
                iPureIntro; split; [| split; [| split; [| split ]]].
                + rewrite dom_union_L; do 2 (rewrite dom_insert_L dom_union_L).
                  do 3 rewrite dom_gset_to_gmap; rewrite same_dom.
                  rewrite -subseteq_union_L pred_eq_dom.
                  rewrite ->dom_filter_subseteq.
                  by rewrite dom_insert_L dom_union_L same_dom dom_gset_to_gmap.
                + intro u'; do 2 rewrite elem_of_app.
                  rewrite elem_of_elements dom_insert_L dom_union_L dom_gset_to_gmap.
                  rewrite elem_of_reverse.
                  case; [ intro in_pred | case ].
                  - have: u' ∈ (list_to_set pred : gset Tv); [
                    by rewrite elem_of_list_to_set | rewrite pred_eq_dom ].
                    intro in_dom; specialize (dom_filter_subseteq _ _ _ in_dom).
                    by rewrite dom_insert_L dom_union_L dom_gset_to_gmap same_dom.
                  - do 2 rewrite elem_of_union; right; right; by apply elem_of_list_to_set.
                  - intro in_us'; do 2 rewrite elem_of_union; right; left.
                    rewrite -elem_of_elements; apply dom_incl; rewrite eq_cons.
                    apply elem_of_cons; by right.
                + case is_Some_lookup_v; intro c'; rewrite lookup_union_Some_raw is_Some_alt; case.
                  - intro lookup_P'. rewrite lookup_union lookup_P'; by case (_ !! v).
                  - case; intros lookup_P' lookup_T'; rewrite insert_union_l.
                    do 2 rewrite lookup_union; rewrite lookup_P'.
                    have: is_Some (<[u:=ε u (λ u0 : Tv, default bot ((P ∪ T) !! u0))]> T !! v).
                    ++ rewrite lookup_insert_is_Some.
                       case (decodable_eq_dec u v); [ left | right ];
                       [ | split; [ | exists c' ] ]; done.
                    ++ case; intros c'' lookup_PuT; rewrite lookup_PuT; by case (_ !! v).
                + intros u' c' lookup_T_ws.
                  case_eq ((gset_to_gmap ([] : list Tv) (list_to_set pred)) !! u').
                  - intros Gu' lookup_ws; left; split.
                    ++ rewrite elem_of_app; left; revert lookup_ws.
                       rewrite (lookup_gset_to_gmap_Some [] (list_to_set pred) u' Gu'); case.
                       rewrite elem_of_reverse elem_of_list_to_set; done.
                    ++ rewrite (lookup_union_Some_l _ _ _ _ lookup_ws).
                       revert lookup_ws; rewrite lookup_gset_to_gmap_Some.
                       case; intros _ eq_Gu'; by rewrite eq_Gu'.
                  - intro lookup_ws; rewrite lookup_union lookup_ws //=.
                    have: u' ∉ dom (filter (λ p, u ∈ p.2)
                            (<[u := G]> (D ∪ gset_to_gmap [] (list_to_set ws)))).
                    { rewrite -pred_eq_dom.
                      rewrite -(dom_gset_to_gmap (list_to_set pred : gset Tv)
                                                 ([] : list Tv)).
                      rewrite not_elem_of_dom; done. }
                    rewrite not_elem_of_dom; intro lookup_filter.
                    case (decodable_eq_dec u' u);[intro u'_eq; rewrite u'_eq|intro u'_neq].
                    ++ rewrite lookup_insert //=; right; exists G; split; [ done | split ].
                       -- revert elements_incl; rewrite Forall_forall; intro elements_incl.
                          intros g in_G; specialize (elements_incl g in_G); revert elements_incl.
                          rewrite app_nil_r elem_of_app; do 2 rewrite dom_union_L.
                          rewrite dom_insert_L dom_union_L dom_gset_to_gmap elem_of_reverse.
                          do 2 rewrite elem_of_elements; set_solver+.
                       -- revert lookup_T_ws; rewrite u'_eq lookup_insert; injection 1.
                          intros c'_eq F' extends_G; rewrite -c'_eq; apply eq_intro.
                          assert (u ∉ G) as not_in_G.
                          { revert lookup_filter. rewrite map_lookup_filter_None u'_eq
                            lookup_insert; case; [ done | intro not_in_intro ];
                            by apply (not_in_intro G). }
                          apply (extends_transitive (λ u, default bot ((P ∪ T) !! u))
                                  (λ v, default bot (
                                  (P ∪ <[u:=ε u (λ v' : Tv, default bot ((P ∪ T) !! v'))]>
                                    (T ∪ gset_to_gmap bot (list_to_set ws))) !! v))); [| done ].
                          rewrite extends_lookup; clear is_Some_lookup_v lookup_P v;
                          intros i v lookup_G.
                          do 2 rewrite lookup_union. rewrite lookup_insert_ne.
                          +++ rewrite lookup_union lookup_gset_to_gmap.
                              case (gset_elem_of_dec v (list_to_set ws));
                              [ intro in_ws;     rewrite option_guard_True  |
                                intro not_in_ws; rewrite option_guard_False ]; try done;
                              by case (P !! v), (T !! v).
                          +++ intro u_eq; have: v ∈ G;
                              [ apply (elem_of_list_lookup_2 _ i) | rewrite -u_eq ]; done.
                    ++ revert lookup_T_ws. rewrite (left_id None).
                       do 2 rewrite (lookup_insert_ne _ _ _ _ (not_eq_sym u'_neq)).
                       rewrite lookup_union_Some_raw.
                       case; [| case]; intro lookup_T'; [| intro lookup_ws' ].
                       -- case (map_Forall_T _ _ lookup_T').
                          +++ rewrite eq_cons; case; intros in_us lookup_D; left.
                              rewrite (lookup_union_Some_l _ _ _ _ lookup_D); split; [| done].
                              do 2 rewrite elem_of_app; right; right; revert in_us.
                              rewrite elem_of_cons; by case.
                          +++ clear eq_intro elements_incl.
                              destruct 1 as [Gu' [lookup_D [elements_incl eq_intro]]].
                              right; exists Gu'; rewrite (lookup_union_Some_l _ _ _ _ lookup_D).
                              split; [ done | split ].
                              --- transitivity (elements (dom (P ∪ T)));[done|].
                                  do 2 rewrite dom_union_L. rewrite dom_insert_L dom_union_L.
                                  intros ?. rewrite !elem_of_elements. set_solver+.
                              --- intros F' extends_Gu'; apply eq_intro.
                                  assert (u ∉ Gu') as not_in_Gu'.
                                  { revert lookup_filter; rewrite map_lookup_filter_None.
                                    rewrite lookup_insert_ne; [| done].
                                    rewrite (lookup_union_Some_l _ _ _ _ lookup_D).
                                    case; [ done | intros not_in_Gu'; by apply not_in_Gu' ]. }
                                  apply (extends_transitive (λ u, default bot ((P ∪ T) !! u))
                                    (λ v, default bot (
                                    (P ∪ <[u:=ε u (λ v' : Tv, default bot ((P ∪ T) !! v'))]>
                                      (T ∪ gset_to_gmap bot (list_to_set ws))) !! v))); [| done ].
                                  rewrite extends_lookup; clear is_Some_lookup_v lookup_P v.
                                  intros i v lookup_Gu'.
                                  do 2 rewrite lookup_union; rewrite lookup_insert_ne.
                                  ++++ rewrite lookup_union lookup_gset_to_gmap.
                                       case (gset_elem_of_dec v (list_to_set ws));
                                       [ intro in_ws;     rewrite option_guard_True  |
                                         intro not_in_ws; rewrite option_guard_False ]; try done;
                                       by case (P !! v), (T !! v).
                                  ++++ intro u_eq; have: v ∈ Gu';
                                      [ apply (elem_of_list_lookup_2 _ i) | rewrite -u_eq ]; done.
                       -- revert lookup_ws'; rewrite lookup_gset_to_gmap_Some; case.
                          intros in_ws _; left; split.
                          +++ do 2 rewrite elem_of_app; right; left; set_solver.
                          +++ have: u' ∉ dom D;
                              [ by rewrite same_dom not_elem_of_dom | rewrite not_elem_of_dom ].
                              intro lookup_D; rewrite lookup_union lookup_D lookup_gset_to_gmap.
                              by rewrite option_guard_True.
                + intros φ D' partial_fp u' c'; rewrite lookup_insert_Some; case.
                  - case; intros eq_u; rewrite -eq_u; intros eq_c' in_D'; rewrite -eq_c'.
                    apply (close_transient_inv_after_else_branch _ _ D'); try done.
                    clear in_D' eq_u u' eq_c' c'; intros u' in_D'.
                    case_eq (P !! u'); clear lookup_P; [ intro c' | ]; intro lookup_P.
                    ++ rewrite (_ : (P ∪ T) !! u' = P !! u');
                       [ simpl | rewrite lookup_union lookup_P; by case (T !! u') ].
                       case generally_consistent; intros _ consistent.
                       apply (consistent _ D'); [ | exists c' | ]; done.
                    ++ case_eq (T !! u'); clear lookup_T; [ intro c' | ]; intro lookup_T;
                       rewrite lookup_union lookup_P lookup_T //=;
                       by apply (semi_consistent _ _ partial_fp _ _ lookup_T).
                  - case; intros _; rewrite lookup_union_Some_raw; case.
                    ++ by apply semi_consistent.
                    ++ rewrite lookup_gset_to_gmap_Some; case; intros _; case; intros _ eq_bot _.
                       rewrite -eq_bot; apply bot_spec.
              }
              by iApply ("IH_loop" with "[Post] [Hlocked1] [Hpermanent] [Hloop_inv]").
  Qed.
End Fix.
