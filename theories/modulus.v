From iris.heap_lang     Require Import proofmode notation lang.
From iris.heap_lang.lib Require Import spin_lock.
From spygame            Require Import lists type_repr proph_lib conjunction lock_lib.

Section Mod.

  Context `{!heapGS Σ} `{lock} `{!lockG Σ}.

  Definition modulus : val :=
    λ: "ff", λ: "f",
      let: "m" := ref nil in
      let: "p" := NewProph in
      let: "lk" := newlock #() in
      let: "spy" := λ: "x",
        let: "y" := "f" "x" in
        withLock "lk" (λ: <>,
          "m" <- cons "x" (! "m");;
          resolve_proph_cons "p" "x");;
        "y"
      in
      let: "c" := withProph (λ: <>, "ff" "spy") in
      acquire "lk";;
      resolve_proph_nil "p";;
      ("c", ! "m").

  Definition extends {A B : Type} (F F' : A → B) (G : list A) : Prop :=
    Forall (λ (a : A), F a = F' a) G.

  Lemma extends_self {A B : Type} (F : A → B) (G : list A) :
    extends F F G.
  Proof. induction G; [ apply Forall_nil | apply Forall_cons ]; done. Qed.

  Lemma extends_lookup {A B : Type} (F F' : A → B) (G : list A) :
    extends F F' G ↔ (∀ i a, G !! i = Some a → F a = F' a).
  Proof. unfold extends; by rewrite Forall_lookup. Qed.

  Lemma extends_comm {A B : Type} (F F' : A → B) (G : list A) :
    extends F F' G → extends F' F G.
  Proof. do 2 rewrite extends_lookup; intros H0 i a H1; by rewrite (H0 i a). Qed.

  Lemma extends_transitive {A B : Type} (F1 F2 F3 : A → B) (G : list A) :
    extends F1 F2 G → extends F2 F3 G → extends F1 F3 G.
  Proof.
    do 3 rewrite extends_lookup; intros H0 H1 i a H2.
    trans (F2 a); [ rewrite (H0 i a) | rewrite (H1 i a) ]; done.
  Qed.

  Lemma Forall_reverse {A : Type} (P : A → Prop) (l : list A) :
    Forall P l → Forall P (reverse l).
  Proof.
    induction l; [ by rewrite reverse_nil | ].
    intro; rewrite reverse_cons Forall_app.
    split; [ apply IHl | apply Forall_cons; split; [ | by apply Forall_nil ] ];
    by inversion H0.
  Qed.

  Definition modulus_inv `{Decodable T} m p D ws :=
    (∃ us vs,
       m ↦ (encode' us) ∗ proph_list p vs ∗
       ⌜ Forall D us ⌝ ∗ ⌜ ws = reverse us ++ vs ⌝)%I.

  Lemma modulus_partial_spec `{Da: Decodable Ta, Db: Decodable Tb, Dc: Decodable Tc} ff f FF F D :

    {{{ implements       (Part (Data Ta) (Data Tb))            f  (LibFix.Build_partial F D) ∗
        implements (Func (Func (Data Ta) (Data Tb)) (Data Tc)) ff FF }}}
      modulus ff f
    {{{ (ws : list Ta), RET (encode' (FF F), encode' ws)%V;
        ⌜ Forall D ws ⌝ ∗ ⌜ ∀ F', extends F F' ws → FF F = FF F' ⌝ }}}.

  Proof.
    iIntros (Φ) "[#f_spec #ff_spec] Post"; wp_lam; wp_pures.
    wp_alloc m as "Hm"; wp_pures; wp_apply (wp_new_proph_list Ta); [ done | ].
    iIntros (ws p) "Hp"; wp_pures;
    wp_apply (newlock_spec (modulus_inv m p D ws) with "[Hm Hp]").
    { iExists [], ws; iFrame; iSplit;
      [ iPureIntro; apply Forall_nil | rewrite app_nil_l ]; done. }
    iIntros (lk γ) "#Hlk"; wp_pures.

    iAssert ({{{ True }}}
               ff (λ: "x", let: "y" := f "x" in
                     (withLock lk) (λ: <>, #m <- (cons "x") ! #m;; (resolve_proph_cons #p) "x");;
                     "y")%V
             {{{ y, RET y; True }}})%I as "#spec".
    { iModIntro; clear Φ; iIntros (Φ); iIntros "_ Post".
      wp_apply ("ff_spec" $! _ F); [ | iIntros (y) "->"; by iApply "Post" ].
      iIntros (x n); iModIntro; clear Φ; iIntros (Φ) "-> Post"; wp_pures.
      wp_apply "f_spec"; [ done | iIntros (k) "[-> %]" ].
      simpl in H; rename H into in_dom; simpl; wp_pures.
      wp_apply (withLock_spec γ lk (modulus_inv m p D ws) (True)%I (λ _, True)%I).
      + iModIntro; clear Φ; iIntros (Φ) "[Hinv _] Post".
        iDestruct "Hinv" as (us vs) "(Hm & Hp & % & ->)"; rename H into incl; wp_load.
        wp_apply (cons_spec n); [ done | iIntros "_" ]; wp_store.
        wp_apply ((wp_resolve_proph_cons p vs n) with "Hp").
        iIntros (ws) "[-> Hp]"; iApply "Post"; iSplit; [ | done ].
        iExists (n :: us), ws; iFrame; iPureIntro; split; [ by apply Forall_cons | ].
        rewrite reverse_cons -app_assoc //=.
      + by iSplit.
      + iIntros (y) "_"; wp_pures; by iApply "Post". }

    wp_apply (withProph_spec2 (True)%I
               (λ F' v, extends F F' ws → v = (encode' (FF F'))));
    [|done|].
    + iSplit.
      - iIntros (F'); iModIntro; clear Φ; iIntros (Φ); iIntros "_ Post".
        case (LibLogic.classicT (extends F F' ws));
        [ intro Hextends | intro Hnot_extends ].
        ++ wp_pures. wp_apply ("ff_spec" $! _ F'); [ | iIntros (y) "->"; by iApply "Post" ].
           iIntros (x n); iModIntro; clear Φ; iIntros (Φ) "-> Post"; wp_pures.
           wp_apply "f_spec"; [ done | iIntros (k) "[-> %]" ].
           simpl in H; rename H into in_dom; simpl; wp_pures.
           wp_apply (withLock_spec γ lk (modulus_inv m p D ws) (True)%I (λ _, ⌜ F n = F' n ⌝)%I).
           -- iModIntro. clear Φ; iIntros (Φ) "[Hinv _] Post".
              iDestruct "Hinv" as (us vs) "(Hm & Hp & % & ->)"; rename H into incl; wp_load.
              wp_apply (cons_spec n); [ done | iIntros "_" ]; wp_store.
              wp_apply ((wp_resolve_proph_cons p vs n) with "Hp").
              iIntros (ws) "[-> Hp]"; iApply "Post"; iSplit; [ | iPureIntro ].
              +++ iExists (n :: us), ws; iFrame; iPureIntro; split; [ by apply Forall_cons | ].
                  rewrite reverse_cons -app_assoc //=.
              +++ revert Hextends; unfold extends.
                  rewrite Forall_app; case; intros _; apply Forall_inv.
           -- by iSplit.
           -- iIntros (y) "->"; wp_pures; by iApply "Post".
        ++ wp_pures. wp_apply "spec";
           [ | iIntros (y) "_"; iApply "Post"; iPureIntro; intro Hextends ]; done.
      - iModIntro. iIntros (Φ') "_ Post". wp_lam. by wp_apply "spec".

    + iIntros (c extends_strong); wp_pures.
      wp_pures; wp_bind (acquire _); wp_apply (acquire_spec with "Hlk").
      iIntros "[Hlocked Hinv]"; iDestruct "Hinv" as (us vs) "(Hm & Hp & % & ->)".
      rename H into incl. wp_pures.
      wp_apply (wp_resolve_proph_nil p vs with "Hp"); iIntros "->"; wp_load; wp_pures.
      assert (c = (encode' (FF F))) as c_eq. { apply extends_strong, extends_self. }
      rewrite c_eq; iApply "Post"; iPureIntro; split; [ done | ].
      rewrite app_nil_r c_eq //= in extends_strong.
      intros F' extends; by apply encode_injective, extends_strong, Forall_reverse.
  Qed.

  Lemma modulus_spec `{Da: Decodable Ta, Db: Decodable Tb, Dc: Decodable Tc} ff f FF F :

    {{{ implements       (Func (Data Ta) (Data Tb))            f  F ∗
        implements (Func (Func (Data Ta) (Data Tb)) (Data Tc)) ff FF }}}
      modulus ff f
    {{{ (ws : list Ta), RET (encode' (FF F), encode' ws)%V;
        ⌜ ∀ F', extends F F' ws → FF F = FF F' ⌝ }}}.

  Proof.
    iIntros (Φ) "[#f_spec #ff_spec] Post".
    iApply (modulus_partial_spec _ _ FF F (λ _, True)).
    iSplit; [ iApply (implements_func_entails_implements_part _ _ f F) | ]; done.
    iNext; iIntros (ws) "[_ %]"; by iApply "Post".
  Qed.

End Mod.
