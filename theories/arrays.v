(* Author: Paulo Emílio de Vilhena.
   Date: 15/04/2019.
*)

(* Arrays
*)

From iris.proofmode     Require Import proofmode.
From iris.base_logic    Require Import invariants.
From iris.program_logic Require Import weakestpre.
From iris.heap_lang     Require Import proofmode notation lang.

From spygame Require Import type_repr lists.

(* We introduce a type class to work with head_lang code modelling an array.
   The definitions are not original, they come from Esben Glavind Clausen's
   Master's thesis, which can be found in the main page of the Iris Project.
*)

Class Array Σ `{!heapGS Σ} := {
  make_array   : val;
  array_load   : val;
  array_store  : val;
  array_length : val;

  is_array {A} : val → list A → iProp Σ;

  make_array_spec {A} `{Ea : Encodable A} (n : nat) (u : A) :
    {{{ True }}}
      make_array #n (encode' u)
    {{{ (a : val), RET a; is_array a (replicate n u)}}};

  array_load_spec {A} `{Ea : Encodable A} (a : val) (n : nat) (u : A) (us : list A) :
    us !! n = Some u ->
    {{{ ▷ is_array a us }}}
      array_load a #n
    {{{ RET (encode' u); is_array a us }}};

  array_store_spec {A} `{Ea : Encodable A} (a : val) (n : nat) (u : A) (us : list A) :
    {{{ ▷ is_array a us }}}
      array_store a #n (encode' u)
    {{{ RET #(); is_array a (<[ n := u ]> us) }}} ;

  array_length_spec  {A} `{Ea : Encodable A} (a : val) (us : list A) :
    {{{ ▷ is_array a us }}}
      array_length a
    {{{ RET #(length us)%nat; is_array a us }}} ;
}.

Section ArrayDefs.

  Context `{Array Σ}.

  Definition foldl_aux : val :=
    rec: "foldl_aux" "f" "acc" "a" "n" :=
      if: "n" < array_length "a" then
        "foldl_aux" "f" ("f" "acc" (array_load "a" "n")) "a" ("n" + #1)
      else "acc".

  Definition foldl : val :=
    λ: "f" "init" "a", foldl_aux "f" "init" "a" #0.

  Definition iter : val :=
    λ: "f" "a",
      let: "_" :=
         foldl (λ: "acc" "u", "f" "acc" "u";; "acc" + #1) #0 "a" in
      #().

  Definition array_inserts : val :=
    λ: "a", λ: "a'",
      let: "len" := array_length "a'" in
      iter (λ: "n" "u",
        if: "n"%nat < "len"%nat then array_store "a'" "n" "u" else #()) "a".

  Definition array_copy : val :=
    λ: "a",
      let: "a'" := make_array (array_length "a") (array_load "a" #0%nat) in
      array_inserts "a" "a'";;
      "a'".

  Definition array_of_list : val :=
    λ: "u" "us",
      let: "a" :=
        make_array (list_length "us") "u" in
      list_iter (λ: "n" "u", array_store "a" "n" "u") "us";;
      "a".

  Lemma foldl_aux_spec `{Ea : Encodable A} (f init a : val) (us vs : list A)
        (I : val → list A → iProp Σ) :
    {{{ (∀ (n : nat) (acc : val) (u : A),
           {{{ I acc (take n (us ++ vs)) ∗ ⌜ (us ++ vs) !! n = Some u ⌝ }}}
             f acc (encode' u)
           {{{ (acc' : val), RET acc'; I acc' (take (S n) (us ++ vs)) }}}) ∗
        is_array a (us ++ vs) ∗
        I init us }}}
      foldl_aux f init a #(length us)
    {{{ (acc : val), RET acc; is_array a (us ++ vs) ∗ I acc (us ++ vs) }}}.
  Proof.
    iIntros (Φ) "[#f_spec [Ha Hinit]] Post".
    iInduction (vs) as [| w ws ] "IH" forall (us init).
    - rewrite app_nil_r. unfold foldl_aux.
      wp_apply (array_length_spec with "Ha"). iIntros "Ha". wp_pures.
      rewrite bool_decide_false; [|apply Z.lt_irrefl]. wp_pures. iApply "Post". iFrame.
    - unfold foldl_aux. wp_apply (array_length_spec with "Ha"). iIntros "Ha". wp_pures.
      rewrite bool_decide_true; [|rewrite app_length //=; lia]. wp_pures.
      wp_apply (array_load_spec with "Ha"); [by apply list_lookup_middle|]. iIntros "Ha".
      wp_apply ("f_spec" with "[Hinit]").
      { iSplit;[|iPureIntro; by apply list_lookup_middle]. by rewrite take_app. }
      iIntros (acc') "Iacc'". rewrite cons_middle app_assoc.
      rewrite (_: length us + 1 = length (us ++ [w]));[|rewrite app_length //=; lia].
      iApply ("IH" with "[f_spec] [Ha] [Iacc']"); try done.
      rewrite take_app_alt; [|rewrite app_length //=; lia]; done.
  Qed.

  Lemma foldl_spec `{Ea : Encodable A} (f init a : val) (us : list A)
        (I : val → list A → iProp Σ) :
    {{{ (∀ (n : nat) (acc : val) (u : A),
           {{{ I acc (take n us) ∗ ⌜ us !! n = Some u ⌝ }}}
             f acc (encode' u)
           {{{ (acc' : val), RET acc'; I acc' (take (S n) us) }}}) ∗
        is_array a us ∗
        I init [] }}}
      foldl f init a
    {{{ (acc : val), RET acc; is_array a us ∗ I acc us }}}.
  Proof.
    rewrite (_: us = [] ++ us);[|by simpl].
    iIntros (Φ) "[#f_spec [Ha Hinit]] Post". wp_lam. wp_pures.
    rewrite (_ : 0 = (length ([] : list A)));[|by simpl].
    wp_apply (foldl_aux_spec _ _ _ [] us I  with "[f_spec Ha Hinit]"); by iFrame.
  Qed.

  Lemma foldl_spec' `{Ea : Encodable A} (f init a : val) (us : list A)
        (I : val → list A → iProp Σ) :
    {{{ (∀ (acc : val) (v : A) (vs : list A),
           {{{ I acc vs }}}
             f acc (encode' v)
           {{{ (acc' : val), RET acc'; I acc' (vs ++ [v]) }}}) ∗
        is_array a us ∗
        I init [] }}}
      foldl f init a
    {{{ (acc : val), RET acc; is_array a us ∗ I acc us }}}.
  Proof.
    iIntros (Φ) "[#f_spec [Ha Iinit]] Post".
    wp_apply (foldl_spec f init a us I with "[Ha Iinit]"); iFrame.
    iIntros (n acc u). iModIntro. clear Φ; iIntros (Φ) "[Hacc %] Post".
    wp_apply ("f_spec" $! acc u (take n us) with "Hacc").
    rewrite (_ : take n us ++ [u] = take (S n) us); [ done | ].
    destruct (lookup_elim _ _ _ H0) as [vs [ ws [ H1 H2 ] ] ].
    rewrite H1 (cons_middle u _ ws) take_app_alt; [ | done ].
    rewrite app_assoc take_app_alt; [ done | rewrite app_length; simpl; lia ].
  Qed.

  Lemma iter_spec `{Ea : Encodable A} (f a : val) (us : list A) (I : nat → iProp Σ) :
    {{{ (∀ (n : nat) (u : A),
           {{{ I n ∗ ⌜ us !! n = Some u ⌝ }}} f #n (encode' u) {{{ RET #(); I (S n) }}}) ∗
        is_array a us ∗
        I 0%nat }}}
      iter f a
    {{{ RET #(); is_array a us ∗ I (length us) }}}.
  Proof.
    iIntros (Φ) "[#f_spec [Hl I0]] Post"; wp_lam; wp_let.
    wp_apply (foldl_spec _ _ a us (λ acc vs,
      ⌜ acc = #(length vs) ⌝ ∗ I (length vs))%I with "[Hl I0]").
    + iFrame; iSplit; [ | done ].
      iIntros (n acc u); iModIntro; clear Φ; iIntros (Φ) "[[% In] %] Post".
      destruct (lookup_elim _ _ _ H1) as [vs [ ws [ H2 H3 ] ] ]; rewrite H0.
      rewrite (_ : take n us = vs); [ rewrite H3 | rewrite H2; by apply take_app_alt ].
      rewrite (_ : take (S n) us = vs ++ [u]).
      rewrite (_ : length (vs ++ [u]) = S n); [ | rewrite app_length H3 //=; lia ].
      - wp_pures; wp_apply ("f_spec" with "[In]"); [ by iFrame | iIntros "ISn" ].
        wp_pures; iApply "Post"; iSplit; [ | done ].
        iPureIntro; rewrite (_ : n + 1 = S n); [ done | lia ].
      - rewrite H2 cons_middle app_assoc take_app_alt; [ done | ].
        rewrite app_length H3 //=; lia.
    + iIntros (acc) "[Hl [-> Ius]]"; wp_pures; iApply "Post"; by iFrame.
  Qed.

  Lemma iter_spec' `{Ea : Encodable A} (f a : val) (us : list A) (I : list A → iProp Σ) :
    {{{ (∀ (v : A) (vs : list A),
           {{{ I vs }}} f #(length vs) (encode' v) {{{ RET #(); I (vs ++ [v]) }}}) ∗
        is_array a us ∗
        I [] }}}
      iter f a
    {{{ RET #(); is_array a us ∗ I us }}}.
  Proof.
    iIntros (Φ) "[#f_spec [Hnil Ha]] Post".
    wp_apply (iter_spec f a us (λ n, I (take n us)) with "[Ha Hnil]");
      [ iFrame | by rewrite firstn_all ].
    iIntros; iModIntro; clear Φ; iIntros (Φ) "[In %] Post".
    destruct (lookup_elim _ _ _ H0) as [vs [ ws [ H1 H2 ] ] ]; rewrite H1.
    rewrite (cons_middle u _ ws) take_app_alt; [ | done ].
    rewrite app_assoc take_app_alt; [ | rewrite app_length H2 //=; lia]; rewrite -H2.
    by wp_apply ("f_spec" with "In").
  Qed.

  Lemma array_inserts_spec `{Ea : Encodable A} (a a' : val) (us vs : list A) :
    {{{ is_array a us ∗ is_array a' vs }}}
      array_inserts a a'
    {{{ RET #(); is_array a us ∗ is_array a' (list_inserts 0%nat us vs) }}}.
  Proof.
    iIntros (Φ) "[Ha Ha'] Post"; wp_lam; wp_pures.
    wp_apply (array_length_spec with "Ha'"); iIntros "Ha'"; wp_pures.
    wp_apply (iter_spec' _ a us (λ ws, is_array a' (list_inserts 0 ws vs)) with "[Ha Ha']");
      iFrame.
    iIntros (w ws); iModIntro; clear Φ; iIntros (Φ) "Hinserts Post"; wp_pures.
    case (Z.lt_ge_dec (length ws) (length vs)); iIntros "%".
    - rewrite (bool_decide_true _ l); wp_if.
      wp_apply (array_store_spec with "Hinserts"); iIntros "Hinserts"; iApply "Post".
      rewrite list_inserts_app_l Nat.add_0_r //=.
    - rewrite (bool_decide_false _ g); wp_if.
      iApply "Post"; rewrite list_inserts_app_l //=.
      have : (length vs ≤ length ws)%nat; [ lia | ]; intro H2.
      rewrite Nat.add_0_r; rewrite -(inserts_length vs 0 ws) in H2.
      by rewrite (list_insert_ge _ _ _ H2).
  Qed.

  Lemma array_copy_spec `{Ea : Encodable A} (a : val) (us : list A) :
    length us > 0 →
      {{{ is_array a us }}}
        array_copy a
      {{{ (a' : val), RET a'; is_array a us ∗ is_array a' us }}}.
  Proof.
    case us; [ done | ].
    iIntros (v vs H1 Φ) "Ha Post"; wp_lam; wp_pures.
    wp_apply (array_load_spec with "Ha"); [ by simpl | iIntros "Ha" ].
    wp_apply (array_length_spec with "Ha"); iIntros "Ha".
    wp_apply (make_array_spec); [ done | iIntros (a') "Ha'" ]; wp_pures.
    wp_apply (array_inserts_spec with "[Ha Ha']"); [ iFrame | iIntros "[Ha Ha']" ]; wp_pures.
    iApply "Post"; iFrame.
    rewrite -(app_nil_r (replicate _ _)).
    rewrite list_inserts_0_r; [rewrite app_nil_r | rewrite replicate_length ]; done.
  Qed.

  Lemma array_of_list_spec `{Ea : Encodable A} (u : A) (us : list A) :
    {{{ True }}} array_of_list (encode' u) (encode' us) {{{ (a : val), RET a; is_array a us }}}.
  Proof.
    iIntros (Φ) "_ Post". wp_lam.
    wp_apply list_length_spec; [done|]. iIntros "_".
    wp_apply (make_array_spec (length us) u); [done|].
    iIntros (a) "Ha". wp_pures.
    wp_apply (list_iter_spec _ us (λ n, (∃ ws,
      is_array a ((take n us) ++ ws) ∗ ⌜ (length us)%nat = (length ws + n)%nat ⌝)%I)
      with "[Ha]").
    - iSplit.
      + iIntros (n v). iModIntro. clear Φ. iIntros (Φ) "[Hws %] Post". 
        iDestruct "Hws" as (ws) "[Ha %]". wp_pures.
        destruct (lookup_elim _ _ _ H0) as [xs [ys [-> xs_len]]].
        rewrite (_: take n (xs ++ _) = xs);[|by rewrite -xs_len take_app].
        wp_apply (array_store_spec with "Ha").
        case_eq ws.
        -- intros ->. revert H1. rewrite app_length xs_len //=. lia.
        -- intros w ws' ->.
           rewrite (_: <[n := v]> (xs ++ w :: ws') = xs ++ v :: ws').
           ++ iIntros "Ha". iApply "Post". iExists ws'. iSplit.
              --- rewrite (_: take (S n) (xs ++ v :: ys) = xs ++ [v]);
                  rewrite cons_middle app_assoc //=. rewrite take_app_alt; [done|].
                  rewrite app_length //=. lia.
              --- iPureIntro. revert H1. rewrite app_length xs_len. simpl. lia.
           ++ rewrite -xs_len. rewrite (_: length xs = (length xs + 0)%nat); [|lia].
              rewrite insert_app_r //=.
      + iExists (replicate (length us) u). iFrame. by rewrite replicate_length.
    - iDestruct (1) as (vs) "[Ha %]". wp_pures. iApply "Post".
      rewrite (_: vs = []). by rewrite firstn_all app_nil_r.
      { rewrite -length_zero_iff_nil. revert H0. lia. }
  Qed.

End ArrayDefs.
