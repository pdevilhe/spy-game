From iris.proofmode     Require Import proofmode.
From iris.program_logic Require Import weakestpre.
From iris.heap_lang     Require Import proofmode notation lang.

From spygame Require Import type_repr.


Section ListDefs.

  Definition nil : val := NONEV.

  Definition cons : val :=
    λ: "u" "us", SOME ("u", "us").

  Definition list_foldl : val :=
    rec: "foldl" "f" "init" "l" :=
      match: "l" with
        NONE     => "init"
      | SOME "p" =>
        "foldl" "f" ("f" "init" (Fst "p")) (Snd "p")
      end.

  Definition list_ifoldl : val :=
    λ: "f" "init" "us",
      Fst (list_foldl (λ: "acc" "u",
                  ("f" (Fst "acc") (Snd "acc") "u", (Snd "acc" + #1))) ("init", #0) "us").

  Definition list_length : val :=
    λ: "us", list_foldl (λ: "n" <>, "n" + #1%Z) #0%Z "us".

  Definition list_reverse : val :=
    λ: "us", list_foldl (λ: "acc" "u", cons "u" "acc") nil "us".

  Definition list_foldr : val :=
    λ: "f" "init" "us",
      list_foldl (λ: "acc" "u", "f" "u" "acc") "init" (list_reverse "us").

  Definition list_ifoldr : val :=
    λ: "f" "init" "us",
      list_ifoldl (λ: "acc" "n" "u", "f" "n" "u" "acc") "init" (list_reverse "us").

  Definition list_append : val :=
    λ: "l" "s", list_foldr (λ: "u" "acc", cons "u" "acc") "s" "l".

  Definition list_imap : val :=
    λ: "f" "us", list_reverse (list_ifoldl (λ: "acc" "n" "u", cons ("f" "n" "u") "acc") nil "us").

  Definition list_iter : val :=
    λ: "f" "us", list_ifoldl (λ: <> "n" "u", "f" "n" "u") #() "us".

  Definition list_lookup : val :=
    rec: "lookup" "us" "n" :=
      match: "us" with
        NONE     => #()
      | SOME "p" =>
        if: "n" = #0%nat then
          Fst "p"
        else
          "lookup" (Snd "p") ("n" - #1)
      end.

  Definition list_insert : val :=
    λ: "us" "n" "v",
      list_imap (λ: "m" "u", if: "n" = "m" then "v" else "u") "us".

  Definition list_mem (eq : val) : val :=
    λ: "x" "ys", list_foldl (λ: "acc" "y", if: "acc" then #true else eq "x" "y") #false "ys".

  Definition list_filter : val :=
    λ: "f" "us", list_foldr (λ: "u" "acc", if: "f" "u" then cons "u" "acc" else "acc") nil "us".

  Definition list_replicate : val :=
    rec: "list_replicate" "n" "u" :=
      if: "n" = #0%nat then nil else cons "u" ("list_replicate" ("n" - #1%nat) "u").

End ListDefs.


Section ListSpec.

  Context `{!heapGS Σ}.

  Lemma cons_spec `{Encodable T} u us :
    {{{ True }}} cons (encode' u) (encode' us) {{{ RET (encode' (u :: us)); True }}}.
  Proof. iIntros (Φ) "_ Post"; wp_lam; wp_pures; rewrite encode_cons; by iApply "Post". Qed.

  Lemma list_foldl_spec_aux `{Encodable T} (f init : val) us vs I :
    {{{ (∀ n acc u,
           {{{ I acc (take n (us ++ vs)) ∗ ⌜ (us ++ vs) !! n = Some u ⌝ }}}
             f acc (encode' u)
           {{{ (acc' : val), RET acc'; I acc' (take (S n) (us ++ vs)) }}}) ∗
        I init us }}}
      list_foldl f init (encode' vs)
    {{{ (acc : val), RET acc; I acc (us ++ vs) }}}.
  Proof.
    iInduction vs as [| w ws ] "IH" forall (us init);
    iIntros (Φ) "[#f_spec Iinit] Post"; wp_lam; wp_pures.
    + iApply "Post"; rewrite app_nil_r //=; by iFrame.
    + wp_apply ("f_spec" $! (length us) init w with "[Iinit]").
      { rewrite take_app_length; iFrame; iPureIntro; by apply list_lookup_middle. }
      iIntros (acc') "Iacc'"; rewrite cons_middle app_assoc.
      wp_apply ("IH" $! (us ++ [w]) acc' with "[Iacc']").
      - rewrite (take_app_length' (us ++ [w]) ws); [ by iFrame |
        rewrite app_length; simpl; by lia ].
      - iIntros (acc) "Iacc"; iApply "Post"; iFrame.
  Qed.

  Lemma list_foldl_spec `{Encodable T} (f init : val) us I :
    {{{ (∀ n acc u,
           {{{ I acc (take n us) ∗ ⌜ us !! n = Some u ⌝ }}}
             f acc (encode' u)
           {{{ (acc' : val), RET acc'; I acc' (take (S n) us) }}}) ∗
        I init [] }}}
      list_foldl f init (encode' us)
    {{{ (acc : val), RET acc; I acc us }}}.
  Proof.
    rewrite -(app_nil_l us) (_: encode' ([] ++ us) = encode' us);
      [ apply (list_foldl_spec_aux f init [] us) | simpl ]; done.
  Qed.

  Lemma list_foldl_spec' `{Encodable T} (f init : val) us I :
    {{{ (∀ acc v vs,
           {{{ I acc vs }}}
             f acc (encode' v)
           {{{ (acc' : val), RET acc'; I acc' (vs ++ [v]) }}}) ∗
        I init [] }}}
      list_foldl f init (encode' us)
    {{{ (acc : val), RET acc; I acc us }}}.
  Proof.
    iIntros (Φ) "[#f_spec Iinit] Post".
    wp_apply (list_foldl_spec f init us I with "[Iinit]"); iFrame.
    iIntros (n acc u); iModIntro; clear Φ; iIntros (Φ) "[Hacc %] Post".
    wp_apply ("f_spec" $! acc u (take n us) with "Hacc").
    by rewrite -take_S_r.
  Qed.

  Lemma list_ifoldl_spec `{Encodable T} (f init : val) us I :
    {{{ (∀ n acc u,
           {{{ I acc (take n us) ∗ ⌜ us !! n = Some u ⌝ }}}
             f acc #n (encode' u)
           {{{ (acc' : val), RET acc'; I acc' (take (S n) us) }}}) ∗
        I init [] }}}
      list_ifoldl f init (encode' us)
    {{{ (acc : val), RET acc; I acc us }}}.
  Proof.
    iIntros (Φ) "[#f_spec Iinit] Post"; wp_lam; wp_pures.
    wp_apply (list_foldl_spec _ _ us (λ acc vs, ∃ (acc' : val),
      ⌜ acc = (acc' , #(length vs))%V ⌝ ∗ I acc' vs)%I with "[Iinit]").
    + iSplit; [ | iExists init; by iFrame ]; iIntros (n acc u); iModIntro; clear Φ.
      iIntros (Φ) "[Hacc' %] Post"; iDestruct "Hacc'" as (acc') "[-> Iacc']"; wp_pures.
      assert (n < length us)%nat by (apply lookup_lt_is_Some_1; rewrite H0; eauto).
      iSpecialize ("f_spec" $! n acc' u); simplify_eq; rewrite !length_take_le; [|lia..].
      wp_apply ("f_spec" with "[Iacc']"); [ by iFrame | ].
      iIntros (acc) "Iacc"; wp_pures; iApply "Post"; iExists acc; iFrame; iPureIntro.
      repeat f_equal. lia.
    + iIntros (acc) "Hacc'"; iDestruct "Hacc'" as (acc') "[-> Iacc']"; wp_pures; by iApply "Post".
  Qed.

  Lemma list_ifoldl_spec' `{Encodable T} (f init : val) us I :
    {{{ (∀ acc v vs,
           {{{ I acc vs }}}
             f acc #(length vs) (encode' v)
           {{{ (acc' : val), RET acc'; I acc' (vs ++ [v]) }}}) ∗
        I init [] }}}
      list_ifoldl f init (encode' us)
    {{{ (acc : val), RET acc; I acc us }}}.
  Proof.
    iIntros (Φ) "[#f_spec Iinit] Post"; wp_lam; wp_pures.
    wp_apply (list_foldl_spec' _ _ us (λ acc vs, ∃ (acc' : val),
      ⌜ acc = (acc' , #(length vs))%V ⌝ ∗ I acc' vs)%I with "[Iinit]").
    + iSplit; [ | iExists init; by iFrame ]; iIntros (acc v vs); iModIntro; clear Φ.
      iIntros (Φ) "Hacc' Post"; iDestruct "Hacc'" as (acc') "[-> Iacc']"; wp_pures.
      wp_apply ("f_spec" with "Iacc'"); iIntros (acc'') "Iacc''"; wp_pures; iApply "Post".
      iExists acc''; iFrame; iPureIntro; rewrite app_length //=. repeat f_equal. lia.
    + iIntros (acc) "Hacc'"; iDestruct "Hacc'" as (acc') "[-> Iacc']"; wp_pures; by iApply "Post".
  Qed.

  Lemma list_length_spec `{Encodable T} us :
    {{{ True }}} list_length (encode' us) {{{ RET #(length us); True }}}.
  Proof.
    iIntros (Φ) "_ Post"; wp_lam; wp_pures.
    wp_apply (list_foldl_spec' _ _ us (λ acc vs, ⌜ acc = #(length vs)%nat ⌝)%I).
    + iFrame; iSplit; [ iIntros (acc v vs) | by iPureIntro ].
      iModIntro; clear Φ; iIntros (Φ) "% Post"; iSimplifyEq; wp_pures.
      rewrite app_length; simpl; iApply "Post"; iPureIntro.
      repeat f_equal. lia.
    + iIntros (acc) "%"; iSimplifyEq; by iApply "Post".
  Qed.

  Lemma list_reverse_spec `{Encodable T} us :
    {{{ True }}} list_reverse (encode' us) {{{ RET (encode' (reverse us)); True }}}.
  Proof.
    iIntros (Φ) "_ Post"; wp_lam; wp_pures.
    wp_apply (list_foldl_spec' _ _ us (λ acc vs, ⌜ acc = encode' (reverse vs) ⌝)%I);
     [ iSplit; [ | by rewrite reverse_nil ] | iIntros (acc) "%"; iSimplifyEq; by iApply "Post" ].
    iIntros (acc v vs); iModIntro; clear Φ; iIntros (Φ) "% Post"; iSimplifyEq.
    wp_pures; wp_apply cons_spec; [ done | iIntros "_"; iApply "Post" ]; by rewrite reverse_snoc.
  Qed.

  Lemma list_foldr_spec `{Encodable T} (f init : val) us I :
    {{{ (∀ v acc vs,
           {{{ I acc vs }}}
             f (encode' v) acc
           {{{ (acc' : val), RET acc'; I acc' (v :: vs) }}}) ∗
        I init [] }}}
      list_foldr f init (encode' us)
    {{{ (acc : val), RET acc; I acc us }}}.
  Proof.
    iIntros (Φ) "[#f_spec I0] Post"; wp_lam; wp_pures.
    wp_apply list_reverse_spec; [ done | iIntros "_"]; wp_pures.
    wp_apply (list_foldl_spec' _ _ (list.reverse us) (λ acc vs, I acc (list.reverse vs))
                with "[I0]"); iFrame.
    + iIntros (acc v vs); iModIntro; clear Φ; iIntros (Φ) "Iacc Post"; wp_pures.
      wp_apply ("f_spec" with "Iacc"); by rewrite reverse_snoc.
    + rewrite reverse_involutive; iIntros (acc) "Iacc"; iApply "Post"; by iFrame.
  Qed.

  Lemma list_ifoldr_spec `{Encodable T} (f init : val) us I :
    {{{ (∀ acc v vs,
           {{{ I acc vs }}}
             f #(length vs) (encode' v) acc
           {{{ (acc' : val), RET acc'; I acc' (v :: vs) }}}) ∗
        I init [] }}}
      list_ifoldr f init (encode' us)
    {{{ (acc : val), RET acc; I acc us }}}.
  Proof.
    iIntros (Φ) "[#f_spec Iinit] Post"; wp_lam; wp_pures.
    wp_apply list_reverse_spec; [ done | iIntros "_" ]; wp_pures.
    wp_apply (list_ifoldl_spec' _ init _ (λ acc vs, I acc (list.reverse vs)) with "[Iinit]").
    + iFrame; iIntros (acc v vs); iModIntro; clear Φ; iIntros (Φ) "Iacc Post"; wp_pures.
      rewrite -length_reverse reverse_app; by wp_apply ("f_spec" with "Iacc").
    + by rewrite reverse_involutive.
  Qed.

  Lemma list_append_spec `{Encodable T} us vs :
    {{{ True }}} list_append (encode' us) (encode' vs) {{{ RET (encode' (us ++ vs)); True }}}.
  Proof.
    iIntros (Φ) "_ Post"; wp_lam; wp_pures.
    wp_apply (list_foldr_spec _ _ us (λ acc us', ⌜ acc = encode' (us' ++ vs) ⌝)%I).
    + iSplit; [ | done ]; iIntros (v acc us'); iModIntro; clear Φ; iIntros (Φ) "-> Post".
      wp_pures; wp_apply cons_spec; [ done | iIntros "_" ]; by iApply "Post".
    + iIntros (acc) "->"; by iApply "Post".
  Qed.

  Lemma list_imap_spec `{Ea : Encodable A, Eb: Encodable B} (f : val) us (F : nat → A → B) I :
    {{{ (∀ n u,
           {{{ I n ∗ ⌜ us !! n = Some u ⌝ }}}
             f #n (encode' u)
           {{{ RET (encode' (F n u)); I (S n) }}}) ∗
        I 0%nat }}}
      list_imap f (encode' us)
    {{{ RET (encode' (imap F us)); I (length us) }}}.
  Proof.
    iIntros (Φ) "[#f_spec I0] Post"; wp_lam; wp_pures.
    wp_apply (list_ifoldl_spec _ _ us (λ acc vs,
      ⌜ acc = (encode' (list.reverse (list.imap F vs)))%V ⌝ ∗
      I (length vs))%I
    with "[I0]").
    + iFrame; iSplit; [ | done ].
      iIntros (n acc u); iModIntro; clear Φ; iIntros (Φ) "[[-> In] %] Post".
      assert (n < length us)%nat by (apply lookup_lt_is_Some_1; rewrite H; eauto).
      rewrite (take_S_r _ _ u) // imap_app reverse_app /= app_length
              !length_take_le; [|lia..].
      wp_pures. wp_apply ("f_spec" with "[In]"); iFrame; [ done | ].
      iIntros "ISn"; wp_apply cons_spec; [ done | iIntros "_" ].
      iApply "Post".
      rewrite Nat.add_0_r (_:n + length [u]=S n)%nat /=; [|lia]. auto.
    + iIntros (acc) "[-> Hacc]"; wp_pures.
      wp_apply list_reverse_spec; [ done | iIntros "_" ].
      rewrite reverse_involutive; by iApply "Post".
  Qed.

  Lemma list_iter_spec `{E : Encodable T} (f : val) us I :
    {{{ (∀ n u,
           {{{ I n ∗ ⌜ us !! n = Some u ⌝ }}} f #n (encode' u) {{{ RET #(); I (S n) }}}) ∗
        I 0%nat }}}
      list_iter f (encode' us)
    {{{ RET #(); I (length us) }}}.
  Proof.
    iIntros (Φ) "[#f_spec I0] Post"; wp_lam; wp_pures.
    wp_apply (list_ifoldl_spec _ _ us (λ acc vs, ⌜ acc = #() ⌝ ∗ I (length vs))%I with "[I0]").
    + iFrame; iSplit; [ | done ].
      iIntros (n acc u); iModIntro; clear Φ; iIntros (Φ) "[[% In] %] Post".
      assert (n < length us)%nat by (apply lookup_lt_is_Some_1; rewrite H0; eauto).
      rewrite !length_take_le; [|lia..].
      wp_pures; wp_apply ("f_spec" with "[In]"); [ by iFrame | iIntros "ISn" ].
      iApply "Post"; iSplit; done.
    + iIntros (acc) "[-> Ius]"; iApply "Post"; by iFrame.
  Qed.

  Lemma list_iter_spec' `{E : Encodable T} (f : val) us I :
    {{{ (∀ v vs,
           {{{ I vs }}} f #(length vs) (encode' v) {{{ RET #(); I (vs ++ [v]) }}}) ∗
        I [] }}}
      list_iter f (encode' us)
    {{{ RET #(); I us }}}.
  Proof.
    iIntros (Φ) "[#f_spec I0] Post"; wp_lam; wp_pures.
    wp_apply (list_ifoldl_spec' _ _ us (λ acc vs, ⌜ acc = #() ⌝ ∗ I vs)%I with "[I0]").
    + iFrame; iSplit; [ | done ].
      iIntros (acc v vs). iModIntro. clear Φ. iIntros (Φ) "[-> Ivs] Post".
      wp_pures. wp_apply ("f_spec" with "Ivs"). iIntros "Ivvs". iApply "Post". by iFrame.
    + iIntros (acc) "[-> Ius]". by iApply "Post".
  Qed.

  Lemma list_lookup_spec `{E : Encodable T} (n : nat) (u : T) (us : list T) :
    us !! n = Some u →
      {{{ True }}} list_lookup (encode' us) #n {{{ RET (encode' u); True }}}.
  Proof.
    iInduction n as [| m] "IH" forall (us);
    try (case us; [ simpl; by iIntros "%" | iIntros (v vs H Φ) "Hl Post" ];
         wp_lam; wp_pures).
    + injection H; iIntros "%"; iSimplifyEq; by iApply "Post".
    + rewrite (_ : (S m - 1)%Z = m); [ | lia ].
      wp_apply ("IH" $! vs); [ by inversion H | by iFrame | iIntros "Hl'" ]; by iApply "Post".
  Qed.

  Lemma list_insert_spec `{E : Encodable T} (n : nat) (v : T) (us : list T) :
    {{{ True }}}
      list_insert (encode' us) #n (encode' v)
    {{{ RET (encode' (<[ n := v ]> us)); True }}}.
  Proof.
    iIntros (Φ) "_ Post"; wp_lam; wp_pures.
    wp_apply (list_imap_spec _ us
               (λ m u, if (bool_decide (n = m)%nat) then v else u) (λ _, True)%I); [| iIntros "_"].
    + iSplit; [ | done ]; iIntros (m u); iModIntro; clear Φ; iIntros (Φ) "[_ %] Post"; wp_pures.
      case (Nat.eq_dec n m); intro H1;
        [ rewrite (bool_decide_true  _ H1) bool_decide_true  |
          rewrite (bool_decide_false _ H1) bool_decide_false ];
      try intro; try (by simplify_eq); wp_pures; by iApply "Post".
    + rewrite (_ : <[n:=v]> us = imap (λ m u, if bool_decide (n = m) then v else u) us);
      [ by iApply "Post" | ].
      generalize us; clear us; induction n; intro us.
      - case us as [ | u us ]; [ done | simpl ];
        rewrite (_ : imap ((λ m u', if bool_decide (0%nat = m) then v else u') ∘ S) us = us);
        [ | induction us; [ | simpl; rewrite IHus ] ]; done.
      - case us as [ | u us ]; [ done | simpl ]. rewrite IHn.
        rewrite ( _ : imap  (λ m u', if bool_decide (n   = m) then v else u')      us =
                      imap ((λ m u', if bool_decide (S n = m) then v else u') ∘ S) us); [ done | ].
        apply imap_ext; intros i u' _; simpl.
        case (Nat.eq_dec n i); intro H;
          [ rewrite (bool_decide_true  _ H) bool_decide_true  |
            rewrite (bool_decide_false _ H) bool_decide_false ]; try intro; try by simplify_eq.
  Qed.

  Lemma list_mem_spec `{D : Encodable T} `{EqDecision T} (eq : val) (u : T) (us : list T) :
    (∀ x y : T, {{{ True }}} eq (encode' x) (encode' y)
                {{{ RET #(bool_decide (x = y)); True}}}) →
    {{{ True }}}
      list_mem eq (encode' u) (encode' us)
    {{{ RET #(bool_decide (u ∈ us)); True }}}.
  Proof.
    iIntros (Heq Φ) "_ Post"; wp_lam; wp_pures.
    wp_apply (list_foldl_spec' _ _ us (λ acc vs, ⌜ acc = #(bool_decide (u ∈ vs)) ⌝)%I);
      [ iSplit | iIntros (acc) "->"; by iApply "Post" ].
    - iIntros (acc v vs); iModIntro; clear Φ; iIntros (Φ) "-> Post"; wp_pures.
      case (elem_of_list_dec u vs); intro H0;
        [ rewrite (bool_decide_true _ H0) | rewrite (bool_decide_false _ H0) ];
        wp_pures.
      + iApply "Post"; iPureIntro.
        rewrite bool_decide_true; [ | rewrite elem_of_app; left ]; done.
      + iApply Heq=>//. iIntros "!> _". iApply "Post". iPureIntro.
        destruct (decide (u = v)) as [->|Neq].
        * rewrite !bool_decide_true //. set_solver+.
        * rewrite !bool_decide_false //. set_solver.
    - iPureIntro; rewrite bool_decide_false; [ | rewrite elem_of_nil; intro ]; done.
  Qed.

  Lemma list_filter_spec `{E : Encodable T} (P : T → Prop) `{Dec: ∀ u, Decision (P u)}
          (f : val) (us : list T):
    {{{ implements (Func (Data T) (Data bool)) f (λ u, bool_decide (P u)) }}}
      list_filter f (encode' us)
    {{{ RET (encode' (filter P us)); True }}}.
  Proof.
    iIntros (Φ) "#f_spec Post"; wp_lam; wp_pures.
    wp_apply (list_foldr_spec _ _ us (λ acc vs, ⌜ acc = encode' (filter P vs) ⌝)%I).
    + iSplit; [ | done ]; iIntros (v acc vs); iModIntro; clear Φ; iIntros (Φ) "-> Post"; wp_pures.
      wp_apply "f_spec"; [ done | iIntros (y) "->" ]; case (Dec v); intro H1;
        [ rewrite (bool_decide_true _ H1) | rewrite (bool_decide_false _ H1) ]; wp_pures.
      - wp_apply cons_spec; [ done | iIntros "_" ];
        rewrite filter_cons_True;  [ iApply "Post" | apply H1 ]; done.
      - rewrite filter_cons_False; [ iApply "Post" | apply H1 ]; done.
    + iIntros (acc) "->"; by iApply "Post".
  Qed.

  Lemma list_replicate_spec `{Encodable T} n u :
    {{{ True }}} list_replicate #n (encode' u) {{{ RET (encode' (replicate n u)); True }}}.
  Proof.
    iInduction n as [| m ] "IH"; iIntros (Φ) "_ Post"; wp_lam; wp_pures; [ by iApply "Post" | ].
    rewrite (_ : (S m - 1)%Z = m); [ | lia ].
    wp_apply "IH"; [ done | iIntros "_" ]; wp_apply cons_spec; [ done | iIntros "_" ].
    by iApply "Post".
  Qed.

End ListSpec.
