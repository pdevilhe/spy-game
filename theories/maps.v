From stdpp              Require Import fin_maps list.
From iris.proofmode     Require Import proofmode.
From iris.program_logic Require Import weakestpre.
From iris.heap_lang     Require Import proofmode notation lang.

From spygame Require Import type_repr lists.

Class Map Σ `{!heapGS Σ} := {
  make_map          : val;
  map_lookup        : val;
  map_insert        : val;
  map_delete        : val;
  map_to_assoc_list : val;

  is_map {A B} `{Countable A} :
    val → gmap A B → iProp Σ;

  make_map_spec A B `{Countable A} :
    {{{ True }}} make_map #() {{{ (m : val), RET m; is_map m (∅ : gmap A B) }}};

  map_lookup_spec {A B} `{Encodable A, Encodable B, Countable A} m (map : gmap A B) a :
    {{{ is_map m map }}}
      map_lookup m (encode' a)
    {{{ RET (match map !! a with
             | Some b => SOMEV (encode' b)
             | None   => NONEV
             end);
        is_map m map }}};

  map_insert_spec {A B} `{Encodable A, Encodable B, Countable A} m (map : gmap A B) a b :
    {{{ is_map m map }}}
      map_insert m (encode' a) (encode' b)
    {{{ RET #(); is_map m (<[ a := b ]> map) }}};

  map_delete_spec {A B} `{Encodable A, Countable A} m (map : gmap A B) a :
    {{{ is_map m (map : gmap A B) }}}
      map_delete m (encode' a)
    {{{ RET #(); is_map m (delete a map) }}};

  map_to_assoc_list_spec {A B} `{Encodable A, Encodable B, Countable A} m (map : gmap A B) :
    {{{ is_map m map }}}
      map_to_assoc_list m
    {{{ RET (encode' ((map_to_list map) : list (A * B))); is_map m map }}}
}.

Section MapDefs.

  Context `{Map Σ}.

  Definition map_lookup_exn : val :=
    λ: "m" "a",
      match: map_lookup "m" "a" with NONE => #() | SOME "b" => "b" end.

  Definition map_mem : val :=
    λ: "m" "a",
      match: map_lookup "m" "a" with NONE => #false | SOME "_" => #true end.

  Definition map_fold : val :=
    λ: "f" "init" "m",
      list_foldr (λ: "u" "acc", "f" "acc" (Fst "u") (Snd "u")) "init" (map_to_assoc_list "m").

  Definition map_flush : val :=
    λ: "m",
      list_foldl (λ: <> "u", map_delete "m" (Fst "u")) #() (map_to_assoc_list "m").

  Definition map_of_assoc_list : val :=
    λ: "us",
      let: "m" := make_map #() in
      list_foldr (λ: "u" <>, map_insert "m" (Fst "u") (Snd "u")) #() "us";;
      "m".

  Definition map_of_list : val :=
    λ: "y" "xs",
      let: "m" := make_map #() in
      list_foldr (λ: "x" <>, map_insert "m" "x" "y") #() "xs";;
      "m".

  Definition map_assignement : val :=
    λ: "m1" "m2",
      map_flush "m1";;
      map_fold (λ: <> "a" "b", map_insert "m1" "a" "b") #() "m2".

  Definition map_copy : val :=
    λ: "m1",
      let: "m2" := make_map #() in
      map_assignement "m2" "m1";;
      "m2".

  Definition map_union : val :=
    λ: "m1" "m2",
      let: "m3" := map_copy "m2" in
      map_fold (λ: <> "a" "b", map_insert "m3" "a" "b") #() "m1";;
      "m3".

  Definition map_transfer : val :=
    λ: "m1" "m2",
      map_assignement "m2" (map_union "m2" "m1");;
      map_flush "m1".

  Definition map_filter : val :=
    λ: "f" "m",
      map_of_assoc_list (list_filter "f" (map_to_assoc_list "m")).

  Definition map_dom : val :=
    λ: "m", list_imap (λ: <> "u", Fst "u") (map_to_assoc_list "m").

  Context {A B} `{Ea : Encodable A, Eb : Encodable B, C : Countable A}.

  Lemma map_lookup_exn_spec m map a b:
    map !! (a : A) = Some (b : B) →
      {{{ is_map m map }}} map_lookup_exn m (encode' a) {{{ RET (encode' b); is_map m map }}}.
  Proof.
    iIntros (H1 Φ) "Hm Post"; wp_lam; wp_pures.
    wp_apply (map_lookup_spec with "Hm"); iIntros "Hm"; rewrite H1; wp_pures; by iApply "Post".
  Qed.

  Lemma map_mem_spec m map a :
    {{{ is_map m (map : gmap A B) }}}
      map_mem m (encode' (a : A))
    {{{ RET #(bool_decide (is_Some (map !! a))); is_map m map }}}.
  Proof.
    iIntros (Φ) "Hm Post"; wp_lam; wp_pures.
    wp_apply (map_lookup_spec with "Hm"); iIntros "Hm".
    case_eq (map !! a); [ intro b | ]; intro H1; wp_pures; by iApply "Post".
  Qed.

  Lemma map_fold_spec (f init m : val) map (I : val → gmap A B → iProp Σ) :
    {{{ (∀ a b acc map',
           {{{ I acc map' }}}
             f acc (encode' a) (encode' b)
           {{{ (acc' : val), RET acc'; I acc' (<[ a := b ]> map') }}}) ∗
         I init ∅ ∗
         is_map m map }}}
      map_fold f init m
    {{{ (acc : val), RET acc; I acc map ∗ is_map m map }}}.
  Proof.
    iIntros (Φ) "[#f_spec [Iinit Hm]] Post"; wp_lam; wp_pures.
    wp_apply (map_to_assoc_list_spec with "Hm"); iIntros "Hm". wp_pures.
    wp_apply (list_foldr_spec _ _ _ (λ acc (vs : list (A * B)), I acc (list_to_map vs))
              with "[Iinit]"); iFrame.
    + iIntros (v acc vs); iModIntro; clear Φ; iIntros (Φ) "Iacc Post".
      case v; iIntros (a' b'); wp_pures; by wp_apply ("f_spec" with "Iacc").
    + iIntros (acc) "Iacc"; iApply "Post"; rewrite list_to_map_to_list; by iFrame.
  Qed.

  Lemma map_flush_spec m map :
    {{{ is_map m (map : gmap A B) }}} map_flush m {{{ RET #(); is_map m (∅ : gmap A B) }}}.
  Proof.
    iIntros (Φ) "Hm Post"; wp_lam; wp_pures.
    wp_apply (map_to_assoc_list_spec with "Hm"); iIntros "Hm". wp_pures.
    wp_apply (list_foldl_spec _ _ _ (λ acc (us : list (A * B)), ⌜ acc = #() ⌝ ∗
               (∃ map', is_map m map' ∗
                 ⌜ map' ⊆ map ⌝ ∗ ⌜ ∀ u, u ∈ us → map' !! u.1 = None ⌝))%I
              with "[Hm]").
    + iSplit; [ | iSplit; [ done | iExists map; iFrame; iSplit; iPureIntro; set_solver ] ].
      iIntros (n acc u); iModIntro; clear Φ; iIntros (Φ) "[[-> Hm] %] Post".
      iDestruct "Hm" as (map') "[Hm [% %]]"; case_eq u; iIntros (a' b' u_eq); wp_pures.
      wp_apply (map_delete_spec with "Hm"); iIntros "Hm"; iApply "Post".
      iSplit; [ done | ]; iExists (delete a' map'); iFrame; iPureIntro; split; [
      trans map'; [ apply delete_subseteq | apply H1 ] | intro v ].
      rewrite (take_S_r _ _ u) // elem_of_app elem_of_list_singleton u_eq
              lookup_delete_None. naive_solver.
    + iIntros (acc) "[-> Hm]"; iApply "Post"; iDestruct "Hm" as (map') "[Hm [% %]]".
      rewrite (_ : map' = ∅); [ done | ]. apply map_eq_iff; intro a; rewrite lookup_empty.
      case_eq (map !! a); [ intro b | ]; intro H3.
      - by apply (H1 (a, b)), elem_of_map_to_list.
      - by apply (lookup_weaken_None _ map).
  Qed.

  Lemma map_of_assoc_list_spec (us : list (A * B)) :
    {{{ True }}}
      map_of_assoc_list (encode' (us : list (A * B)))
    {{{ (m : val), RET m; is_map m (list_to_map us : gmap A B) }}}.
  Proof.
    iIntros (Φ) "_ Post"; wp_lam; wp_pures.
    wp_bind (make_map #())%E.
    wp_apply make_map_spec; [ done | iIntros (m) "Hm" ]; wp_pures.
    wp_apply (list_foldr_spec _ _ us (λ acc (vs : list (A * B)), ⌜ acc = #() ⌝ ∗
      is_map m (list_to_map vs : gmap A B))%I with "[Hm]").
    + iSplit; [ | iSplit; by iFrame ].
      iIntros (v acc vs); iModIntro; clear Φ; iIntros (Φ) "[-> Hm] Post"; wp_pures; case v.
      iIntros (a b); wp_pures; wp_apply (map_insert_spec with "Hm"); iIntros "Hm"; iApply "Post".
      by iSplit.
    + iIntros (acc) "[-> Hm]"; wp_pures; by iApply "Post".
  Qed.

  Lemma map_of_list_spec y xs :
    {{{ True }}}
      map_of_list (encode' (y : B)) (encode' (xs : list A))
    {{{ (m : val), RET m; is_map m (gset_to_gmap y (list_to_set xs) : gmap A B) }}}.
  Proof.
    iIntros (Φ) "_ Post"; wp_lam; wp_pures.
    wp_bind (make_map _)%E; wp_apply make_map_spec; [ done | iIntros (m) "Hm" ]; wp_pures.
    wp_apply (list_foldr_spec _ _ xs (λ acc vs, ⌜ acc = #() ⌝ ∗
      is_map m (gset_to_gmap y (list_to_set vs) : gmap A B))%I with "[Hm]").
    + iSplit; [ | iSplit; simpl; [ | rewrite gset_to_gmap_empty ]; done ].
      iIntros (x acc xs'); iModIntro; clear Φ; iIntros (Φ) "[-> Hm] Post"; wp_pures.
      wp_apply (map_insert_spec with "Hm"); iIntros "Hm"; iApply "Post"; iSplit; [ done | ].
      by rewrite -gset_to_gmap_union_singleton.
    + iIntros (acc) "[-> Hm]"; wp_pures; by iApply "Post".
  Qed.

  Lemma map_assignement_spec (m1 m2 : val) (map1 map2 : gmap A B) :
    {{{ is_map m1 map1 ∗ is_map m2 map2 }}}
      map_assignement m1 m2
    {{{ RET #(); is_map m1 map2 ∗ is_map m2 map2 }}}.
  Proof.
    iIntros (Φ) "[Hm1 Hm2] Post"; wp_lam; wp_pures.
    wp_apply (map_flush_spec with "Hm1"); iIntros "Hm1"; wp_pures.
    wp_apply (map_fold_spec _ _ m2 map2 (λ acc map, ⌜ acc = #() ⌝ ∗
                is_map m1 map)%I with "[Hm1 Hm2]").
    + iFrame; iSplit; [| done].
      iIntros (a b acc map); iModIntro; clear Φ; iIntros (Φ) "[% Hm2] Post"; wp_pures.
      wp_apply (map_insert_spec with "Hm2"); iIntros "Hm2"; iApply "Post"; by iFrame.
    + iIntros (acc) "[[-> Hm2] Hm1]"; iApply "Post". by iFrame.
  Qed.

  Lemma map_copy_spec (m1 : val) (map : gmap A B) :
    {{{ is_map m1 map }}}
      map_copy m1
    {{{ m2, RET m2; is_map m1 map ∗ is_map m2 map }}}.
  Proof.
    iIntros (Φ) "Hm1 Post"; wp_lam.
    wp_bind (make_map _)%E; wp_apply make_map_spec; [ done | iIntros (m2) "Hm2" ].
    wp_pures. wp_apply (map_assignement_spec m2 m1 with "[Hm1 Hm2]"); [ iFrame |].
    iIntros "[Hm2 Hm1]"; wp_pures; iApply "Post"; by iFrame.
  Qed.

  Lemma map_union_spec (m1 m2 : val) (map1 map2 : gmap A B) :
    {{{ is_map m1 map1 ∗ is_map m2 map2 }}}
      map_union m1 m2
    {{{ m3, RET m3; is_map m1 map1 ∗ is_map m2 map2 ∗ is_map m3 (map1 ∪ map2) }}}.
  Proof.
    iIntros (Φ) "[Hm1 Hm2] Post"; wp_lam; wp_pures; wp_apply (map_copy_spec with "Hm2").
    iIntros (m3) "[Hm2 Hm3]"; wp_pures.
    wp_apply (map_fold_spec  _ _ m1 map1 (λ acc map, ⌜ acc = #() ⌝ ∗
                is_map m3 (map ∪ map2))%I with "[Hm1 Hm3]").
    + rewrite left_id. iFrame; iSplit; [| done ].
      iIntros (a b acc map'); iModIntro; clear Φ; iIntros (Φ) "[-> Hm3] Post"; wp_pures.
      wp_apply (map_insert_spec with "Hm3"); iIntros "Hm3"; iApply "Post".
      iSplit; [ | rewrite insert_union_l ]; done.
    + iIntros (acc) "[[-> Hm3] Hm1]"; wp_pures; iApply "Post"; by iFrame.
  Qed.

  Lemma map_transfer_spec (m1 m2 : val) (map1 map2 : gmap A B) :
    {{{ is_map m1 map1 ∗ is_map m2 map2 }}}
      map_transfer m1 m2
    {{{ RET #(); is_map m1 (∅ : gmap A B) ∗ is_map m2 (map2 ∪ map1) }}}.
  Proof.
    iIntros (Φ) "[Hm1 Hm2] Post"; wp_lam; wp_let.
    wp_apply (map_union_spec m2 m1 with "[Hm1 Hm2]"); [ iFrame | iIntros (m3) "[Hm2 [Hm1 Hm3]]"].
    wp_apply (map_assignement_spec m2 m3 with "[Hm2 Hm3]"); [ iFrame | iIntros "[Hm2 _]" ].
    wp_pures. wp_apply (map_flush_spec with "Hm1"); iIntros "Hm1"; iApply "Post"; by iFrame.
  Qed.

  Lemma map_dom_spec (m : val) (map : gmap A B) :
    {{{ is_map m map }}}
      map_dom m
    {{{ us, RET (encode' (us : list A)); is_map m map ∗
        ⌜ list_to_set us = dom map ⌝ }}}.
  Proof.
    iIntros (Φ) "Hm Post"; wp_lam; wp_apply (map_to_assoc_list_spec with "Hm"); iIntros "Hm".
    wp_pures. wp_apply (list_imap_spec _ _ (λ _, fst : A * B → A) (λ _, True)%I).
    + iSplit; [| done]; iIntros (n u); iModIntro; clear Φ; iIntros (Φ) "[_ %] Post"; wp_pures.
      case u; intros a b; wp_pures; by iApply "Post".
    + iIntros "_"; iApply "Post"; iFrame; iPureIntro; revert map; apply map_ind;
      [ rewrite dom_empty_L map_to_list_empty //= | clear m; intros a b m lookup_m IH ].
      rewrite dom_insert_L -IH.
      rewrite (_: list_to_set (imap (λ _ : nat, fst) (map_to_list (<[a:=b]> m))) =
                  list_to_set (imap (λ _ : nat, fst) ((a, b) :: map_to_list m))); [ by simpl |].
      apply list_to_set_perm_L; do 2 rewrite imap_const.
      by apply Permutation_map, map_to_list_insert.
  Qed.

  Lemma map_filter_spec
    (P : (A * B) → Prop) `{Dec : ∀ u, Decision (P u)} (f m1 : val) (map : gmap A B) :
    {{{ implements (Func (Data (A * B)) (Data bool)) f (λ u, bool_decide (P u)) ∗
        is_map m1 map
    }}}
      map_filter f m1
    {{{ m2, RET m2; is_map m1 map ∗ is_map m2 (filter P map) }}}.
  Proof.
    iIntros (Φ) "[#f_spec Hm1] Post"; wp_lam; wp_let.
    wp_apply (map_to_assoc_list_spec with "Hm1"); iIntros "Hm1".
    wp_apply (list_filter_spec P); [ done | iIntros "_" ].
    wp_apply (map_of_assoc_list_spec (filter P (map_to_list map) : list (A * B))); [ done | ].
    iIntros (m2) "Hm2"; iApply "Post"; iFrame; by rewrite map_filter_alt.
  Qed.

End MapDefs.
