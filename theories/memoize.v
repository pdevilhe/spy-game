(* memoize.v

   In this theory, we verify two implementations of memoization. The first one,
   [memoize_seq], relies on a thread-unsafe data structure, therefore the
   client must use the memoized function in a sequential fashion. The second
   one, [memoize_conc], avoids this complication by wrapping every access to
   the data structure in locks. Consequently, it provides a more convenient
   usage of the memoized function by the client.

   The difference between these implementations becomes clearer when we
   compare their specifications. In the specification of [memoize_seq], we
   notice the presence of an abstract permission that serves to impose the
   sequential usage of the memoized function. Meanwhile, the specification of
   [memoize_conc] simply states that the output function satisfies the same
   specification as the input one. Another difference is the range of admissible
   input functions that each implementation supports. [memoize_conc] supports
   apparently pure functions, while [memoize_seq] supports functions with
   side effects. These effects are visible every time that the memoized function
   receives a call to a previously unseen argument.
*)

From iris.heap_lang     Require Import proofmode notation lang.
From iris.heap_lang.lib Require Import spin_lock.
From spygame            Require Import maps lock_lib type_repr.

Section Memoize.

  Context `{Map Σ} `{lock} `{!lockG Σ}.

  Definition memoize_seq : val := λ: "f",
    let: "m" := make_map #() in

    λ: "x",
      match: map_lookup "m" "x" with
        SOME "y" => "y"
      | NONE     =>
        let: "y" := "f" "x" in
        map_insert "m" "x" "y";;
        "y"
      end.

  Definition memoize_conc : val := λ: "f",
    let: "m" := make_map #() in
    let: "lk" := newlock #() in

    λ: "x",
      match: withLock "lk" (λ: <>, map_lookup "m" "x") with
        SOME "y" => "y"
      | NONE     =>
        let: "y" := "f" "x" in
        withLock "lk" (λ: <>, map_insert "m" "x" "y");;
        "y"
      end.

  Lemma memoize_seq_spec `{Dx: Decodable X, Dy: Decodable Y}
    (P : X     → iProp Σ)
    (Q : X → Y → iProp Σ)
    (R : X → Y → iProp Σ)
    (f : val) :

    {{{ (∀ x, {{{ P x }}} f (encode' x) {{{ y, RET (encode' y); Q x y ∗ □ R x y }}}) }}}
      memoize_seq f
    {{{ (g : val) (S : gset X → iProp Σ), RET g;

      S ∅ ∗

      (∀ x xs,
        {{{ S xs ∗ ⌜ x ∈ xs ⌝ }}}
          g (encode' x)
        {{{ y, RET (encode' y); S xs ∗ □ R x y }}}) ∗

      (∀ x xs,
        {{{ S xs ∗ ⌜ x ∉ xs ⌝ ∗ P x }}}
          g (encode' x)
        {{{ y, RET (encode' y); S ({[x]} ∪ xs) ∗ Q x y ∗ □ R x y }}})

    }}}.

  Proof.
    iIntros (Φ) "#f_spec Post". wp_lam.
    wp_apply (make_map_spec X Y); [done|]. iIntros (m) "Hm". wp_pures. iModIntro.

    (* Definition of the abstract permission S. *)
    iSpecialize ("Post" $! _ (λ xs,
      (∃ (map : gmap X Y),
        is_map m map ∗
        ⌜ dom map = xs ⌝ ∗
        ∀ x y, ⌜ map !! x = Some y ⌝ → □ R x y)%I)).

    iApply "Post". iSplit; [|iSplit].
    (* Case ∅: no arguments were seen in the start. *)
    + iExists ∅. iFrame. iSplit.
      - iPureIntro. by apply dom_empty_L.
      - iIntros (x y). rewrite ->lookup_empty. auto with congruence.
    (* Case (x ∈ x): the argument x was already seen. *)
    + iIntros (x xs). iModIntro. clear Φ. iIntros (Φ) "[HS %] Post".
      iDestruct "HS" as (map) "[Hmap [% HR]]".
      rename H0 into in_xs, H1 into dom_map.
      wp_pures. wp_apply (map_lookup_spec m map with "Hmap"). iIntros "Hmap".
      (* There must be some computation stored for x. *)
      have: is_Some (map !! x).
      { cut (x ∈ dom map); [apply elem_of_dom|by subst]. }
      destruct 1 as [y lookup_map]. rewrite lookup_map.
      wp_pures. iModIntro. iApply "Post". iSplit; [|by iApply "HR"].
      iExists map. by iFrame.
    (* Case (x ∉ x): the argument x has never been seen. *)
    + iIntros (x xs). iModIntro. clear Φ. iIntros (Φ) "[HS [% HP]] Post".
      iDestruct "HS" as (map) "[Hmap [% #HR]]".
      rename H0 into not_in_xs, H1 into dom_map.
      wp_pures. wp_apply (map_lookup_spec m map with "Hmap"). iIntros "Hmap".
      (* There must be none computation stored for x. *)
      assert (map !! x = None) as ->.
      { cut (x ∉ dom map); [apply not_elem_of_dom|by subst]. }
      (* Computation: (f x). *)
      wp_pures. wp_apply ("f_spec" with "HP"). iIntros (y) "[HQ #HR']".
      (* Store result of the computation. *)
      wp_pures. wp_apply (map_insert_spec m map x y with "Hmap"). iIntros "Hmap".
      wp_pures. iModIntro. iApply "Post". iFrame. iSplit; [iSplit|done].
      - iPureIntro. rewrite dom_insert_L. congruence.
      - iIntros (x' y'). rewrite lookup_insert_Some.
        iIntros ([[-> ->]|[_ ?]]); first done. by iApply "HR".
  Qed.

  Lemma memoize_conc_spec `{Dx: Decodable X, Dy: Decodable Y}
    (P : X     → iProp Σ)
    (Q : X → Y → iProp Σ)
    (f : val) :

    {{{ (∀ x, {{{ □ P x }}} f (encode' x) {{{ y, RET (encode' y); □ Q x y }}}) }}}
      memoize_conc f
    {{{ (g : val), RET g;
        (∀ x, {{{ □ P x }}} g (encode' x) {{{ y, RET (encode' y); □ Q x y }}}) }}}.

  Proof.
    iIntros (Φ) "#f_spec Post". wp_lam.
    wp_apply (make_map_spec X Y); [done|]. iIntros (m) "Hm". wp_pures.

    (* Invariant that describes the map and which is owned by the lock. *)
    wp_apply (newlock_spec (∃ (map : gmap X Y),
      is_map m map ∗ ∀ x y, ⌜ map !! x = Some y ⌝ → □ Q x y)%I
    with "[Hm]").
    { iExists (∅ : gmap X Y). iFrame.
      iIntros (x y). rewrite lookup_empty. auto with congruence. }
    iIntros (lk γ) "#Hlk". wp_pures. iModIntro. iApply "Post".

    (* Proof that the memoized function satisfies the same spec as the input. *)
    iIntros (x). iModIntro. clear Φ. iIntros (Φ) "#HP Post". wp_lam.
    unfold withLock. wp_pures.
    (* Acquire the lock: perform map lookup. *)
    wp_apply (acquire_spec with "Hlk").
    iIntros "[Hlocked Hmap]". iDestruct "Hmap" as (map) "[Hmap #HQ]".
    wp_pures. wp_apply (map_lookup_spec m map with "Hmap").
    iIntros "Hmap". wp_let.
    (* Release the lock. *)
    wp_apply (release_spec γ lk _ with "[Hlk Hlocked Hmap HQ]");[|iIntros"_"].
    { iFrame. iSplitL ""; [iApply "Hlk"|iExists map; iFrame]; done. }

    case_eq (map !! x).
    (* Case where the argument x has already been seen. *)
    + intros y lookup_map. wp_pures. iApply "Post". by iApply "HQ".
    (* Case where the argument x has never been seen. *)
    + intro lookup_map. wp_pures. wp_apply ("f_spec" with "HP").
      iIntros (y) "#Q". wp_pures.
      (* Acquire the lock: addition of new key/value pair into the map. *)
      wp_apply (acquire_spec with "Hlk").
      iIntros "[Hlocked Hmap]". iDestruct "Hmap" as (map') "[Hmap' #HQ']".
      wp_pures. wp_apply (map_insert_spec m map' x y with "Hmap'"). iIntros "Hmap'".
      (* Release the lock. *)
      wp_pures. wp_apply (release_spec γ lk _ with "[Hlk Hlocked Hmap' HQ' Q]");
      [|iIntros "_"].
      { iFrame. iSplitL ""; [iApply "Hlk"|].
        iExists (<[ x := y ]> map'). iFrame.
        iIntros (x' y'). rewrite lookup_insert_Some.
        iIntros ([[-> ->]|[_ ?]]); first done. by iApply "HQ'". }
      wp_pures. by iApply "Post".
  Qed.
End Memoize.
