From iris.base_logic    Require Import invariants.
From iris.proofmode     Require Import proofmode.
From iris.program_logic Require Import weakestpre.
From iris.heap_lang     Require Import proofmode notation lang.
From iris.heap_lang.lib Require Import spin_lock.

From spygame Require Import lists type_repr proph_lib modulus.

Section Preamble.

  Program Instance bnat_countable (m : nat) : Countable { n : nat | (n < m)%nat } := {|
    countable.encode bn := Pos.of_nat (S bn); (* [Program] coerces [bn] to [nat] here. *)
    countable.decode p  :=
      let n := pred (Pos.to_nat p) in
      match lt_dec n m return _ with
      | left  pf => Some (exist _ n pf)
      | right _  => None
      end
  |}.
  Next Obligation.
    intros m bn; case bn; intros n pf; simpl; rewrite Nat2Pos.id; [ simpl | done ].
    case (lt_dec n m); intro pf'; [ | contradiction ].
    rewrite (_ : n ↾ pf = n ↾ pf'); [ |
    apply sig_eq_pi; [ intro; apply Nat.lt_pi | ] ]; done.
  Qed.

  Definition bnat_list (m : nat) : list { n : nat | (n < m)%nat } :=
    match m with
    | 0%nat => []
    | S m'  =>
      let zero : { n : nat | (n < S m')%nat } :=
        0%nat ↾ (Nat.lt_0_succ m') in
      list.imap (λ n _,
        match lt_dec n (S m') with left pf => n ↾ pf | right _ => zero end
      ) (replicate m zero)
    end.

(*
  Definition fun_of_list {A : Type} (l : list A) : { n : nat | (n < length l)%nat } → A.
  Proof.
    case; intros n pf; case_eq (l !! n); [ done | ].
    Search lookup. intro H. Check (lookup_ge_None_1 l n H).
    by have : False; [
     apply (Nat.lt_irrefl _ (PeanoNat.Nat.lt_le_trans _ _ _ pf (lookup_ge_None_1 l n H))) | ].
  Qed.

  Definition total_lookup l (len_eq : length l = m) :
    { n : nat | (n < m)%nat } → { n : nat | (n < m)%nat } :=
    match len_eq in (_ = y) return { n : nat | (n < y)%nat } → { n : nat | (n < m)%nat } with
    | eq_refl _ => fun_of_list l
    end.
*)

  Lemma bnat_eq_intro (m n : nat) (pf pf' : (n < m)%nat) :
    (exist (λ k, (k < m)%nat) n pf) = (exist (λ k, (k < m)%nat) n pf').
  Proof. apply sig_eq_pi; [ intro; apply Nat.lt_pi | ]; done. Qed.

  Lemma bnat_ineq_elim (m n n' : nat) (pf : (n < m)%nat) (pf' : (n' < m)%nat) :
    (n ↾ pf : { k | (k < m)%nat }) ≠ n' ↾ pf' → n ≠ n'.
  Proof. intros H0 H1; simplify_eq; apply H0, bnat_eq_intro. Qed.

  Lemma elem_of_bnat_list (m : nat) : ∀ bn, bn ∈ (bnat_list m).
  Proof.
    unfold bnat_list; case_eq m; [ | intros m' ]; intros H bn; case bn; intros n pf.
    + specialize (Nat.nlt_0_r n pf); contradiction.
    + apply (elem_of_list_lookup_2 _ n); rewrite list_lookup_imap.
      rewrite -list_lookup_fmap fmap_replicate lookup_replicate; split; [ | assumption ].
      case (lt_dec n (S m')); [ | contradiction ]; intro pf'.
      revert pf pf'; rewrite -H; by apply (bnat_eq_intro m n).
  Qed.

End Preamble.


Section Predecessors.

  Context `{!heapGS Σ} (m : nat).

  Definition predecessors : val :=
    λ: "v" "dependencies",
      list_ifoldl (λ: "acc" "u" "Gu",
        if: list_mem (λ: "x" "y", "x" = "y") "v" "Gu" then cons "u" "acc" else "acc"
      ) nil "dependencies".

  Lemma predecessors_spec (v : { n | (n < m)%nat }) (Gs : list (list {n | (n < m)%nat})) :
    {{{ ⌜ length Gs = m ⌝ }}}
      predecessors (encode' v) (encode' Gs)
    {{{ (us : list {n | (n < m)%nat}), RET (encode' us);
        ⌜ ∀ u, u ∈ us ↔ ∃ G, Gs !! (`u) = Some G ∧ v ∈ G ⌝ }}}.
  Proof.
    iIntros (Φ) "% Post"; wp_lam; wp_pures.
    wp_apply (list_ifoldl_spec _ _ Gs (λ acc Gs',
      ∃ (us : list {n | (n < m)%nat}), ⌜ acc = encode' us ⌝ ∗
            ⌜ ∀ u, u ∈ us ↔ ∃ G, Gs' !! (`u) = Some G ∧ v ∈ G ⌝)%I).
    + iSplit; [ | iExists []; iSplit; [ done | iPureIntro; split; [ set_solver | ] ] ].
      - iIntros (n acc G'); iModIntro; clear Φ; iIntros (Φ) "[Hacc %] Post".
        iDestruct "Hacc" as (us) "[-> HG]"; iDestruct "HG" as %HG;  wp_pures.
        wp_apply (list_mem_spec); [ | done | iIntros "_" ].
        { iIntros (x y Φ') "_ HΦ". wp_pures.
          do 2 case_bool_decide; try by iApply "HΦ".
          - naive_solver.
          - pose proof (encode_injective x y). naive_solver. }
        case (elem_of_list_dec v G'); intro HvG';
          [ rewrite (bool_decide_true _ HvG') | rewrite (bool_decide_false _ HvG') ]; wp_pures.
        ++ assert (n < m)%nat as pf.
           { eapply Nat.lt_le_trans; [by eapply lookup_lt_Some|lia]. }
           wp_apply (cons_spec (n ↾ pf) us); [ done | iIntros "_" ]; iApply "Post".
           iExists (n ↾ pf :: us); iPureIntro; split; [ done | ].
           intro u; split; rewrite elem_of_cons; [ case | ].
           -- intros ->. rewrite /= lookup_take; eauto.
           -- rewrite HG; destruct 1 as [D [??]]; exists D; split=>//.
              rewrite (take_S_r _ _ G') //. by apply lookup_app_l_Some.
           -- destruct 1 as [D [HD ?]]; case (sig_eq_dec _ u (n ↾ pf)); [ set_solver | ].
              destruct u as [n' ?]. intros Hne%bnat_ineq_elim.
              right. rewrite HG; exists D; split; [ | done ].
              revert HD. rewrite (take_S_r _ _ G') //= lookup_app_Some=>-[//|].
              rewrite length_take_le; [|lia]. intros [??].
              assert (n' - n = 0)%nat by by destruct (n' - n)%nat. lia.
        ++ iApply "Post"; iExists us; iPureIntro; split; [ done | ].
           intro u; split.
           -- rewrite HG; destruct 1 as [D [H4 H5]]; exists D; split=>//.
              rewrite (take_S_r _ _ G') //. by apply lookup_app_l_Some.
           -- destruct 1 as [D [HD ?]]; rewrite HG; exists D; split; [ | done ].
              revert HD. rewrite (take_S_r _ _ G') //= lookup_app_Some=>-[//|].
              rewrite length_take_le; [|by apply Nat.lt_le_incl, lookup_lt_is_Some; eauto].
              destruct u as [n' ?]=>/= -[??].
              destruct (n' - n)%nat; [naive_solver|done].
      - rewrite lookup_nil; destruct 1 as [ G [? _]]; done.
    + iIntros (acc) "Hacc"; iDestruct "Hacc" as (us) "[-> %]"; by iApply "Post".
  Qed.
End Predecessors.

Section FixSimple.
  Context `{!heapGS Σ} `{lock} `{!lockG Σ}.
  Context (m : nat) (gt : (0 < m)%nat).

  Definition total_lookup :
    list { n | (n < m)%nat } → { n | (n < m)%nat } → { n | (n < m)%nat } :=
    λ l, λ i, nth (`i) l (0%nat ↾ gt).

  Lemma total_lookup_Some us i :
    length us = m → us !! (`i) = Some (total_lookup us i).
  Proof.
    case i; intros n pf H0; unfold total_lookup; simpl.
    assert (is_Some (us !! n)) as H1; [ apply lookup_lt_is_Some_2; by rewrite H0 | ].
    destruct H1 as [u H1]; by rewrite (nth_lookup_Some _ _ _ _ H1).
  Qed.

  Lemma total_lookup_insert us i u :
    (`i < length us)%nat → total_lookup (<[ `i := u ]> us) i = u.
  Proof.
    case i; intros k pf; simpl; intro H0. unfold total_lookup.
    by apply nth_lookup_Some, list_lookup_insert.
  Qed.

  Lemma total_lookup_insert' us i (pf : (i < m)%nat) u :
    (i < length us)%nat → total_lookup (<[ i := u ]> us) (i ↾ pf) = u.
  Proof.
    unfold total_lookup; simpl; intro H0; apply nth_lookup_Some, list_lookup_insert; done.
  Qed.

  Lemma total_lookup_insert_ne us i j u :
    i ≠ `j → total_lookup (<[ i := u ]> us) j = total_lookup us j.
  Proof.
    case j; intros k pf; simpl; intro H0; unfold total_lookup; simpl.
    rewrite nth_lookup nth_lookup list_lookup_insert_ne; done.
  Qed.

  Lemma total_lookup_insert_extends us i (pf : (i < m)%nat) u G :
    ((i ↾ pf) : { n | (n < m)%nat }) ∉ G →
      extends (total_lookup (<[ i := u ]> us)) (total_lookup us) G.
  Proof.
    rewrite extends_lookup; intros H0 i' u' H1.
    case_eq u'; intros j' pf' eq; simplify_eq.
    case (Nat.eq_dec i j').
    + intro; simplify_eq; rewrite (bnat_eq_intro m _ pf' pf) in H1.
      specialize (H0 (elem_of_list_lookup_2 _ _ _ H1)); contradiction.
    + intro H2; simplify_eq; by rewrite total_lookup_insert_ne.
  Qed.

  Lemma total_lookup_implementation us :
    length us = m →
      ⊢ implements (Func (Data { n | (n < m)%nat}) (Data { n | (n < m)%nat}))
        (λ: "u", list_lookup (encode' us) "u") (total_lookup us).
  Proof.
    intro H0; iIntros (x bn); case bn; intros n pf; iModIntro; iIntros (Φ) "-> Post"; wp_pures.
    have : (is_Some (us !! n)); [ apply lookup_lt_is_Some_2; lia | destruct 1 as [u H1] ].
    wp_apply (list_lookup_spec _ _ _ H1); [ done | iIntros "_" ].
    unfold total_lookup; rewrite (nth_lookup_Some _ _ _ _ H1); by iApply "Post".
  Qed.

  Definition simple_lfp : val := λ: "eqs",

    let: "cache"        := ref (list_replicate #m #0%nat) in
    let: "todo"         := ref (encode' (bnat_list m))    in
    let: "dependencies" := ref (list_replicate #m nil)    in

    let: "p" := NewProph in

    let: "reevaluate" := λ: "v",

      let: "cs" := ! "cache" in
      let: "request" :=
        λ: "u", list_lookup "cs" "u" in

      let: "pair" :=
        modulus ("eqs" "v") "request" in

      let: "c" := Fst "pair" in
      let: "G" := Snd "pair" in

      "dependencies" <- list_insert (! "dependencies") "v" "G";;

      if: "c" = list_lookup (! "cache") "v" then #() else
        let: "pred" :=
          predecessors "v" (! "dependencies") in
        list_iter (λ: <> "u",
          "dependencies" <- list_insert (! "dependencies") "u" nil
        ) "pred";;
        "todo"  <- list_append "pred" (! "todo");;
        "cache" <- list_insert (! "cache") "v" "c"
    in

    let: "loop" :=
      (rec: "loop" <> :=
        match: (! "todo") with
          NONE        => #()
        | SOME "pair" =>
          "todo" <- Snd "pair";;
          "reevaluate" (Fst "pair");;
          "loop" #()
        end)
    in

    "loop" #();;
    let: "cs" := ! "cache" in
    resolve_proph_list "p" "cs";;
    resolve_proph_nil "p";;
    (λ: "u", list_lookup "cs" "u").

  Definition simple_lfp_inv ε cache todo dependencies final : iProp Σ :=
    (∃ (cs vs : list {n | (n < m)%nat}) (Gs : list (list {n | (n < m)%nat})),
      cache        ↦ encode' cs ∗
      todo         ↦ encode' vs ∗
      dependencies ↦ encode' Gs ∗
      ⌜ length cs = m ⌝ ∗
      ⌜ length Gs = m ⌝ ∗
      ⌜ ∀ n (pf : (n < m)%nat),
          (((n ↾ pf) : {n | (n < m)%nat}) ∈ vs ∧ Gs !! n = Some []) ∨
          (∃ c G,
             cs !! n = Some c ∧
             Gs !! n = Some G ∧
             (extends (total_lookup cs) (total_lookup final) G →
               c = ε (n ↾ pf) (total_lookup final))) ⌝)%I.

  Lemma simple_lfp_spec ε (eqs : val) :
    let T := (Data {n | (n < m)%nat}) in
    {{{ implements (Func T (Func (Func T T) T)) eqs ε }}}
      simple_lfp eqs
    {{{ φ f, RET f;
        implements (Func T T) f φ ∗ ⌜ ∀ bn, φ bn = ε bn φ ⌝ }}}.
  Proof.
    iIntros (T); unfold T.
    iIntros (Φ) "#eqs_spec Post"; wp_lam; wp_pures.
    wp_apply (list_replicate_spec m ((0%nat ↾ gt) : {n | (n < m)%nat})); [ done | iIntros "_" ].
    wp_alloc cache as "Hcache"; wp_let; wp_alloc todo  as "Htodo";  wp_let.
    wp_apply (list_replicate_spec m []); [ done | iIntros "_" ].
    wp_alloc dependencies as "Hdependencies"; wp_let.
    wp_apply (wp_new_proph_list {n | (n < m)%nat}); [ done | iIntros (final p) "Hp" ]; wp_let.
    iAssert (simple_lfp_inv ε cache todo dependencies final)
      with "[Hcache Htodo Hdependencies]" as "Hsimple_lfp_inv".
    { iExists (replicate m (0%nat ↾ gt)), (bnat_list m), (replicate m []); iFrame.
      iPureIntro; do 2 (rewrite length_replicate); split; [ | split ]; try done.
      intros n pf; left; split; [ apply elem_of_bnat_list | rewrite lookup_replicate ]; done. }
    do 2 (wp_closure; wp_let).
    iLöb as "IH_loop" forall (Φ); wp_rec.
    iDestruct "Hsimple_lfp_inv" as (cs vs Gs) "(Hcache & Htodo & Hdependencies & % & % & %)".
    destruct vs as [|v vs']; wp_load; wp_match.
    + wp_seq; wp_load. wp_let. wp_apply (wp_resolve_proph_list p cs with "Hp").
      iIntros (vs') "[% Hp]"; wp_seq; wp_apply (wp_resolve_proph_nil p with "Hp").
      iIntros "%"; wp_pures.
      iSpecialize ("Post" $! (total_lookup cs)); iApply "Post"; iSplitL.
      - by iApply total_lookup_implementation.
      - iPureIntro; simplify_eq; intro bn; case bn; intros n pf; case (H2 n pf).
        ++ case; rewrite elem_of_nil; contradiction.
        ++ rewrite app_nil_r; destruct 1 as [c [G [? [? E]]]].
           rewrite (_ : total_lookup cs (n ↾ pf) = c); [ apply E, extends_self | ].
           unfold total_lookup; by apply nth_lookup_Some.
    + iSpecialize ("eqs_spec" $! (encode' v) v).
      wp_store; destruct v as [n pf]; wp_pures; wp_load; wp_pures. wp_apply "eqs_spec"; [ done | ].
      iIntros (ff) "#ff_spec".
      wp_bind (modulus ff _)%E.
      wp_apply (modulus_spec ff _ (ε (n ↾ pf)) (total_lookup cs)).
      - iSplit; [ iApply total_lookup_implementation | ]; done.
      - iIntros (G extends_strong); wp_pures.
        wp_load; wp_apply (list_insert_spec n G Gs ); [ done | iIntros "_" ]; wp_store.
        assert (is_Some (cs !! n)) as [c Hc]; [ apply lookup_lt_is_Some_2; lia | ]. wp_load.
        wp_apply (list_lookup_spec _ _ _ Hc); [ done | iIntros "_" ]; wp_pures.
        case (sig_eq_dec _ (ε (n ↾ pf) (total_lookup cs)) c); intro E.
        ++ rewrite E bool_decide_true; [ | done ]; wp_if; wp_seq.
           iAssert (simple_lfp_inv ε cache todo dependencies final)
             with "[Hcache Htodo Hdependencies]" as "Hsimple_lfp_inv".
           { iExists cs, vs', (<[ n := G ]> Gs); iFrame; iSplit; [ done | ]; iPureIntro.
             split; [ by rewrite length_insert | ].
             intros n' pf'; case (Nat.eq_dec n n'); intros ?; simplify_eq.
             -- rewrite list_lookup_insert; [ | lia ].
                right; exists (ε (n' ↾ pf) (total_lookup cs)), G.
                split; [ | split; [ | intro ] ]; try done.
                rewrite (_ : n' ↾ pf' = n' ↾ pf); [ apply extends_strong |
                apply sig_eq_pi; [ intro; apply Nat.lt_pi | ] ]; done.
             -- rewrite list_lookup_insert_ne; last done.
                case (H2 n' pf'); intros ?; [ left; set_solver | right ]; done.
           }
           by iApply ("IH_loop" with "[Post] [Hp] [Hsimple_lfp_inv]").
        ++ rewrite bool_decide_false.
           -- wp_pures; wp_load; wp_apply (predecessors_spec m (n ↾ pf));
                [ by rewrite length_insert | iIntros (pred Hpred) ]; wp_pures.
              wp_apply (list_iter_spec _ pred (λ i,
                ∃ (Gs' : list (list {n | (n < m)%nat})), dependencies ↦ encode' Gs' ∗
                       ⌜ length Gs' = m ⌝ ∗
                       ⌜ ∀ k pf,
                           ((k ↾ pf) ∈ take i pred → Gs' !! k = Some []) ∧
                           ((k ↾ pf) ∉ take i pred → Gs' !! k = (<[ n := G]> Gs) !! k) ⌝)%I
                with "[Hdependencies]").
              +++ iSplit.
                  --- iIntros (i u); iModIntro; clear Φ; iIntros (Φ) "[Hd %] Post".
                      iDestruct "Hd" as (Gs') "(Hdependencies & % & %)"; wp_load.
                      case_eq u; intros k pf_k u_eq.
                      wp_apply (list_insert_spec _ [] _); [ done | iIntros "_" ].
                      wp_store; iApply "Post"; iExists (<[k:=[]]> Gs'); iFrame.
                      iPureIntro; rewrite length_insert; split; [ done | ].
                      intros k' pf_k'; simplify_eq.
                      rewrite (take_S_r _ _ (k ↾ pf_k)) // not_elem_of_app elem_of_app.
                      case (Nat.eq_dec k k'); intros ?; simplify_eq;
                      [ rewrite list_lookup_insert; [ | lia ] |
                        rewrite list_lookup_insert_ne; last done ].
                      ++++ split=>//.
                           rewrite (bnat_eq_intro _ _ pf_k pf_k'). set_solver+.
                      ++++ rewrite elem_of_list_singleton. naive_solver.
                  --- iExists (<[ n := G ]> Gs); iFrame; iPureIntro.
                      rewrite firstn_O length_insert; split; [ done | ].
                      intros k pf'; split; [ set_solver | done ].
              +++ iDestruct 1 as (Gs') "(Hdependencies & % & HGs)"; wp_seq; wp_load.
                  iDestruct "HGs" as %HGs.
                  wp_apply (list_append_spec pred vs'); [ done | iIntros "_" ]; wp_store; wp_load.
                  wp_apply list_insert_spec; [ done | iIntros "_" ]; wp_store.
                  iAssert (simple_lfp_inv ε cache todo dependencies final)
                    with "[Hcache Htodo Hdependencies]" as "Hsimple_lfp_inv".
                  { iExists (<[ n := ε (n ↾ pf) (total_lookup cs) ]> cs), (pred ++ vs'), Gs'.
                    iFrame; rewrite length_insert; iPureIntro; split; [ | split ]; try done.
                    intros k pf_k; rewrite firstn_all in HGs.
                    case (HGs k pf_k); case (elem_of_list_dec (k ↾ pf_k) pred);
                    [ intros Hkpred HGsk _ | intros Hkpred _ HGsk ]; specialize (HGsk Hkpred); [ left;
                    split; [ set_solver | done ] | ].
                    specialize (Hpred (k ↾ pf_k)); specialize (H2 k pf_k).
                    case (Nat.eq_dec k n); intro H10; simplify_eq.
                    + rewrite list_lookup_insert; [ | by rewrite H0 ].
                      rewrite list_lookup_insert in Hpred HGsk; [ | by rewrite H1 ].
                      destruct (elem_of_list_dec (n ↾ pf) G).
                      - have : (∃ G', Some G = Some G' ∧ ((n ↾ pf) : {n | (n < m)%nat}) ∈ G');
                        [ exists G | rewrite -Hpred ]; done.
                      - right; exists (ε (n ↾ pf) (total_lookup cs)), G.
                        split; [ | split ]; try done; intro.
                        rewrite (bnat_eq_intro m _ pf_k pf); apply extends_strong.
                           apply (extends_transitive _
                                 (total_lookup (<[ n := ε (n ↾ pf) (total_lookup cs) ]> cs)));
                        [ apply extends_comm, (total_lookup_insert_extends _ _ pf) |]; done.
                    + rewrite list_lookup_insert_ne in HGsk Hpred; [ rewrite HGsk | done ].
                      destruct H2 as [[Hkelem HGsk']|[c' [G' [? [HGsk' Hc']]]]].
                      - rewrite elem_of_cons in Hkelem. destruct Hkelem as [[= ->]|]; [done|].
                        left; split; [ set_solver | ]; done.
                      - rewrite HGsk' in Hpred.
                        case (elem_of_list_dec (n ↾ pf) G'); intro.
                        ++ have : (∃ G, Some G' = Some G ∧ ((n ↾ pf) : {n | (n < m)%nat}) ∈ G);
                           [ exists G' | rewrite -Hpred ]; done.
                        ++ right; exists c', G'; rewrite list_lookup_insert_ne; [ | done ].
                           split; [ | split ]; try done; intro; apply Hc'.
                           apply (extends_transitive _
                                 (total_lookup (<[ n := ε (n ↾ pf) (total_lookup cs) ]>cs)));
                           [apply extends_comm, (total_lookup_insert_extends _ _ pf) |]; done.
                  }
                  by iApply ("IH_loop" with "[Post] [Hp] [Hsimple_lfp_inv]").
           -- revert E; simplify_eq; case (ε (n ↾ pf) (total_lookup cs)); intros k pf_k.
              case c; intros k' pf_k' E; injection 1; intro; simplify_eq.
  Qed.
End FixSimple.
