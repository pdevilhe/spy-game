From iris.heap_lang     Require Import proofmode notation lang.
From spygame            Require Import type_repr lists maps.

Section ProphList.

  Context `{!heapGS Σ}.

  Fixpoint take_until {A B : Type} (f : A → option B) (xs : list A) : list B :=
    match xs with
    | []      => []
    | x :: xs =>
      match f x with
      | Some y => y :: (take_until f xs)
      | None   => []
      end
    end.

  Definition longest_valid_prefix `{Decodable T} (us : list val) : list T :=
    take_until (λ u, match u with InjLV v => decode' v | _ => None end) us.

  Definition proph_list `{Decodable T} (p : proph_id) (vs : list T) : iProp Σ :=
    (∃ (us : list (val * val)), proph p us ∗
                                ⌜ vs = longest_valid_prefix (map snd us) ⌝)%I.

  Definition resolve_proph_cons : val :=
    λ: "p", λ: "v",
      resolve_proph: "p" to: (InjL "v").

  Definition resolve_proph_nil : val :=
    λ: "p",
      resolve_proph: "p" to: #().

  Definition resolve_proph_list : val :=
    λ: "p", λ: "us",
      list_iter (λ: <> "u", resolve_proph_cons "p" "u") "us".

  Lemma wp_new_proph_list T `{Decodable T} :
    {{{ True }}} NewProph {{{ (vs : list T) (p : proph_id), RET #p; proph_list p vs }}}.
  Proof.
    iIntros (Φ) "_ Post". wp_apply wp_new_proph; [ done | ].
    iIntros (us p) "Hp"; iApply "Post".
    iExists us; iFrame; by iPureIntro.
  Qed.

  Lemma wp_resolve_proph_cons `{D : Decodable T} p vs v :
    {{{ proph_list p vs }}}
      resolve_proph_cons #p (encode' v)
    {{{ (vs' : list T), RET #(); ⌜ vs = v :: vs' ⌝ ∗ proph_list p vs' }}}.
  Proof.
    iIntros (Φ) "Hp Post"; iDestruct "Hp" as (us) "[Hp %]".
    wp_lam; wp_pures; wp_apply (wp_resolve_proph with "Hp").
    iIntros (vs') "[% Hp]"; iApply ("Post" $! (longest_valid_prefix (map snd vs'))); iSplit.
    + iPureIntro; rewrite H H0; unfold longest_valid_prefix; simpl;
      by rewrite (decode_encode' v).
    + iExists vs'; by iFrame.
  Qed.

  Lemma wp_resolve_proph_nil `{D : Decodable T} p vs :
    {{{ proph_list p vs }}} resolve_proph_nil #p {{{ RET #(); ⌜ vs = [] ⌝ }}}.
  Proof.
    iIntros (Φ) "Hp Post"; iDestruct "Hp" as (us) "[Hp %]".
    wp_lam; wp_pures; wp_apply (wp_resolve_proph with "Hp").
    iIntros (vs') "[% Hp]"; iApply "Post"; iPureIntro.
    rewrite H H0; unfold longest_valid_prefix. by simpl.
  Qed.

  Lemma wp_resolve_proph_list `{D : Decodable T} p us vs :
    {{{ proph_list p vs }}}
      resolve_proph_list #p (encode' us)
    {{{ (vs' : list T), RET #(); ⌜ vs = us ++ vs' ⌝ ∗ proph_list p vs' }}}.
  Proof.
    iIntros (Φ) "Hp Post"; wp_lam; wp_pures.
    wp_apply (list_iter_spec _ us (λ n, ∃ vs',
      ⌜ vs = (take n us) ++ vs' ⌝ ∗ proph_list p vs')%I with "[Hp]").
    + iSplit; [ | iExists vs; by iFrame ].
      iIntros (n u); iModIntro; clear Φ; iIntros (Φ) "[Hp %] Post"; wp_pures.
      iDestruct "Hp" as (vs') "[-> Hp]"; iSimplifyEq.
      wp_apply (wp_resolve_proph_cons with "Hp"); iIntros (vs'') "[% Hp]"; iApply "Post".
      iExists vs''. iSimplifyEq. iFrame. iPureIntro.
      rewrite (take_S_r _ _ u) // -app_assoc //.
    + iIntros "Hp"; iDestruct "Hp" as (vs') "[-> Hp]"; iSimplifyEq; iApply "Post"; iFrame.
      by rewrite firstn_all.
  Qed.

End ProphList.

Section ProphOption.

  Context `{!heapGS Σ}.

  Definition proph_option `{D : Decodable T} p v : iProp Σ :=
    (∃ (us : list T), proph_list p us ∗ ⌜ v = us !! 0%nat ⌝)%I.

  Definition resolve_proph_Some := resolve_proph_cons.

  Definition resolve_proph_None := resolve_proph_nil.

  Lemma wp_new_proph_option T `{D : Decodable T} :
    {{{ True }}} NewProph {{{ v p, RET #p; proph_option p v }}}.
  Proof.
    iIntros (Φ) "_ Post". wp_apply wp_new_proph_list; [ done | ].
    iIntros (us p) "Hp"; iApply "Post"; iExists us; iFrame; by iPureIntro.
  Qed.

  Lemma wp_resolve_proph_Some `{D : Decodable T} p v u :
    {{{ proph_option p v }}} resolve_proph_Some #p (encode' u) {{{ RET #(); ⌜ v = Some u ⌝ }}}.
  Proof.
    iIntros (Φ) "Hp Post"; iDestruct "Hp" as (us) "[Hp %]".
    wp_apply (wp_resolve_proph_cons with "Hp"); iIntros (vs') "[% Hp]"; iApply "Post".
    by rewrite H H0 //=.
  Qed.

  Lemma wp_resolve_proph_None `{D : Decodable T} p v :
    {{{ proph_option p v }}} resolve_proph_None #p {{{ RET #(); ⌜ v = None ⌝ }}}.
  Proof.
    iIntros (Φ) "Hp Post"; iDestruct "Hp" as (us) "[Hp %]".
    wp_apply (wp_resolve_proph_nil with "Hp"); iIntros "%"; iApply "Post".
    by subst.
  Qed.

End ProphOption.

Section ProphOneshot.

  Context `{!heapGS Σ}.

  Definition proph_oneshot `{D : Decodable T} `{I : Inhabited T} p z : iProp Σ :=
    (∃ (u : option T), proph_option p u ∗ ⌜ z = default inhabitant u ⌝)%I.

  Definition resolve_proph_oneshot := resolve_proph_cons.

  Lemma wp_new_proph_oneshot T `{D : Decodable T} `{I : Inhabited T} :
    {{{ True }}} NewProph {{{ z p, RET #p; proph_oneshot p z }}}.
  Proof.
    iIntros (Φ) "_ Post". wp_apply (wp_new_proph_option T); [ done | ].
    iIntros (u p) "Hp"; iApply "Post"; iExists u; iFrame; by iPureIntro.
  Qed.

  Lemma wp_resolve_proph_oneshot `{D : Decodable T} `{I : Inhabited T} p z v :
    {{{ proph_oneshot p z }}} resolve_proph_oneshot #p (encode' v) {{{ RET #(); ⌜ z = v ⌝ }}}.
  Proof.
    iIntros (Φ) "Hp Post". iDestruct "Hp" as (u) "[Hp ->]".
    wp_apply (wp_resolve_proph_Some with "Hp"). iIntros "->". by iApply "Post".
  Qed.

End ProphOneshot.

Section ProphMap.

  Context `{Map Σ}.

  Definition proph_map {A B} `{Da : Decodable A, Db : Decodable B, C : Countable A}
    (p : proph_id) (M : gmap A B) : iProp Σ :=
    (∃ (us : list (list (A * B))),
      proph_list p us ∗
      ⌜ M = foldr (λ vs M', (list_to_map vs) ∪ M') ∅ us ⌝)%I.

  Definition resolve_proph_union : val :=
    λ: "p" "m", resolve_proph_cons "p" (map_to_assoc_list "m").

  Definition resolve_proph_empty : val :=
    resolve_proph_nil.

  Lemma wp_new_proph_map A B `{Da : Decodable A, Db : Decodable B, C : Countable A} :
    {{{ True }}}
      NewProph
    {{{ (M : gmap A B) (p : proph_id), RET #p; proph_map p M }}}.
  Proof.
    iIntros (Φ) "_ Post"; wp_apply wp_new_proph_list; [ done | ].
    iIntros (us p) "Hp"; iApply "Post"; iExists us; iFrame; by iPureIntro.
  Qed.

  Lemma wp_resolve_proph_empty {A B} `{Da : Decodable A, Db : Decodable B, C : Countable A} p M :
    {{{ proph_map p (M : gmap A B) }}} resolve_proph_empty #p {{{ RET #(); ⌜ M = ∅ ⌝ }}}.
  Proof.
    iIntros (Φ) "Hp Post"; iDestruct "Hp" as (us) "[Hp ->]".
    wp_apply (wp_resolve_proph_nil p us with "Hp"); iIntros "->"; by iApply "Post".
  Qed.

  Lemma wp_resolve_proph_union {A B} `{Da : Decodable A, Db : Decodable B, Countable A} p m M1 M2 :
    {{{ proph_map p M1 ∗ is_map m M2 }}}
      resolve_proph_union #p m
    {{{ (M3 : gmap A B), RET #(); ⌜ M1 = M2 ∪ M3 ⌝ ∗ proph_map p M3 ∗ is_map m M2 }}}.
  Proof.
    iIntros (Φ) "[Hp Hm] Post"; iDestruct "Hp" as (us) "[Hp ->]"; wp_lam; wp_pures.
    wp_apply (map_to_assoc_list_spec (A:=A) (B:=B) with "Hm"); iIntros "Hm".
    wp_apply (wp_resolve_proph_cons p us (map_to_list M2) with "Hp"); iIntros (vs) "[-> Hp]".
    iSpecialize ("Post" $! (foldr (λ vs, union_with (λ a _, Some a) (list_to_map vs)) ∅ vs)).
    iApply "Post"; iFrame. by rewrite /= list_to_map_to_list.
  Qed.
End ProphMap.
