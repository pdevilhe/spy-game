(* myrec.v

   Virtually every language provides some sort of syntactic construct that
   allows the definition of recursive programs. In OCaml, for example, that
   would be the recursive let: [let rec]. However, it is known that a language
   with support for both mutable stores and first-class functions can emulate
   this functionality. The idea is to store the code of the function that we
   intend to define recursively in a memory location [r] and replace each
   occurrence of a recursive call by a load operation to the location [r].
   This is known in the literature as Landin's knot.

   In this theory, we implement this technique and we verify its correctness.
   We also program and prove a variant of Landin's knot where we combine
   recursion and memoization. It is still another solution to the problem
   detailed in the theory memofix.v.
*)

From iris.base_logic    Require Import invariants.
From iris.heap_lang     Require Import proofmode notation lang.
From iris.heap_lang.lib Require Import spin_lock.
From spygame            Require Import maps lock_lib memoize type_repr.

Section MyRec.

  Context `{Map Σ} `{lockG Σ}.
  Let N := nroot .@ "my_rec".

  Definition myrec : val := λ: "ff",
    let: "r" := ref #() in
    "r" <- (λ: "x", "ff" (! "r") "x");;
    ! "r".

  Definition myrec_memo : val := λ: "ff",
    let: "r" := ref #() in
    "r" <- memoize_conc (λ: "x", "ff" (! "r") "x");;
    ! "r".

  Lemma myrec_spec `{Decodable A, Decodable B}
    (P : A     → iProp Σ)
    (Q : A → B → iProp Σ)
    (ff : val) :

    {{{ (∀ (f : val),
          {{{ (∀ a, {{{ P a }}} f (encode' a) {{{ b, RET (encode' b); Q a b }}}) }}}
            ff f
          {{{ (g : val), RET g;
              (∀ a, {{{ P a }}} g (encode' a) {{{ b, RET (encode' b); Q a b }}})
          }}})
    }}}
      myrec ff
    {{{ (g : val), RET g;
              (∀ a, {{{ P a }}} g (encode' a) {{{ b, RET (encode' b); Q a b }}})
    }}}.

  Proof.
    iIntros (Φ) "#ff_spec Post".
    wp_lam; wp_alloc r as "Hr"; wp_pures; wp_store.

    (* Invariant allocation: (r ↦ (λ x, ff (! r) x)). *)
    iMod (inv_alloc N with "Hr") as "#Inv".

    (* Open the invariant and close it immediately after. *)
    iInv N as "Hopen" "Hclose"; wp_load.
    iMod ("Hclose" with "[Hopen]") as "_"; [ iFrame | iModIntro ].

    (* Löb's induction:  ▷ (∀ a, { P a } (λ x, ff (! r) x) a { b. Q a b }). *)
    iApply "Post". iLöb as "IH".

    (* Proof that the output program satisfies the postcondition. *)
    iIntros (n); iModIntro; clear Φ; iIntros (Φ) "HP Post".
    (* Focus on the atomic expression: (! #r). *)
    wp_pures. wp_bind (! _)%E.

    (* Open the invariant and close it immediately after. *)
    iInv N as "Hopen" "Hclose"; wp_load.
    iMod ("Hclose" with "[Hopen]") as "_"; [ iFrame | iModIntro ].

    (* Apply ff spec together with Löb's induction hypothesis. *)
    wp_apply ("ff_spec" with "IH").
    iIntros (g) "g_spec". by wp_apply ("g_spec" with "HP").
  Qed.


  Lemma myrec_memo_spec `{Decodable A, Decodable B}
    (P : A     → iProp Σ)
    (Q : A → B → iProp Σ)
    (ff : val) :

    {{{ (∀ (f : val),
          {{{ (∀ a, {{{ □ P a }}} f (encode' a) {{{ b, RET (encode' b); □ Q a b }}}) }}}
            ff f
          {{{ (g : val), RET g;
              (∀ a, {{{ □ P a }}} g (encode' a) {{{ b, RET (encode' b); □ Q a b }}})
          }}})
    }}}
      myrec_memo ff
    {{{ (g : val), RET g;
              (∀ a, {{{ □ P a }}} g (encode' a) {{{ b, RET (encode' b); □ Q a b }}})
    }}}.

  Proof.
    iIntros (Φ) "#ff_spec Post". wp_lam; wp_alloc r as "Hr"; wp_pures.
    (* We unfold the definition of [memoize_conc], because the function it
       receives as an argument does not satisfy the precondition of the
       specification we prove in the theory memoize.v. The precondition
       becomes valid only after the write to the location r is completed.
    *)
    unfold memoize_conc. wp_pures.
    wp_apply (make_map_spec A B); [done|]. iIntros (m) "Hm". wp_pures.

    (* Open the invariant and close it immediately after. *)
    wp_apply (newlock_spec (∃ (map : gmap A B),
      is_map m map ∗ ∀ a b, ⌜ map !! a = Some b ⌝ → □ Q a b)%I
    with "[Hm]").
    { iExists ∅. iFrame. iIntros (a b). rewrite lookup_empty; by iIntros "%". }
    iIntros (lk γ) "#Hlk". wp_pures. wp_store.

    (* Invariant allocation: (r ↦ (λ x, match ... end)). *)
    iMod (inv_alloc N with "Hr") as "#Inv".

    (* Open the invariant and close it immediately after. *)
    iInv N as "Hopen" "Hclose"; wp_load.
    iMod ("Hclose" with "[Hopen]") as "_";[iFrame|iModIntro ].

    (* Löb's induction:  ▷ (∀ a, { □ P a } (λ x, match ... end) a { b. □ Q a b }). *)
    iApply "Post". iLöb as "IH".
    iIntros (a). iModIntro. clear Φ. iIntros (Φ) "#HP Post". wp_lam.

    unfold withLock. wp_pures.
    (* Acquire the lock: perform map lookup. *)
    wp_apply (acquire_spec with "Hlk").
    iIntros "[Hlocked Hmap]". iDestruct "Hmap" as (map) "[Hmap #HQ]".
    wp_pures. wp_apply (map_lookup_spec m map with "Hmap"). iIntros "Hmap". wp_let.
    (* Release the lock. *)
    wp_apply (release_spec γ lk _ with "[Hlk Hlocked Hmap HQ]");[|iIntros "_"].
    { iFrame. iSplitL ""; [iApply "Hlk"|iExists map; iFrame]; done. }

    case_eq (map !! a).
    (* Case where the argument x has already been seen. *)
    + intros y lookup_map. wp_pures. iApply "Post". by iApply "HQ".
    (* Case where the argument x has never been seen. *)
    + intro lookup_map. wp_pures.
      (* Focus on the atomic expression: (! #r). *)
        wp_bind (! _)%E.

      (* Open the invariant and close it immediately after. *)
      iInv N as "Hopen" "Hclose"; wp_load.
      iMod ("Hclose" with "[Hopen]") as "_";[iFrame|iModIntro].

      (* Apply ff spec together with Löb's induction hypothesis. *)
      wp_apply ("ff_spec" with "IH").
      iIntros (g) "#g_spec". wp_apply ("g_spec" with "HP").
      iIntros (b) "#Q". wp_pures.
      (* Acquire the lock: addition of new key/value pair into the map. *)
      wp_apply (acquire_spec with "Hlk").
      iIntros "[Hlocked Hmap]". iDestruct "Hmap" as (map') "[Hmap' #HQ']".
      wp_pures. wp_apply (map_insert_spec m map' a b with "Hmap'"). iIntros "Hmap'".
      (* Release the lock. *)
      wp_pures.
      wp_apply (release_spec γ lk _ with "[Hlk Hlocked Hmap' HQ' Q]"); [|iIntros "_"].
      { iFrame. iSplitL ""; [iApply "Hlk"|].
        iExists (<[a:=b]> map'). iFrame.
        iIntros (a' b'). rewrite lookup_insert_Some.
        iIntros ([[-> ->]|[_ ?]]); first done. by iApply "HQ'". }
      wp_pures. by iApply "Post".
  Qed.

End MyRec.
