From iris.heap_lang Require Import proofmode notation lang.

Section ValEncoding.

  Class Encodable A := encode' : A → val.
  Class Decodable A `{E : Encodable A} := {
    decode' : val → option A;
    decode_encode' t : decode' (encode' t) = Some t
  }.

  (* Encodable instances. *)
  Global Instance    val_encoding : Encodable val  := λ v, v.
  Global Instance    nat_encoding : Encodable nat  := λ n, #n.
  Global Instance    int_encdoing : Encodable Z    := λ z, #z.
  Global Instance   bool_encoding : Encodable bool := λ b, #b.
  Global Instance   unit_encoding : Encodable unit := λ _, #().
  Global Instance   bnat_encoding m : Encodable { n | (n < m)%nat } := λ bn, #(`bn).
  Global Instance option_encoding `{Encodable A} : Encodable (option A) :=
    λ t, match t with None => NONEV | Some a => SOMEV (encode' a) end.
  Global Instance   list_encoding `{Encodable A} : Encodable (list A) :=
    fold_right (λ a acc, SOMEV (PairV (encode' a) acc)) NONEV.
  Global Instance    sum_encoding `{Encodable A, Encodable B} : Encodable (A + B) :=
    λ t, match t with inl a => InjLV (encode' a) | inr b => InjRV (encode' b) end.
  Global Instance   prod_encoding `{Encodable A, Encodable B} : Encodable (A * B) :=
    λ t, match t with (a, b) => PairV (encode' a) (encode' b) end.

  (* Decodable instances. *)
  Global Instance val_decoding : Decodable val := {
    decode' := Some; decode_encode' := λ l, eq_refl (Some l)
  }.
  Global Program Instance nat_decoding : Decodable nat := {
    decode' := λ v, match v return _ with LitV (LitInt n) => Some (Z.abs_nat n) | _ => None end
  }.
  Next Obligation. intro t; unfold encode'; simpl; by rewrite Zabs2Nat.id. Qed.
  Global Instance int_decoding : Decodable Z := {
    decode' := λ v, match v with LitV (LitInt n) => Some n | _ => None end;
    decode_encode' := λ l, eq_refl (Some l)
  }.
  Global Instance bool_decoding : Decodable bool := {
    decode' := λ v, match v with LitV (LitBool b) => Some b | _ => None end;
    decode_encode' := λ l, eq_refl (Some l)
  }.
  Global Instance unit_decoding : Decodable unit := {
    decode' := λ v, match v with LitV LitUnit => Some () | _ => None end;
    decode_encode' := λ l, match l with () => eq_refl (Some ()) end
  }.
  Global Program Instance bnat_decoding m : Decodable { n | (n < m)%nat } := {
    decode' := λ v,
      match v return _ with
      | LitV (LitInt n) =>
        let n := Z.abs_nat n in
        match lt_dec n m return _ with
        | left  pf => Some (exist _ n pf)
        | right _  => None
        end
      | _ => None
      end
  }.
  Next Obligation.
    intros m. case; intros n pf; simpl.
    case (lt_dec (Z.abs_nat n) m); rewrite Zabs2Nat.id; intro pf'; [ | contradiction ].
    rewrite (_ : n ↾ pf = n ↾ pf'); [ | apply sig_eq_pi; [ intro; apply Nat.lt_pi | ] ]; done.
  Qed.
  Global Program Instance option_decoding `{Decodable A} : Decodable (option A) := {
    decode' := λ v, match v return _ with SOMEV a => Some (decode' a) | NONEV => Some None | _ => None end;
  }.
  Next Obligation. intros ???. case; simpl; [|done]. intro. by rewrite decode_encode'. Qed.
  Global Program Instance list_decoding `{Decodable A} : Decodable (list A) := {
    decode' :=
      (fix decode_ v :=
        match v return _ with
        | NONEV => option_ret _ []
        | SOMEV (a, bs) =>
          decode'  a ≫= λ u,
          decode_ bs ≫= λ us,
          option_ret _ (u :: us)
        | _ => None
        end)
  }.
  Next Obligation.
    induction t as [ | v vs ]; [ by simpl | ]; intros; simpl; rewrite decode_encode' IHvs //=.
  Qed.
  Global Program Instance sum_decoding `{Decodable A, Decodable B} : Decodable (A + B) := {
    decode' := λ v,
      match v return _ with
      | InjLV u => decode' u ≫= λ a, option_ret _ (inl a)
      | InjRV u => decode' u ≫= λ b, option_ret _ (inr b)
      | _ => None
      end }.
  Next Obligation. intros ??????. case; intro u; simpl; rewrite ?decode_encode'; done. Qed.
  Global Program Instance prod_decoding `{Decodable A, Decodable B} : Decodable (A * B) := {
    decode' := λ v,
      match v return _ with
      | PairV u v =>
        decode' u ≫= λ a,
        decode' v ≫= λ b,
        option_ret _ (a, b)
      | _ => None
      end }.
  Next Obligation. intros ??????. case; intros a b; simpl; do 2 rewrite decode_encode'; done. Qed.

  Lemma encode_injective `{Decodable A} (u v : A) : encode' u = encode' v → u = v.
  Proof. intro eq; specialize (decode_encode' u); rewrite eq decode_encode'; by case. Qed.

  Global Instance decodable_inj `{Decodable A} : Inj eq eq encode' := encode_injective.

  Global Instance decodable_eq_dec `{Decodable A} : EqDecision A.
  Proof. by apply (inj_eq_dec encode'). Qed.

  Global Instance decodable_countable `{Decodable A} : Countable A.
  Proof. by apply (inj_countable encode' decode' decode_encode'). Qed.

End ValEncoding.


From TLC Require Import LibFix.

Section FunEncoding.

  Context `{!heapGS Σ}.

  Inductive ArrowRepr : Type :=
  | Data T `{Encodable T} : ArrowRepr
  | Func : ArrowRepr → ArrowRepr → ArrowRepr
  | Part : ArrowRepr → ArrowRepr → ArrowRepr.

  Fixpoint interp (τ : ArrowRepr) : Type :=
    match τ with
    | Data T    => T
    | Func τ τ' => (interp τ)  -> (interp τ')
    | Part τ τ' => (interp τ) --> (interp τ')
    end.

  Fixpoint implements (τ : ArrowRepr) (f : val) : interp τ → iProp Σ :=
    match τ with
    | Data T    => λ X, (⌜ f = encode' X ⌝)%I
    | Func τ τ' => λ F,
      (∀ x X, {{{ implements τ x X }}} f x {{{ y, RET y; implements τ' y (F X) }}})%I
    | Part τ τ' => λ F,
      (∀ x X, {{{ implements τ x X }}} f x {{{ y, RET y; implements τ' y (F X) ∗ ⌜ dom F X ⌝ }}})%I
    end.

  Fixpoint implements_seq (τ : ArrowRepr) (f : val) : interp τ → iProp Σ → iProp Σ :=
    match τ with
    | Data T    => λ X _, (⌜ f = encode' X ⌝)%I
    | Func τ τ' => λ F S,
      (∀ x X, {{{ S ∗ implements_seq τ x X S }}} f x {{{ y, RET y; S ∗ ∀ S, implements_seq τ' y (F X) S }}})%I
    | Part τ τ' => λ F S,
      (∀ x X, {{{ S ∗ implements_seq τ x X S }}} f x {{{ y, RET y; S ∗ ∀ S, implements_seq τ' y (F X) S ∗ ⌜ dom F X ⌝ }}})%I
    end.

  Lemma implements_part_entails_implements_func τ τ' f F :
    implements (Part τ τ') f F ⊢ implements (Func τ τ') f F.
  Proof.
    iIntros "#spec"; iIntros (x X); iModIntro; iIntros (Φ) "Hx Post".
    iApply ("spec" $! x X with "Hx"); iNext; iIntros (y) "[Hy _]"; by iApply "Post".
  Qed.

  Lemma implements_func_entails_implements_part τ τ' f F :
    implements (Func τ τ') f F ⊢ implements (Part τ τ') f (Build_partial F (λ _, True : Prop)).
  Proof.
    iIntros "#spec"; iIntros (x X); iModIntro; iIntros (Φ) "Hx Post".
    iApply ("spec" $! x X with "Hx"); iNext; iIntros (y) "Hy"; iApply "Post"; iSplit; done.
  Qed.

End FunEncoding.

Section RewritingLemmas.

  Lemma encode_unit `{Encodable A} : encode' () = #().
  Proof. unfold encode'; by simpl. Qed.

  Lemma encode_nil `{Encodable A} : encode' [] = NONEV.
  Proof. unfold encode'; by simpl. Qed.

  Lemma encode_cons `{Encodable A} u us :
    encode' (u :: us) = SOMEV (PairV (encode' u) (encode' us)).
  Proof. unfold encode'; simpl; unfold encode'; done. Qed.

  Lemma encode_pair `{Encodable A, Encodable B} (a : A) (b : B) :
    encode' (a, b) = PairV (encode' a) (encode' b).
  Proof. unfold encode'; simpl; unfold encode'; done. Qed.

  Lemma encode_sum_l `{Encodable A} a : encode' (inl a) = InjLV (encode' a).
  Proof. unfold encode'; by simpl. Qed.

  Lemma encode_sum_r `{Encodable B} b : encode' (inr b) = InjRV (encode' b).
  Proof. unfold encode'; by simpl. Qed.

End RewritingLemmas.
