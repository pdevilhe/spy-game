(* dir_graphs.v

   We introduce some basic definitions and lemmas about graphs. This is only
   a preliminary development though. We plan to make the following adjustements:
   - Introduce a general type class for graphs.
   - Rebuild the present development as an instance of the above type class, that
     is, reintroduce our representation of graphs as a particular choice of
     implementation of graphs (using adjancy lists).
   - Integrate the [connected_component] construction into the general theory of
     fixpoints (here, we develop the particular result that a monotone function
     in the domain of finite sets admits a fixpoint).
   - Revise the question of directed and undirected graphs. In fact, the current
     development has support for undirected graphs, however the definition of
     acyclic makes sense only in the directed case.
*)

From stdpp Require Import relations base option gmap list mapset.

Section fin_sets_ext.

  Lemma set_choose_or_empty' `{FinSet A C} (X : C) : {x | x ∈ X} + {X ≡ ∅}.
  Proof.
    destruct (elements X) as [|x l] eqn:HX; [right|left].
    - apply equiv_empty; intros x. by rewrite <-elem_of_elements, HX, elem_of_nil.
    - exists x. rewrite <-elem_of_elements, HX. by left.
  Qed.

  Lemma set_indunction_type_L {A} `{Countable A} (P : gset A → Type) :
    P ∅ → (∀ x X, x ∉ X → P X → P ({[ x ]} ∪ X)) → ∀ X, P X.
  Proof.
    intros Hemp Hadd. apply well_founded_induction_type with (⊂).
    { apply set_wf. }
    intros X IH. destruct (set_choose_or_empty' X) as [[x ?]|HX].
    - rewrite (union_difference_L {[ x ]} X) by set_solver.
      apply Hadd. set_solver. apply IH; set_solver.
    - rewrite leibniz_equiv_iff in HX. by rewrite HX.
  Qed.

End fin_sets_ext.

Section relations_ext.

  Lemma nsteps_succ_inv {A} (R : relation A) n x z :
    nsteps R (S n) x z → ∃ y, nsteps R n x y ∧ R y z.
  Proof.
    revert x. induction n; intro x.
    + intro nsteps_once. exists x.
      split;[apply nsteps_O|apply nsteps_once_inv];done.
    + inversion 1. destruct (IHn _ H2) as [w [Hw1 Hw2]].
      exists w. split;[|done]. by apply (nsteps_l _ _ _ y).
  Qed.

  Lemma nsteps_succ_inv' {A} (R : relation A) n x y :
    nsteps R (S n) x y → ∃ zs, length zs = n ∧ Forall2 R (x :: zs) (zs ++ [y]).
  Proof.
    revert n y. induction n as [|n IH].
    + intros y nsteps_once. exists []. split;[done|].
      apply Forall2_cons;[|done]. by apply nsteps_once_inv.
    + intros z nsteps_succ_succ.
      destruct (nsteps_succ_inv _ _ _ _ nsteps_succ_succ) as [y [nsteps_xy Ryz]].
      destruct (IH _ nsteps_xy) as [zs [zs_len Forall2_zs]].
      exists (zs ++ [y]). split.
      - rewrite app_length, zs_len. simpl. lia.
      - rewrite app_comm_cons. apply Forall2_app;[|apply Forall2_cons];done.
  Qed.

  Lemma nsteps_succ {A} (R : relation A) x y zs :
    Forall2 R (x :: zs) (zs ++ [y]) → nsteps R (S (length zs)) x y.
  Proof.
    revert zs x. induction zs as [|z zs].
    + intro x. simpl. intro HForall2. apply nsteps_once.
      by apply (proj1 (Forall2_cons_inv _ _ _ _ _ HForall2)).
    + intro x. rewrite <-app_comm_cons. intros HForall2.
      destruct (Forall2_cons_inv _ _ _ _ _ HForall2) as [Rxz HForall2_tl].
      apply (nsteps_l _ _ x z);[|apply IHzs];done.
  Qed.

  Lemma nsteps_succ_iff {A} (R : relation A) n x y :
    nsteps R (S n) x y ↔ ∃ zs, length zs = n ∧ Forall2 R (x :: zs) (zs ++ [y]).
  Proof.
    split;[by apply nsteps_succ_inv'|].
    destruct 1 as [zs [zs_len Hzs2]]. rewrite <-zs_len.
    by apply nsteps_succ.
  Qed.

End relations_ext.

(* TODO: In the same lines as the theory fin_maps, redefine graphs
         more abstracly as a type class (hints bellow) and then work
         on a specific implementation such as the one we studied here.
*)

(*
Class Vertices (V G : Type) := vertices : G -> V.
Class Edges (E G : Type) := edges : G -> E.

Class DirGraph A V E G `{Vertices V G, Edges E G, FinSet A V, FinSet (A * A) E, Lookup A V G} := {
  vertices_of_edges (g : G) : ∀ e, e ∈ edges g → e.1 ∈ vertices g ∧ e.2 ∈ vertices g;
  neighbors_incl (g : G) (u : A) : is_Some (g !! u) → u ∈ vertices g;
  neighbors_correct (g : G) (u v : A) (vs : V) : g !! u = Some vs → v ∈ vs → (u, v) ∈ edges g
}.

Definition outgoing_edges {A} `{Countable A} (g : dir_graph A) : gmap A (gset (A * A)) :=
  map_imap (λ u vs, Some (list_to_set ((λ v, (u, v)) <$> elements vs))) (neighbors A g).
Definition edges {A} `{Countable A} (g : dir_graph A) : gset (A * A) :=
  map_fold (λ _, (∪)) ∅ (outgoing_edges g).
*)

Record dir_graph A `{Countable A} := Dir_graph {
  neighbors : gmap A (gset A);
  lookup_neighbors : ∀ u vs, neighbors !! u = Some vs → vs ⊆ dom neighbors
}.

Definition vertices {A} `{Countable A} (G : dir_graph A) : gset A := dom (neighbors A G).
Definition edges {A} `{Countable A} (G : dir_graph A) : gset (A * A) :=
 Mapset (gmap_curry ((mapset_car : gset A → gmap A unit) <$> (neighbors A G))).
Definition edge_rel {A} `{Countable A} (G : dir_graph A) : relation A :=
  λ u v, (u, v) ∈ edges G.
Instance edge_rel_dec {A} `{Countable A} (G : dir_graph A) : RelDecision (edge_rel G) :=
  λ u v, gset_elem_of_dec (u, v) (edges G).
Definition path_rel {A} `{Countable A} (G : dir_graph A) : relation A := rtc (edge_rel G).

Lemma elem_of_edges1 `{Countable A} G u v :
  (u, v) ∈ edges G → ∃ vs, neighbors _ G !! u = Some vs ∧ v ∈ vs.
Proof.
  unfold edges, elem_of, gset_elem_of, mapset_elem_of. simpl.
  intro in_edges. rewrite lookup_gmap_curry, lookup_fmap in in_edges.

  case_eq (neighbors _ G !! u).
  + intros vs Some_neighbors. rewrite Some_neighbors in in_edges.
    simpl in in_edges. exists vs. done.
  + intros None_neighbors. rewrite None_neighbors in in_edges.
    simpl in in_edges. done.
Qed.

Lemma elem_of_edges2 `{Countable A} G u v vs :
  neighbors _ G !! u = Some vs → v ∈ vs → (u, v) ∈ edges G.
Proof.
  unfold edges, elem_of, gset_elem_of, mapset_elem_of. simpl.
  intro Some_neighbors.
  by rewrite lookup_gmap_curry, lookup_fmap, Some_neighbors.
Qed.

Lemma elem_of_edges_iff `{Countable A} G u v :
  (u, v) ∈ edges G ↔ ∃ vs, neighbors _ G !! u = Some vs ∧ v ∈ vs.
Proof.
  split.
  + by apply elem_of_edges1.
  + destruct 1 as [vs [Hvs in_vs]].
    by apply (elem_of_edges2 _ _ _ vs).
Qed.

Lemma elem_of_edges3 `{Countable A} G u v :
  (u, v) ∈ edges G → u ∈ vertices G ∧ v ∈ vertices G.
Proof.
  rewrite elem_of_edges_iff. destruct 1 as [vs [Hvs in_vs]].
  split.
  + apply elem_of_dom. by exists vs.
  + by apply (lookup_neighbors A _ _ _ Hvs).
Qed.

Instance nsteps_rel_dec {A} `{Countable A} G n : RelDecision (nsteps (edge_rel G) n).
Proof.
  revert n. induction n.
  + intros x y. case (decide (x = y)).
    - intros ->. left. by apply nsteps_O.
    - intro ineq. right. inversion 1. done.
  + intros x z.
    set (P y := edge_rel G x y ∧ nsteps (edge_rel G) n y z).
    assert (∀ y, Decision (P y)) as dec_P.
    { intro y. apply (and_dec (edge_rel_dec _ _ _) (IHn _ _)). }
    assert (Decision (set_Exists P (vertices G))) as dec_Exists.
    { by apply set_Exists_dec. }
    case dec_Exists.
    - intro Hexists. left. destruct Hexists as [y [in_vertices [Hy Hy']]].
      by apply (nsteps_l _ _ _ y).
    - intro Hnot_exists. right.
      inversion 1 as [|m x' y z' Hedge_rel Hnsteps eq_n eq_x eq_z].
      apply (not_set_Exists_Forall P _ Hnot_exists y);[|done].
      by apply (proj2 (elem_of_edges3 G x y Hedge_rel)).
Qed.

Definition all_neighbors {A} `{Countable A} (G : dir_graph A) : gset A → gset A :=
  λ S, foldr (∪) ∅ ((λ x, default ∅ (neighbors A G !! x)) <$> elements S).

Lemma all_neighbors_empty {A} `{Countable A} (G : dir_graph A) : all_neighbors G ∅ = ∅.
Proof. done. Qed.

Lemma foldr_union `{Countable A} Xs Y :
  foldr (∪) (Y : gset A) Xs = Y ∪ foldr (∪) ∅ Xs.
Proof.
  revert Y. induction Xs as [|X Xs IH].
  + intro Y. simpl. rewrite union_empty_r_L. done.
  + intro Y. simpl. rewrite IH. set_solver.
Qed.

Lemma all_neighbors_incl {A} `{Countable A} (G : dir_graph A) x S :
  x ∈ S → default ∅ (neighbors A G !! x) ⊆ all_neighbors G S.
Proof.
  intros in_S. unfold all_neighbors.
  rewrite <-(elem_of_elements S x) in in_S.
  destruct (elem_of_list_split _ _ in_S) as [ys [xs ->]].
  rewrite fmap_app, fmap_cons, foldr_app. simpl.
  rewrite (foldr_union (_ <$> ys)). set_solver.
Qed.

Lemma all_neighbors_insert {A} `{Countable A} (G : dir_graph A) x S :
  all_neighbors G ({[x]} ∪ S) = (default ∅ (neighbors A G !! x)) ∪ all_neighbors G S.
Proof.
  case (gset_elem_of_dec x S);[intro in_S|intro not_in_S].
  + rewrite subseteq_union_1_L, subseteq_union_1_L. done.
    - apply all_neighbors_incl. done.
    - rewrite <-elem_of_subseteq_singleton. done.
  + assert (∀ (f : A → gset A),
      foldr (∪) ∅ (f <$> elements ({[x]} ∪ S)) =
      foldr (∪) ∅ (f <$> x :: elements S))
    as aux_lemma.
    {  intro f. apply foldr_permutation_proper.
       + split; repeat intro; congruence.
       + intros X Y Y' ->. done.
       + intros X Y Z. set_solver.
       + apply Permutation_map', elements_union_singleton. done.
    }
    unfold all_neighbors. rewrite aux_lemma. done.
Qed.

Lemma all_neighbors_union {A} `{Countable A} (G : dir_graph A) X Y :
  all_neighbors G (X ∪ Y) = all_neighbors G X ∪ all_neighbors G Y.
Proof.
  induction X as [|x X not_in_X IH] using set_ind_L.
  + rewrite all_neighbors_empty. do 2 rewrite union_empty_l_L. done.
  + rewrite <-union_assoc_L.
    do 2 rewrite all_neighbors_insert. rewrite IH, union_assoc_L. done.
Qed.

Lemma elem_of_all_neighbors {A} `{Countable A} G S v :
  v ∈ all_neighbors G S ↔ ∃ u, u ∈ S ∧ (u, v) ∈ edges G.
Proof.
  induction S as [| x S not_in IH] using set_ind_L; split.
  + rewrite all_neighbors_empty. done.
  + destruct 1 as [u [in_empty _]]. done.
  + rewrite all_neighbors_insert. rewrite elem_of_union.
    intro in_union. case in_union.
    - case_eq (neighbors A G !! x);[|done].
      simpl. intros vs lookup_neighbors_Some in_vs. exists x. split.
      ++ rewrite elem_of_union, elem_of_singleton. by left.
      ++ rewrite elem_of_edges_iff. exists vs. split; done.
    - rewrite IH. destruct 1 as [u [in_S in_edges]].
      exists u. split;[|done]. rewrite elem_of_union. by right.
  + destruct 1 as [u [in_union in_edges]]; revert in_union.
    rewrite elem_of_union, elem_of_singleton. destruct 1 as [eq_x|in_S].
    - revert in_edges. rewrite all_neighbors_insert, elem_of_edges_iff, eq_x.
      destruct 1 as [vs [-> in_vs]]. simpl. rewrite elem_of_union. by left.
    - rewrite all_neighbors_insert, elem_of_union. right.
      rewrite IH. exists u. split; done.
Qed.

Lemma all_neighbors_incl_vertices {A} `{Countable A} G S :
  all_neighbors G S ⊆ vertices G.
Proof.
  intro v. rewrite elem_of_all_neighbors.
  destruct 1 as [u [in_S in_edges]].
  apply (proj2 (elem_of_edges3 _ _ _ in_edges)).
Qed.

Section fun_pow_def.

  Definition fun_pow {A} (f : A → A) :=
    (fix go (n : nat) x := match n with 0 => x | S m => f (go m x) end).

  Lemma fun_pow_zero {A} (f : A → A) x : fun_pow f 0 x = x.
  Proof. by simpl. Qed.

  Lemma fun_pow_succ {A} (f : A → A) n x : fun_pow f (S n) x = f (fun_pow f n x).
  Proof. by simpl. Qed.

End fun_pow_def.

Section fun_pow.

  Context {A} `{Countable A} (f : gset A → gset A) (Y : gset A).
  Context (f_bounded : ∀ X, X ⊆ Y → f X ⊆ Y).
  Context (f_prefixpoints : ∀ X, X ⊆ Y → X ⊆ f X).

  Lemma fun_pow_incl n X : X ⊆ Y → fun_pow f n X ⊆ Y.
  Proof. induction n as [|m IH];[simpl|intro; apply f_bounded, IH]; done. Qed.

  Lemma fun_pow_mono n X : X ⊆ Y → fun_pow f n X ⊆ fun_pow f (S n) X.
  Proof. intros incl_Y. apply f_prefixpoints, fun_pow_incl. done. Qed.

  Lemma fun_pow_trivial X n m :
    fun_pow f n X = fun_pow f (S n) X →
    fun_pow f (n + m) X = fun_pow f n X.
  Proof.
    induction m as [|m IH].
    + rewrite <-plus_n_O. done.
    + intro IH_pre. specialize (IH IH_pre).
      rewrite PeanoNat.Nat.add_succ_r, fun_pow_succ, IH. done.
  Qed.

  Lemma fun_pow_fixpoint X :
    X ⊆ Y → fun_pow f (size Y) X = fun_pow f (S (size Y)) X.
  Proof.
    intro incl_Y.

    set (P := λ i, fun_pow f i X ⊂ fun_pow f (S i) X).

    assert (∀ i, Decision (P i)) as P_dec.
    { intro i. case (gset_eq_dec (fun_pow f i X) (fun_pow f (S i) X)).
      + intro eq. right. intro incl.
        cut (size (fun_pow f i X) < size (fun_pow f (S i) X));
        [rewrite eq; lia|apply subset_size; done].
      + intro ineq. left.
        case (set_subseteq_inv_L _ _ (fun_pow_mono i X incl_Y)); done.
    }

    destruct (List.Forall_Exists_dec _ P_dec 
               (imap (λ i _, size Y - i) (replicate (S (size Y)) 0)))
    as [Hforall|Hexists].
    + assert (size Y < size (fun_pow f (S (size Y)) X)) as gt_size_Y.
      { revert Hforall. induction (size Y).
        + simpl. rewrite Forall_cons. simpl. intros [incl _].
          apply (PeanoNat.Nat.le_lt_trans _ (size X));[lia|].
          apply subset_size. done.
        + intro Hforall.
          destruct (Forall_cons_1 _ _ _ Hforall) as [incl Hforall'].
          simpl. simpl in incl.
          apply (PeanoNat.Nat.le_lt_trans _ (size (f (fun_pow f n X))));
          [apply IHn|apply subset_size];done.
      }
      assert (size (fun_pow f (S (size Y)) X) ≤ size Y) as le_size_Y.
      { apply subseteq_size. by apply fun_pow_incl. }
      cut (size Y < size Y);lia.
    + rewrite Exists_exists in Hexists.
      destruct Hexists as [i [in_imap Hi]].
      case (set_not_subset_inv_L _ _ Hi).
      - specialize (fun_pow_mono i X incl_Y). done.
      - revert in_imap. rewrite elem_of_lookup_imap.
        destruct 1 as [j [k [eq lookup]]].
        specialize (lookup_lt_Some _ _ _ lookup).
        rewrite replicate_length. intros lt_succ_size_Y fixpoint.
        specialize (fun_pow_trivial X i j fixpoint).
        cut (i + j = size Y);[|lia]. simpl. intros -> ->. done.
  Qed.

End fun_pow.

Definition connected_component {A} `{Countable A} (G : dir_graph A) X :=
  fun_pow (λ X, X ∪ all_neighbors G X) (size (vertices G)) (X ∩ vertices G).

Lemma elem_of_connected_component1 {A} `{Countable A} (G : dir_graph A) X v :
  v ∈ connected_component G X ↔ ∃ u, u ∈ (X ∩ vertices G) ∧ path_rel G u v.
Proof.
  set (f := λ X, X ∪ all_neighbors G X).
  assert (∀ X, X ⊆ vertices G → f X ⊆ vertices G) as f_bounded.
  { clear X. intros X incl_V. unfold f.
    apply (union_least _ _ _ incl_V). apply all_neighbors_incl_vertices.
  }
  assert (∀ X, X ⊆ f X) as f_prefixpoints.
  { intro Z. apply union_subseteq_l. }
  assert (∀ n X, fun_pow f (S n) X =
              (fun_pow f    n  X) ∪ (all_neighbors G (fun_pow f n X)))
  as unfold_f.
  { intros n Y. unfold f. done. }

  assert (∀ X n v,
    v ∈ fun_pow f n X ↔
    ∃ u, u ∈ X ∧ (∃ m, m ≤ n ∧ nsteps (edge_rel G) m u v))
  as aux_lemma.
  { clear v. intro Y. induction n as [|n IH].
    + intro v. simpl. split;[intro in_X|].
      - exists v. split;[done|].
        exists 0. split;[lia|]. apply nsteps_O.
      - destruct 1 as [u [in_X [m [eq_0 msteps]]]].
        rewrite Nat.le_0_r in eq_0. rewrite eq_0 in msteps.
        inversion msteps. rewrite H2 in in_X. done.
    + intro w. split.
      - rewrite unfold_f, elem_of_union.
        intro in_union. case in_union.
        ++ intro IH_pred. rewrite IH in IH_pred.
           destruct IH_pred as [u [in_X [m [le_n msteps]]]].
           exists u. split;[done|]. exists m. split;[lia|done].
        ++ rewrite elem_of_all_neighbors.
           destruct 1 as [v [in_fun_pow in_edges]].
           rewrite IH in in_fun_pow.
           destruct in_fun_pow as [u [in_X [m [le_n msteps]]]].
           exists u. split;[done|]. exists (S m). split;[lia|].
           apply (nsteps_r _ _ v); done.
      - destruct 1 as [u [in_X [m [le_succ msteps]]]].
        inversion le_succ as [eq_succ|k le_n eq_k].
        ++ rewrite eq_succ in msteps.
           destruct (nsteps_succ_inv _ _ _ _ msteps) as [v [nsteps_rel in_edge]].
           rewrite unfold_f, elem_of_union, elem_of_all_neighbors. right.
           exists v. split;[|done]. rewrite IH.
           exists u. split;[done|]. exists n. split;[lia|done].
        ++ rewrite unfold_f, elem_of_union, IH. left.
           exists u. split;[done|]. exists m. split;done.
  }

  split.
  + unfold connected_component. unfold f in aux_lemma. rewrite aux_lemma.
    destruct 1 as [u [in_inter [m [_ msteps]]]]. exists u. split;[done|].
    apply (nsteps_rtc m). done.
  + destruct 1 as [u [in_X path_rel_u_v]].
    destruct (rtc_nsteps _ _ path_rel_u_v) as [m msteps].
    case (le_gt_dec m (size (vertices G))) as [le_size_V|gt_size_V].
    - unfold connected_component. unfold f in aux_lemma. rewrite aux_lemma.
      exists u. split;[|exists m;split];done.
    - cut (v ∈ fun_pow f m (X ∩ vertices G)).
      ++ unfold connected_component.
         cut (m = (size (vertices G)) + (m - size (vertices G)));[|lia].
         intros ->. rewrite (fun_pow_trivial _ _ (size (vertices G)));[done|].
         apply fun_pow_fixpoint;[done|done|]. apply intersection_subseteq_r.
      ++ rewrite aux_lemma. exists u. split;[|exists m; split];done.
Qed.

Lemma elem_of_connected_component2 {A} `{Countable A} (G : dir_graph A) X v :
  X ⊆ vertices G →
    v ∈ connected_component G X ↔ ∃ u, u ∈ X ∧ path_rel G u v.
Proof.
  intro incl_V. rewrite elem_of_connected_component1. split.
  + destruct 1 as [u [in_inter path_rel_u_v]]. exists u. split;[|done].
    rewrite elem_of_intersection in in_inter. apply (proj1 in_inter).
  + destruct 1 as [u [in_X path_rel_u_v]]. exists u. split;[|done].
    rewrite elem_of_intersection. split;[|apply incl_V];done.
Qed.

Lemma elem_of_connected_component3 {A} `{Countable A} (G : dir_graph A) u :
  u ∈ vertices G →
    ∀ v, v ∈ connected_component G {[u]} ↔ path_rel G u v.
Proof.
  intros in_V v.
  assert ({[u]} ⊆ vertices G) as incl_V.
  { intro w. rewrite elem_of_singleton. intros ->. done. }
  rewrite (elem_of_connected_component2 _ {[u]} _ incl_V). split.
  + destruct 1 as [u' [eq_u path]]. revert eq_u.
    rewrite elem_of_singleton. intros ->. done.
  + intro path. exists u. split;[|done]. rewrite elem_of_singleton. done.
Qed.

Lemma connected_components_subseteq {A} `{Countable A} (G : dir_graph A) X :
  connected_component G X ⊆ vertices G.
Proof.
  intros v. rewrite elem_of_connected_component1.
  destruct 1 as [u [in_X path]].
  destruct (rtc_nsteps _ _ path) as [m msteps].
  revert msteps. case m.
  + inversion 1. revert in_X.
    rewrite elem_of_intersection, H2. intros [_ in_V]. done.
  + intros k Sk_steps.
    destruct (nsteps_succ_inv _ _ _ _ Sk_steps) as [w [_ in_edges]].
    apply (proj2 (elem_of_edges3 _ _ _ in_edges)).
Qed.

Lemma path_rel_not_elem_of_vertices {A} `{Countable A} (G : dir_graph A) u v :
  u ∉ vertices G → path_rel G u v → u = v.
Proof.
  intros not_in_V path. destruct (rtc_nsteps _ _ path) as [m m_steps].
  revert m_steps. case m.
  + inversion 1. done.
  + intro k. inversion 1. destruct (elem_of_edges3 _ _ _ H1) as [in_V _]. done.
Qed.

Instance path_rel_po {A} `{Countable A} (G : dir_graph A) : PreOrder (path_rel G).
Proof. apply rtc_po. Qed.

Instance path_rel_dec {A} `{Countable A} (G : dir_graph A) : RelDecision (path_rel G).
Proof.
  intros u v. case (gset_elem_of_dec u (vertices G)).
  + intro in_V. case (gset_elem_of_dec v (connected_component G {[u]})).
    - intro in_cc. left. rewrite <-(elem_of_connected_component3 _ _ in_V). done.
    - intro not_in_cc. right. rewrite <-(elem_of_connected_component3 _ _ in_V). done.
  + case (EqDecision0 u v).
    - intros -> _. left. apply rtc_refl.
    - intros ineq not_in_V. right. intro path.
      apply ineq, (path_rel_not_elem_of_vertices G); done.
Qed.


Lemma next_goal {A} `{Countable A} (G : dir_graph A) u :
  size (connected_component G {[u]}) ≤ S (size (edges G)).
Admitted.

(* TODO: add the following instances. *)

(*
Instance dir_graph_elem_of.
Instance dir_graph_empty.
Instance dir_graph_singleton.
Instance dir_graph_union.
Instance dir_graph_intersection.
Instance dir_graph_difference.
Instance dir_graph_elements.
*)
