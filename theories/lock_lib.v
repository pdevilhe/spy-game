From iris.proofmode     Require Import proofmode.
From iris.base_logic    Require Import invariants.
From iris.program_logic Require Import weakestpre.
From iris.heap_lang     Require Import proofmode notation lang.
From iris.heap_lang.lib Require Import spin_lock.

Section WithLock.

  Context `{!heapGS Σ} `{lock} `{!lockG Σ}.

  Definition withLock : val := λ: "lk" "e",
    acquire "lk";;
    let: "y" := "e" #() in
    release "lk";;
    "y".

  Lemma withLock_spec γ lk I P Q (e : val) :

    {{{ I ∗ P }}} e #() {{{ y, RET y; I ∗ Q y }}} ⊢

    {{{ is_lock γ lk I ∗ P }}}
      withLock lk e
    {{{ y, RET y; Q y }}}.

  Proof.
    iIntros "#e_spec". iModIntro. iIntros (Φ) "[#Hlk P] Post". wp_lam. wp_pures.
    wp_apply (acquire_spec with "Hlk"). iIntros "[Hlocked I]". wp_pures.
    wp_apply ("e_spec" with "[P I]"); [iFrame|iIntros (y) "[I Q]"]. wp_pures.
    wp_apply (release_spec γ lk I with "[Hlk Hlocked I]"); [by iFrame|iIntros "_"].
    wp_seq. by iApply "Post".
  Qed.

End WithLock.
