From TLC   Require Import LibMin LibLogic.
From stdpp Require Import base orders.

Class Sup A := sup: (A → Prop) → A.
Notation "∐" := sup (format "∐") : stdpp_scope.

Class Inf A := inf: (A → Prop) → A.
Notation "∏" := inf (format "∏") : stdpp_scope.

Global Instance sup_join `{Sup A} : Join A :=
  λ x y, ∐ (λ z, z = x ∨ z = y).
Global Instance inf_meet `{Inf A} : Meet A :=
  λ x y, ∏ (λ z, z = x ∨ z = y).

Global Instance sup_bottom `{Sup A} : Bottom A := ∐ (λ (_ : A), False).
Global Instance sup_top    `{Sup A} : Top A    := ∐ (λ (_ : A), True).

Class CompleteLattice `{Sup A} (R : relation A) := {
  #[global] complete_lattice_partial :: PartialOrder R;
  sup_lub S : lub R S (∐ S)
}.

Global Instance complete_lattice_inhab `{CompleteLattice A R} : Inhab A := (Inhab_of_val ⊥).
Global Instance complete_lattice_inf   `{CompleteLattice A R} : Inf A   := λ S, ∐ (λ x, ∀ y, S y → R x y).

Lemma bottom_spec `{CompleteLattice A R} : ∀ a, R ⊥ a.
Proof. case (sup_lub (λ _, False)); intros _ Lowb a; apply Lowb; by intros a'. Qed.
Lemma top_spec    `{CompleteLattice A R} : ∀ a, R a ⊤.
Proof. case (sup_lub (λ _, True)); intros Uppb _ a; by apply Uppb. Qed.
Lemma inf_glb `{CompleteLattice A R} (S : A → Prop) : glb R S (∏ S).
Proof.
  unfold inf, complete_lattice_inf.
  case (sup_lub (λ x, ∀ y, S y → R x y)); intros Uppb Lowb; split.
  + intros a Sa; apply Lowb; intros u Hu; by apply Hu.
  + intros l Lowl; by apply Uppb.
Qed.
Lemma sup_eq_intro `{CompleteLattice A R} S a : lub R S a → ∐ S = a.
Proof.
  intros [Uppa Lowa].
  case (sup_lub S); intros Uppb Lowb.
  assert (AntiSymm eq R) as antisymm; [ apply H0 |].
  apply antisymm; [ apply Lowb | apply Lowa ]; done.
Qed.
Lemma inf_eq_intro `{CompleteLattice A R} S a : glb R S a → ∏ S = a.
Proof.
  intros [Lowa Uppa].
  case (inf_glb S); intros Lowb Uppb.
  assert (AntiSymm eq R) as antisymm; [ apply H0 |].
  apply antisymm; [ apply Uppa | apply Uppb ]; done.
Qed.
Lemma sup_alt_def `{CompleteLattice A R} S : ∐ S = ∏ (λ x, ∀ y, S y → R y x).
Proof.
  apply sup_eq_intro.
  case (inf_glb (λ x, ∀ y, S y → R y x)); intros Lowb Uppb; split.
  + intros a Sa; apply Uppb; intros u Hu; by apply Hu.
  + intros l Uppl; by apply Lowb.
Qed.


Definition pointwise_rel {A B} (R : relation B) : relation (A → B) := λ f g, ∀ x, R (f x) (g x).

Global Instance partial_order_fun {A B} `{PartialOrder B R} :
  PartialOrder (pointwise_rel R : relation (A → B)).
Proof.
  split; [ apply Build_PreOrder | ].
  + intros f x; apply H.
  + intros f g h Rfg Rgh x; transitivity (g x); [ apply Rfg | apply Rgh ].
  + intros f g Rfg Rgf; apply fun_ext_dep; intro x.
    apply H; [ apply Rfg | apply Rgf ].
Qed.
Global Instance sup_fun {A} `{Sup B} : Sup (A → B) := λ S, λ a, ∐ (λ b, ∃ f, S f ∧ b = f a).
Global Instance complete_lattice_fun {A} `{CompleteLattice B R} :
  CompleteLattice (pointwise_rel R : relation (A → B)).
Proof.
  apply (Build_CompleteLattice _ _ _ partial_order_fun).
  intro S; unfold sup, sup_fun; split.
  + intros f Sf x.
    case (sup_lub (λ b, ∃ f, S f ∧ b = f x)); intros Uppb _; apply Uppb; by exists f.
  + intros g Uppg x.
    case (sup_lub (λ b, ∃ f, S f ∧ b = f x)); intros _ Lowb; apply Lowb.
    intros y [f [Sf ->]]; by apply Uppg.
Qed.

Definition monotone {A B} (Ra : relation A) (Rb : relation B) :=
  λ f, ∀ x y, Ra x y → Rb (f x) (f y).

Section tarski.

  Context `{CompleteLattice A R}.
  Context (F : A → A) (M : monotone R R F).

  Lemma sup_tarski (S : A → Prop) :
    let s := ∏ (λ x, (∀ y, S y → R y x) ∧ R (F x) x) in
    (∀ x, S x → x = F x) →
    min_element R (λ x, (∀ y, S y → R y x) ∧ x = F x) s.
  Proof.
    set (S' := λ x, (∀ y, S y → R y x) ∧ R (F x) x).
    case (inf_glb S'); simpl; intros Lows Upps FixS.
    assert (R (F (∏ S')) (∏ S')) as ge.
    { apply Upps; intros x [Sx RxFx]; transitivity (F x); [ apply M, Lows | ]; done. }
    assert (R (∏ S') (F (∏ S'))) as le.
    { apply Lows; split; [ | by apply M ].
      intros y Sy; rewrite (FixS y Sy); apply M.
      apply Upps; intros x [Sx RxFx]; by apply Sx. }
    split; [ split; [ | by apply H0 ] | ].
    + intros y Sy; apply Upps; intros x [Sx RxFx]; by apply Sx.
    + intros x [Sx Fx]; apply Lows; split; [ | rewrite <-Fx ]; done.
  Qed.

  Definition tarski_lfp : A := ∏ (λ x, R (F x) x).

  Lemma tarski_lfp_properties :
    tarski_lfp = F tarski_lfp ∧ (∀ x, x = F x → R tarski_lfp x).
  Proof.
    assert (tarski_lfp = ∏ (λ x, (∀ y, False → R y x) ∧ R (F x) x)) as lfp_eq.
    { unfold tarski_lfp.
      rewrite (@fun_ext_dep A (λ _, Prop) (λ x : A, R (F x) x)
                                          (λ x : A, (∀ y : A, False → R y x) ∧ R (F x) x));
      [ | intros x; apply prop_ext; split; [ intros; split | intros [_ Ryx] ] ]; done. }
    rewrite lfp_eq.
    case (sup_tarski (λ _, False)); [ done | ].
    intros [_ Fix] Low. split; [ done | ].
    intros x Fixx; apply Low; split; done.
  Qed.

  Definition fixpoint_rel : relation { x : A | x = F x } := λ x y, R (`x) (`y).

  Global Instance partial_order_fixpoint : PartialOrder fixpoint_rel.
  Proof.
    split; [ split | ].
    + intros x; apply H0.
    + intros x y z Rxy Ryz; unfold fixpoint_rel; transitivity (`y); done.
    + intros x y; case x; intros x' pf_x; case y; intros y' pf_y Rxy Ryx.
      apply sig_eq_pi; [ | by apply H0 ].
      intros z pf pf'; apply proof_irrelevance.
  Qed.

End tarski.
