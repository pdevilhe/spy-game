(* memo_last.v

   [memo_last] is a simple algorithm that receives a pure implementation of
   a mathematical function as input and returns a program implementing the
   same function but with the additional feature of storing the pair of
   input and output from the last time it was called. In a new call, the
   program uses this information to avoid unnecessary work. Please refer
   to its implementation bellow for further details.

   The interest in verifying [memo_last] is to understand the main techniques
   for the verification of memoizing programs.
*)

From iris.base_logic    Require Import invariants.
From iris.heap_lang     Require Import proofmode notation lang.
From spygame            Require Import type_repr.

Section MemoLast.

  Context `{!heapGS Σ} (eq : val).
  Let N := nroot .@ "memo_last".

  Definition memo_last : val := λ: "f",

    let: "cache" := ref NONE in

    let: "update" := λ: "x",
      let: "y" := "f" "x" in
      "cache" <- SOME ("x", "y");; "y"
    in

    λ: "x",
      match: !"cache" with
        NONE     => "update" "x"
      | SOME "l" =>
        let: "x'" := Fst "l" in
        let: "y'" := Snd "l" in
        if: eq "x" "x'" then "y'" else "update" "x"
      end.


  Definition memo_inv `{Encodable A, Encodable B} (F : A → B) (cache : loc) :=
    (∃ w, cache ↦ w ∗ ⌜ (w = NONEV) ∨ (∃ a, w = SOMEV (encode' (a, F a))) ⌝)%I.

  Lemma memo_last_spec `{Decodable A, Encodable B} f F :

    (∀ a a',
      {{{ True }}}
        eq (encode' (a : A)) (encode' a')
      {{{ RET #(bool_decide (a = a')); True }}}) ⊢

    {{{ implements (Func (Data A) (Data B)) f F }}}
      memo_last f
    {{{ (g : val), RET g;
        implements (Func (Data A) (Data B)) g F }}}.

  Proof.
    iIntros "#eq_spec". iModIntro. iIntros (Φ) "#f_spec Post".
    wp_lam. wp_alloc cache as "Hcache". wp_let.

    (* Invariant allocation: (cache ↦ NONEV). *)
    iMod (inv_alloc N _ (memo_inv F cache) with "[Hcache]") as "#Inv".
    { iNext. iExists NONEV. iFrame. iPureIntro. by left. }

    (* Proof that [update] implements the mathematical function F. *)
    iAssert
      (implements (Func (Data A) (Data B))
         (λ: "x", let: "y" := f "x" in #cache <- InjR ("x", "y");; "y") F)%I
    as "#update_spec".
    { iIntros (x a). iModIntro. clear Φ. iIntros (Φ) "-> Post". wp_pures.
      wp_apply ("f_spec" $! _ a); [done|]. iIntros (y) "->". wp_pures.
      (* Focus on the atomic expression: (#cache <- ...). *)
      wp_bind (#cache <- _)%E.
      (* Open the invariant. *)
      iInv N as "Hopen" "Hclose". iDestruct "Hopen" as (v) "[Hw _]".
      (* Close the invariant. *)
      wp_store. iMod ("Hclose" with "[Hw]") as "_".
      { iNext. iExists (encode' (Some (a, F a))).
        iFrame. iPureIntro. by right; exists a.
      }
      iModIntro. wp_pures. by iApply "Post".
    }

    wp_pures. iApply "Post". iModIntro.

    (* Proof that the output program implements the mathematical function F. *)
    iIntros (x a). iModIntro. clear Φ. iIntros (Φ) "-> Post". wp_pures.
    (* Focus on the atomic expression: (! #cache). *)
    wp_bind (! #cache)%E.
    (* Open the invariant. *)
    iInv N as "Hopen" "Hclose".
    iDestruct "Hopen" as (w) "[Hcache Hw]".
    (* Case analysis on (w = NONEV) ∨ (∃ a', w = SOMEV encode' (a', F a')). *)
    iDestruct "Hw" as "[>->|Hw]".
    (* Case: (cache ↦ NONEV). *)
    + wp_load.
      (* Close the invariant. *)
      iMod ("Hclose" with "[Hcache]") as "_".
      { iNext. iExists NONEV. iFrame. iPureIntro. by left. }
      iModIntro. wp_match. iApply ("update_spec" $! _ a); done.
    (* Case: (cache ↦ SOMEV (encode' (a', F a')). *)
    + wp_load. iDestruct "Hw" as (a') "->".
      (* Close the invariant. *)
      iMod ("Hclose" with "[Hcache]") as "_".
      { iNext. iExists (encode' (Some (a', F a'))).
        iFrame. iPureIntro. by right; exists a'.
      }
      iModIntro. wp_pures.
      wp_apply ("eq_spec" $! a a'); [done|]. iIntros "_".
      (* Case analisys on (a = a') ∨ (a ≠ a'). *)
      case (decodable_eq_dec a a').
      (* Case: (a = a'). *)
      - intros ->. rewrite bool_decide_eq_true_2;[|done].
        wp_if. by iApply "Post".
      (* Case: (a ≠ a'). *)
      - intros ineq. rewrite bool_decide_eq_false_2;[|done].
        wp_if. by iApply ("update_spec" $! _ a).
  Qed.

End MemoLast.
