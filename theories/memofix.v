(* memofix.v

   Consider a recursive program [f] and its memoized version [g] (that is, the
   output value of an application to [memoize_seq] or [memoize_conc] from the
   theory memoize.v). When the program [g] receives a call with a previously
   unseen argument, it transfers the control to the function [f] in order to
   compute the correct output for this argument. [f] is a recursive program,
   therefore, it can perform calls to itself. Some of these calls could feature
   arguments that have already been seen by [g], and thus leading to the
   duplication of work.

   The solution for this problem is to consider a non-recursive implementation
   of the program [f], that is, a non-recursive program [ff] for which the
   program [f] is unequivocally defined as the fixpoint of [ff].

   Notice that we are able to recover the program [f] from its non-recursive
   implementation [ff]. It suffices to define a recursive function [g] as
   [let rec g x = ff g x]. The function [g] satisfies the fixpoint equation
   by definition, hence it must behave like [f], the unique solution.

   Finally, we combine this alternative definition of [f] with memoization. This
   is precisely what is done by the program [memofix]. It defines a program [g]
   that solves the fixpoint equation and handles memoization, which is now
   possible since each recursive call passes the control back to [g].

   In this theory, we verify that [memofix] implements a fixpoint operator and
   we use this functionality to define a memoized program that implements the
   Fibonacci sequence.
*)

From iris.base_logic    Require Import invariants.
From iris.heap_lang     Require Import proofmode notation lang.
From iris.heap_lang.lib Require Import spin_lock.
From spygame            Require Import type_repr maps lock_lib.

Section Memofix.

  Context `{Map Σ} `{lock} `{!lockG Σ}.

  Definition memofix : val := λ: "ff",
    let: "m" := make_map #() in
    let: "lk" := newlock #() in

    (rec: "f" "x" :=
      match: withLock "lk" (λ: <>, map_lookup "m" "x") with
        SOME "y" => "y"
      | NONE     =>
        let: "y" := "ff" "f" "x" in
        withLock "lk" (λ: <>, map_insert "m" "x" "y");;
        "y"
      end).

  Lemma memofix_spec `{Decodable A, Decodable B}
    (P : A     → iProp Σ)
    (Q : A → B → iProp Σ)
    (ff : val) :

    {{{ (∀ (f : val),
          {{{ (∀ a, {{{ □ P a }}} f (encode' a) {{{ b, RET (encode' b); □ Q a b }}}) }}}
            ff f
          {{{ (g : val), RET g;
              (∀ a, {{{ □ P a }}} g (encode' a) {{{ b, RET (encode' b); □ Q a b }}})
          }}})
    }}}
      memofix ff
    {{{ (g : val), RET g;
              (∀ a, {{{ □ P a }}} g (encode' a) {{{ b, RET (encode' b); □ Q a b }}})
    }}}.

  Proof.
    iIntros (Φ) "#ff_spec Post". wp_lam.
    wp_apply (make_map_spec A B); [done|]. iIntros (m) "Hm". wp_pures.

    (* Invariant that describes the map and which is owned by the lock. *)
    wp_apply (newlock_spec (∃ (map : gmap A B),
      is_map m map ∗ ∀ a b, ⌜ map !! a = Some b ⌝ → □ Q a b)%I
    with "[Hm]").
    { iExists ∅. iFrame. iIntros (a b). rewrite lookup_empty; by iIntros "%". }
    iIntros (lk γ) "#Hlk". wp_pures. iModIntro. iApply "Post".

    (* Löb's induction: ▷ (∀ a, { □ P a } (rec f x := ... ) a { b. □ Q a b }). *)
    iLöb as "IH".
    iIntros (a). iModIntro. clear Φ. iIntros (Φ) "#HP Post". wp_lam.

    unfold withLock. wp_pures.
    (* Acquire the lock: perform map lookup. *)
    wp_apply (acquire_spec with "Hlk").
    iIntros "[Hlocked Hmap]". iDestruct "Hmap" as (map) "[Hmap #HQ]".
    wp_pures. wp_apply (map_lookup_spec m map with "Hmap"). iIntros "Hmap". wp_let.
    (* Release the lock. *)
    wp_apply (release_spec γ lk _ with "[Hlk Hlocked Hmap HQ]");[|iIntros "_"].
    { iFrame. iSplitL ""; [iApply "Hlk"|iExists map; iFrame]; done. }

    case_eq (map !! a).
    (* Case where the argument x has already been seen. *)
    + intros y lookup_map. wp_pures. iApply "Post". by iApply "HQ".
    (* Case where the argument x has never been seen. *)
    + intro lookup_map. wp_pures.
      (* Apply ff spec together with Löb's induction hypothesis. *)
      wp_apply ("ff_spec" with "IH").
      iIntros (g) "#g_spec". wp_apply ("g_spec" with "HP").
      iIntros (b) "#Q". wp_pures.
      (* Acquire the lock: addition of new key/value pair into the map. *)
      wp_apply (acquire_spec with "Hlk").
      iIntros "[Hlocked Hmap]". iDestruct "Hmap" as (map') "[Hmap' #HQ']".
      wp_pures. wp_apply (map_insert_spec m map' a b with "Hmap'"). iIntros "Hmap'".
      (* Release the lock. *)
      wp_pures. wp_apply (release_spec γ lk _ with "[Hlk Hlocked Hmap' HQ' Q]");
      [|iIntros "_"].
      { iFrame. iSplitL ""; [iApply "Hlk"|].
        iExists (<[ a := b ]> map'). iFrame.
        iIntros (a' b'). rewrite lookup_insert_Some.
        iIntros ([[-> ->]|[_ ?]]); first done. by iApply "HQ'". }
      wp_pures. by iApply "Post".
  Qed.

End Memofix.

Section Fibonacci.

  Context `{Map Σ} `{lock} `{!lockG Σ}.

  Fixpoint Fibonacci (n : nat) : nat :=
    (match n with 0 => 1 | S m =>
      match m with 0 => 1 | S l => Fibonacci m + Fibonacci l end
    end)%nat.

  Definition fib_norec : val := λ: "fib" "n",
    if: "n" ≤ #1%nat then #1%nat else ("fib" ("n" - #1%nat)) + ("fib" ("n" - #2%nat)).

  Lemma memofix_fib_norec_spec :

    {{{ True }}}
      memofix fib_norec
    {{{ f, RET f; implements (Func (Data nat) (Data nat)) f Fibonacci }}}.

  Proof.
    iIntros (Φ) "_ Post".
    wp_apply (memofix_spec (fun _ => True%I) (fun a b => (⌜ b = Fibonacci a ⌝)%I));
    unfold encode', nat_encoding.
    (* Proof that the non-recursive implementation of Fibonacci is correct. *)
    + iIntros (fib). iModIntro. clear Φ; iIntros (Φ) "#fib_spec Post". wp_lam.
      wp_pures. iModIntro. iApply "Post".
      iIntros (n). iModIntro. clear Φ; iIntros (Φ) "_ Post". wp_pures.
      (* Case analysis on: (n = 0) ∨ (n = 1) ∨ (n > 1). *)
      case n; [|intro m; case m;[|intro l]].
      (* Case : (n = 0). *)
      - rewrite bool_decide_eq_true_2;[|lia]. wp_pures. by iApply "Post".
      (* Case : (n = 1). *)
      - rewrite bool_decide_eq_true_2;[|lia]. wp_pures. by iApply "Post".
      (* Case : (n > 1). *)
      - rewrite bool_decide_eq_false_2;[|lia]. wp_pures.
        rewrite (_: (S (S l) - 2)%Z = l);[|lia]. wp_apply "fib_spec";[done|].
        iIntros (b) "#->". wp_pures.
        rewrite (_: (S (S l) - 1)%Z = S l);[|lia]. wp_apply "fib_spec";[done|].
        iIntros (b) "#->". wp_pures.
        rewrite (_: (match l with 0 => 1 | S k => Fibonacci l + Fibonacci k end)%nat =
                    Fibonacci (S l));
        [|by simpl].
        rewrite (_: Fibonacci (S l) + Fibonacci l = Fibonacci (S (S l)))%Z;[|simpl; lia].
        by iApply "Post".
    (* Proof that the output program implements Fibonacci. *)
    +  iIntros (f) "#f_spec". iApply "Post".
       iIntros (x n). iModIntro. clear Φ. iIntros (Φ) "-> Post". wp_apply "f_spec";[done|].
       iIntros (b) "#->". by iApply "Post".
  Qed.

End Fibonacci.
