From TLC Require Import LibTactics LibLogic LibReflect LibEpsilon LibInt
  LibProd LibSum LibOperation LibRelation LibWf LibOrder LibMin LibFix.

Definition covers {A B} (S : (A --> B) -> Prop) : A -> (A --> B) -> Prop :=
  fun x f => S f /\ dom f x.

Definition pfun_union {A B} {I : Inhab B} (S : (A --> B) -> Prop) : A --> B :=
  let Dom : A -> Prop :=
    fun x => exists f, covers S x f in
  let f : A -> B :=
    fun x => if classicT (Dom x) then (epsilon (covers S x)) x else arbitrary in
  Build_partial f Dom.

Lemma pfun_union_of_consistent_set {A B} {I : Inhab B}
  (E : binary B) (F : (A -> B) -> (A -> B)) (S : (A --> B) -> Prop) :
  equiv E ->
  consistent_set E S ->
  (forall fi, S fi -> partial_fixed_point E F fi) ->
  lub (extends E) S (pfun_union S) /\ partial_fixed_point E F (pfun_union S).
Proof using.
  introv Equiv Cons Fixi. split. split.
  { (* proof that f is an upper bound *)
    intros f' Sf'. split; simpl.
    intros x Dx. exists~ f'. split~.
    intros x D'x. destruct_if as Dx.
    epsilon fi. apply Dx. intros [Si Domi]. apply~ Cons.
    case Dx. exists~ f'. split~. }
  { (* proof that f is the smallest upper bound *)
    intros f' Upper'. split; simpl.
    intros x (fi&Ci&Di). apply~ (Upper' fi Ci).
    intros x Dx. destruct_if.
    epsilon~ fi. intros [Si Domi]. apply~ (Upper' fi). }
  { (* proof that f is a fixed point *)
    intros f' Eq'. simpls. intros x Dx. lets (fi&Ci&Di): Dx.
    apply~ (Fixi _ Ci). intros y Diy.
    asserts~ Dy: (exists fi, covers S y fi). exists~ fi. split~.
    apply~ (trans_inv ((pfun_union S) y)).
      unfold pfun_union. simpl. destruct_if.
       epsilon~ fj. intros [Sj Domj]. apply~ Cons.
      apply~ Eq'. }
Qed.

Lemma pfun_union_of_consistent_set_rel {A B} {I : Inhab B}
  (E R : binary B) (F : (A -> B) -> (A -> B)) (S : (A --> B) -> Prop) :
  antisym_wrt E R ->
  equiv E ->
  consistent_set R S ->
  (forall fi, S fi -> partial_fixed_point E F fi) ->
  lub (extends E) S (pfun_union S) /\ partial_fixed_point E F (pfun_union S).
Proof using.
  introv Antisym Equiv Cons Fixi.
  apply (pfun_union_of_consistent_set _ _ _ Equiv); [| apply Fixi ].
  intros fi fi' Sfi Sfi' x [Dx Dx'].
  apply Antisym.
    apply Cons; [ | | split ]; assumption.
    apply Cons; [ | | split ]; assumption.
Qed.

Definition generally_consistent_partial_lfp {A B}
  (E R : binary B) (F : (A -> B) -> (A -> B)) (f : A --> B) :=
     partial_fixed_point E F f
  /\ forall f', partial_fixed_point E F f' -> consistent R f f'.

Definition optimal_least_fixed_point {A B}
  (E R : binary B) (F : (A -> B) -> (A -> B)) (f : A --> B) :=
  max_element (extends E) (generally_consistent_partial_lfp E R F) f.

Definition optimal_lfp {A B} {I : Inhab B}
  (E R : binary B) (F : (A -> B) -> (A -> B)) : A --> B :=
  epsilon (optimal_least_fixed_point E R F).

Lemma optimal_least_fixed_point_exists {A B} {I : Inhab B}
  (E R : binary B) (F : (A -> B) -> (A -> B)) :
  antisym_wrt E R ->
  equiv E ->
  exists f, optimal_least_fixed_point E R F f.
Proof using.
  introv Antisym Equiv.
 (* there exists an optimal fixed point [f] *)
  sets S: (generally_consistent_partial_lfp E R F).
  forwards~ ([Upf Lubf]&Fixf): (@pfun_union_of_consistent_set_rel _ _ _ E R F S).
    intros f f' [Pf Cf] [Pf' Cf']. auto.
    intros f [Pf Cf]. auto.
  exists (pfun_union S). split~. split~.
  (* and it is generally consistent *)
  intros g Fixg. intros x [Dfx Dgx]. simpls. destruct_if. clear C.
  epsilon~ fi. intros [[Fixfi Sfi] Dfix]. apply~ Sfi.
Qed.

Lemma optimal_lfp_properties {A B} {I : Inhab B}
  (E R : binary B) (F : (A -> B) -> (A -> B)) :
  antisym_wrt E R ->
  equiv E ->
  optimal_least_fixed_point E R F (optimal_lfp E R F).
Proof using.
  intros Antisym Equiv.
  unfold optimal_lfp. epsilon~ fi. apply~ (@optimal_least_fixed_point_exists A).
Qed.

Lemma generally_consistent_alt_def {A B} (E : binary B) (F : (A -> B) -> (A -> B)) (f : A --> B) :
  generally_consistent_partial_fixed_point E F f = generally_consistent_partial_lfp E E F f.
Proof using.
  unfold generally_consistent_partial_fixed_point,
         generally_consistent_partial_lfp; reflexivity.
Qed.

Lemma optimal_fixed_point_alt_def {A B} (E : binary B) (F : (A -> B) -> (A -> B)) (f : A --> B) :
  optimal_fixed_point E F f = optimal_least_fixed_point E E F f.
Proof using.
  unfold optimal_fixed_point, optimal_least_fixed_point, max_element.
  rewrite~ @generally_consistent_alt_def.
Qed.

Lemma optimal_fixed_point_exists_another_proof {A B} {I : Inhab B}
  (E : binary B) (F : (A -> B) -> (A -> B)) :
  equiv E ->
  exists f, optimal_fixed_point E F f.
Proof using.
  introv Equiv. forwards~ (f&OptFixf):
    (@optimal_least_fixed_point_exists _ _ _ E E F (fun _ _ pf _ => pf) Equiv).
  exists f. rewrite~ @optimal_fixed_point_alt_def.
Qed.
