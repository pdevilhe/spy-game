#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

ARCHIVE=spy-game

rm -rf $ARCHIVE $ARCHIVE.tar.gz

mkdir $ARCHIVE

cp -r \
  theories \
  opam \
  setup.sh \
  Makefile \
  INSTALL.txt \
  _CoqProject \
  $ARCHIVE

# Remove useless source files.

(cd $ARCHIVE/theories && make clean && rm -f \
  arrays.v \
  fix_simple.v \
  memo_last.v \
  memoize.v \
)

tar cvfz $ARCHIVE.tar.gz $ARCHIVE
